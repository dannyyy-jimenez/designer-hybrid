import Constants from 'expo-constants';

const ENV = {
  dev: {
    baseUrl: 'http://10.0.0.102:80',
    apiUrl: 'http://10.0.0.102:80/api',
    stripeKey: 'pk_test_pHZsXAaoeTMMuc0l0kPZ5Hxh00IiHmUG8H'
  },
  staging: {
    baseUrl: 'http://10.0.0.102:80',
    apiUrl: "http://10.0.0.102:80",
    stripeKey: 'pk_test_pHZsXAaoeTMMuc0l0kPZ5Hxh00IiHmUG8H'
  },
  prod: {
    baseUrl: 'https://www.tailorii.app',
    apiUrl: "https://www.tailorii.app/api/",
    stripeKey: 'pk_live_8Er8mHNsXc7fXfzp1uobJw0q002IqK04Qx'
  }
};

function getEnvVars(env = "") {
  if (!env || env === null || env === undefined || env === "") return ENV.dev;
  if (env.indexOf("dev") !== -1) return ENV.dev;
  if (env.indexOf("staging") !== -1) return ENV.staging;
  if (env.indexOf("prod") !== -1) return ENV.prod;
  return ENV.dev;
}

export default getEnvVars(Constants.manifest.releaseChannel);
