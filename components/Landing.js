import React from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity } from 'react-native';
import localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';
import { useIsFocused } from '@react-navigation/native';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

export default function Landing({navigation}) {
  let colorScheme = useColorScheme();

  React.useEffect(() => {
    setTimeout(() => {
      StatusBar.setBarStyle('light-content')
    }, 100);
  }, [colorScheme])

  React.useEffect(() => {
    if (!navigation.isFocused() && Appearance.getColorScheme() === 'light') {
      StatusBar.setBarStyle('dark-content');
      return;
    }

    StatusBar.setBarStyle('light-content');
  }, [useIsFocused()]);

  return (
    <View style={{
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100%',
      backgroundColor: colorScheme === 'light' ? stylesheet.Primary: stylesheet.Secondary
    }}>
      <View style={styles.spacer}></View>
      <View style={styles.spacer}></View>
      <Image style={styles.defaultLandingImage} source={colorScheme === 'light' ? require('../assets/iconion-white.png') : require('../assets/iconion.png')}></Image>
      <View style={{flex: 0.3}}></View>
      <Text style={[colorScheme === 'light' ? styles.secondary : styles.tertiary, styles.subHeaderTextAlt, styles.nunitoText]}>{localization.t('slogan')}</Text>
      <View style={styles.spacer}></View>
      <View style={styles.spacer}></View>
      <TouchableOpacity
        style={[styles.roundedButton, colorScheme === 'light' ? styles.clear : styles.filled]}
        onPress={() => navigation.navigate('Register')}
        underlayColor='#fff'>
        <Text style={[colorScheme === 'light' ? styles.primary : styles.tertiary, styles.baseText, styles.centerText]}>{localization.t('signup')}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.roundedButton, colorScheme === 'light' ? styles.filled : styles.clear]}
        onPress={() => navigation.navigate('Login')}
        underlayColor='#fff'>
        <Text style={[colorScheme === 'light' ? styles.secondary : styles.primary, styles.baseText, styles.centerText]}>{localization.t('login')}</Text>
      </TouchableOpacity>
      <View style={{flex: 0.1}}></View>
    </View>
  );
}
