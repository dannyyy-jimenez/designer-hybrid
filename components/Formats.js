import Colors from '../models/colors';
import Localization from '../localization/config';

export const Formats = {
  formatPhoneNumber : (phoneNumberString) => {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{1,3})?(\d{1,4})?$/)
    if (match) {
      return '(' + match[1] + (match[1] && match[2] ? ') ' : '') + (match[2] || '') + (match[2] && match[3] ? '-' : '') + (match[3] || '')
    }
    return phoneNumberString;
  },
  formatDob : (dobString) => {
    var cleaned = ('' + dobString).replace(/\D/g, '')
    var match = cleaned.match(/^(0[1-9]|1[0-2])(0[1-9]?|1\d?|2\d?|3[01]?)((19?\d{0,2})|(20?\d{0,2}))?$/);
    if (match) {
      return match[1] + (match[2] ? '/' + match[2] : '') + (match[3] && match[2] && match[2].length == 2 ? '/' + match[3] : '');
    }
    return dobString;
  },
  ccNumber: (number) => {
    number = number.replace(/\D/g, '');
    let groups = number.match(/.{1,4}/g);
    if (!groups) return '';
    return groups.join(' ');
  },
  ccExp: (date, previous) => {
    if (date.length < previous.length && date.length == 2) {
      // backspace
      date = date.substring(0,1);
    } else if (date.length == 2) {
      date += '/';
    }

    return date;
  },
  ccExpPretty: (date) => {
    let parts = date.split('/');

    if (parts[1] && parts[1].length > 2) {
      parts[1] = parts[1].substring(2);
    }

    return parts.join('/');
  },
  cardIcon: (number) => {
    if (number.match(/^4/g) != null)
        return "cc-visa";

    if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
        return "cc-mastercard";

    if (number.match(/^3[47]/g) != null)
        return "cc-amex";

    let re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
      return "cc-discover";

    re = new RegExp("^36");
    if (number.match(re) != null)
      return "cc-diners-club";

    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
      return "cc-jcb";

    return 'credit-card';
  },
  abbreviate: (quantity) => {
    if (quantity >= 1000000000) {
      return `${(quantity / 1000000000).toFixed(1)}B`;
    } else if (quantity >= 1000000) {
      return `${(quantity / 1000000).toFixed(1)}B`;
    } else if (quantity >= 1000) {
      return `${(quantity / 1000).toFixed(1)}K`;
    }
    return quantity.toString();
  },
  cloudinarize: (uri, transformations = '', png = false) => {
    if (!uri) return '';

    if (uri && uri.includes('https://res.cloudinary.com/lgxy')) {
      return uri;
    }

    if (png) {
      uri = uri.split(".")[0] + '.png';
    }
    if (uri.includes('gif')) {
      transformations = '';
    }

    return `https://res.cloudinary.com/lgxy/image/upload/${transformations}${uri}`;
  },
  getType: (type) => {
    if (type === "shirtTxB") return Localization.t('shirtTxB');
    if (type === "shirtTsleeve") return Localization.t('shirtTsleeve');
    if (type === "shirtT") return Localization.t('shirtT');
    if (type.includes("shirt")) return Localization.t('shirt');
    if (type.includes("hoodie")) return Localization.t('hoodie');
    if (type === "popsocket") return Localization.t('popsocket');
    if (type === "mug") return Localization.t('mug');
    if (type === "pillow") return Localization.t('pillow');
    if (type === "keychain") return Localization.t('keychain')
    if (type === "crewneck") return Localization.t('crewneck');
    if (type === "beanie") return Localization.t('beanie');

    return Localization.t('item');
  },
  getAvailableColors: (type, color = 'default') => {
    return Colors[type] ? Colors[type] : [];
  },
  getColor: (type, color, raw = false, labelize = false) => {
    if (raw) {
      if (!Colors[type].find(colorMap => colorMap.value == color)) return "212121";

      return Colors[type].find(colorMap => colorMap.value == color).color.slice(1);
    }

    if (labelize) {
      return (Colors[type].find((colorMap) => colorMap.value == color) || {label: 'TBD'}).label;
    }

    return Colors[type].find(colorMap => colorMap.value == color).color;
  },
  formatSize: (size) => {
    if (['XS', 'S', 'M', 'L', 'XL', 'XXL'].includes(size)) {
      return Localization.t(size);
    }

    return Localization.t('default');
  },
  getEstimatedPricing: (type, royalty) => {
    let pricing = 0;
    let DEF = 4.5;
    royalty = parseFloat(royalty) || 0;

    if (type === 'shirtT') {
      pricing = ((0.1 * royalty) + DEF) + royalty + 7;
    }

    if (type === 'shirtTsleeve') {
      pricing = ((0.1 * royalty) + DEF) + royalty + 10;
    }

    if (type === 'hoodie') {
      DEF = 5.5;
      pricing = ((0.1 * royalty) + DEF) + royalty + 19;
    }

    if (type === 'crewneck') {
      DEF = 5.5;
      pricing = ((0.1 * royalty) + DEF) + royalty + 16;
    }

    if (type === 'beanie') {
      DEF = 4;
      pricing = ((0.1 * royalty) + DEF) + royalty + 8;
    }

    if (pricing === 0) {
      pricing = 15 + royalty;
    }

    return Localization.t('estPricing', {price: pricing});
  },
  getEstimatedRoyalty: (type, pricing, cop) => {

    cop = parseFloat(cop);
    pricing = parseFloat(pricing);

    let royalty = pricing - cop;
    royalty -= 4.5;
    royalty *= 0.90;
    royalty = (Math.round(royalty * 100) / 100).toFixed(2);

    return Localization.t('estRoyalty', {royalty});
  },
  getAvailableSizes: (type) => {
    if (type === 'hoodie') return ['S', 'M', 'L', 'XL'];
    if (type === 'shirtT_Sleeve') return ['S', 'M', 'L', 'XL'];
    if (type === 'cap') return [];
    if (type === 'shoe_AF') return ['M3.5', 'M4', 'M4.5', 'M5', 'M5.5', 'M6', 'M6.5', 'M7', 'M7.5', 'M8', 'M8.5', 'M9', 'M9.5', 'M10', 'M10.5', 'M11', 'M11.5', 'M12', 'M12.5', 'M13', 'M13.5'];

    return ['XS', 'S', 'M', 'L', 'XL', 'XXL'];
  },
  getReactivePlaceholder: (reactivity) => {
    if (reactivity === 'songname') return Localization.t('songPlace');

    return Localization.t('reactiveTxt');
  },
  formatSide: (side) => {
    return Localization.t(side, { defaultValue: "" });
  }
}
