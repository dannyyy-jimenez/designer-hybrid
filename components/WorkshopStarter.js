import React from 'react';
import { View, SafeAreaView, Text, Image, ScrollView, Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

let colorScheme = Appearance.getColorScheme();
let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;
const DesignTypes = require('../models/Types').DesignTypes;
const Templates = require('../models/Templates').Templates;

export default function WorkshopStarter({navigation, route}) {
  const SESSION = route.params.session;

  const [type, setType] = React.useState('');
  const mode = "workshop";
  const [shots, setShots] = React.useState([]);

  const onDeleteImage = (index) => {
    let previous = shots.slice();
    previous.splice(index, 1);
    setShots(previous);
  }

  React.useEffect(() => {
    if (type === '') return;

    navigation.navigate('Workshop', {type, mode, session: SESSION});
    setType('');
  }, [type])

  React.useEffect(() => {

  }, [useColorScheme()])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="md-close" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={[{alignItems: 'flex-start', width: '90%', marginLeft: '5%', paddingTop: 10},{justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap'}]}>
        <Text style={[styles.centerText, styles.primary, styles.bold, styles.fullWidth, styles.subHeaderText, {marginTop: 20, marginBottom: 20}]}>{Localization.t('buildPrompt')}</Text>
        {
          DesignTypes.map((dType) => {
            return (
              <TouchableOpacity activeOpacity={1} key={dType.identifier} onPress={() => setType(dType.identifier)} style={styles.defaultStoreCardMini}>
                <Image style={styles.defaultSCImage} source={{ uri: `https://res.cloudinary.com/lgxy/image/upload/tailori/templates/static/${dType.uri}.png` }} />
                <View style={styles.defaultColumnContainer, styles.fullWidth, styles.featuredSCContent}>
                  <View style={{position: 'absolute', left: 0, bottom: 0, padding: 5, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 5, borderBottomLeftRadius: 5, backgroundColor: stylesheet.SecondaryTint}}>
                    <Text style={[styles.baseText, styles.tertiary, styles.opaque, {marginTop: 2}]}>{Localization.t(dType.identifier)}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: Dimensions.get('window').width * 0.7,
    marginLeft: Dimensions.get('window').width * 0.025,
    marginRight: Dimensions.get('window').width * 0.025,
    height: 350,
    borderRadius: 30,
    flexDirection: 'column',
    backgroundColor: stylesheet.SecondaryTint
  },
  itemImage: {
    height: '85%',
    width: 'auto',
    resizeMode: 'contain'
  },
  templateImage: {
    height: 150,
    width: 150,
    resizeMode: 'contain'
  },
  templateContainer: {
    margin: 8,
    height: 168,
    width: 168,
    padding: 9,
    borderRadius: 8,
    backgroundColor: stylesheet.SecondaryTint
  },
  delete: {
    position: 'absolute',
    right: 8,
    top: 2
  }
});
