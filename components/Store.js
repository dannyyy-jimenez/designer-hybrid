import React from 'react';
import { View, SafeAreaView, StyleSheet, TouchableWithoutFeedback, Switch, Keyboard, Dimensions, TextInput, RefreshControl, Image, ActivityIndicator, Text, ScrollView, TouchableOpacity } from 'react-native';
import { Ionicons, AntDesign, MaterialIcons, Octicons } from '@expo/vector-icons';
import * as Linking from 'expo-linking';
import Localization from "../localization/config";
import ActionSheet from "react-native-actions-sheet";
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';
import Merch from '../models/Merch';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

const actionSheetRef = React.createRef();

export default function Store({navigation, route}) {
  const SESSION = route.params.session;

  const [merch, setMerch] = React.useState([]);
  const [searchQuery, setSearchQuery] = React.useState('');
  const [filters, setFilters] = React.useState({
    min: '',
    max: '',
    sort: 'newest'
  });
  const [showFilters, setShowFilters] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [refreshing, setRefreshing] = React.useState(false);
  const [baseMerch, setBaseMerch] = React.useState([]);
  const [featured, setFeatured] = React.useState([]);

  // filters

  const [sortBy, setSortBy] = React.useState("newest");
  const [appearance, setAppearance] = React.useState("thumb");

  React.useEffect(() => {
    load();
  }, [sortBy]);

  React.useEffect(() => {

  }, [useColorScheme()])

  const load = async (refreshed) => {
    if (!refreshed && !showFilters) {
      setMerch([]);
      setIsLoading(true);
    }
    setRefreshing(true);

    try {
      const res =  await API.get('store', {auth: SESSION.crypto, sort: sortBy});
      if (res.isError) throw 'error';

      setBaseMerch(res.data._r.map(release => new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions)));
      setMerch(res.data._r.map(release => new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions)));
      setFeatured(res.data._f.map(release => new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions)));
      setIsLoading(false);
      setRefreshing(false);
    } catch (e) {
      setIsLoading(false);
      setRefreshing(false);
    };
  };

  const loadDeepZoom = (identifier) => {
    API.get('store/release', {auth: SESSION.crypto, identifier}).then((res) => {
      if (res.isError) throw 'error';

      let release = res.data._r;
      let scopeMerch = new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions);
      navigation.navigate('Zoom', {session: SESSION, identifier: identifier, release: scopeMerch})
    }).catch(e => {
      setIsLoading(false);
    });
  }

  const checkDeepZoom = async () => {
    const link = await Linking.getInitialURL();
    if (link) {
      // app was opened with a url and had no previous state
      let {path} = Linking.parse(link);
      if (!path) return;
      let matches = path.match(/^shop\/zoom\/.*$/g)

      if (matches) {
        loadDeepZoom(path.split('shop/zoom/')[1]);
      }
    }
  }

  React.useEffect(() => {
    if (!route.params.deepZoom || route.params.deepZoom == '') return;
    loadDeepZoom(route.params.deepZoom)
  }, [route.params?.deepZoom]);

  React.useEffect(() => {
    if (searchQuery.replace(/ /g, '') === '') {
      setMerch(baseMerch.slice());
      return;
    }
    setMerch(baseMerch.filter((release) => {
      let likeRegex = new RegExp(searchQuery, 'gi');
      return likeRegex.test(release.getName()) || likeRegex.test(release.getDescription()) || likeRegex.test(release.designer.getName());
    }));
  }, [searchQuery]);

  React.useEffect(() => {
    Linking.addEventListener('url', (data) => {
      let {path} = Linking.parse(data.url);
      if (!path) return;
      let matches = path.match(/^shop\/zoom\/.*$/g)

      if (matches) {
        loadDeepZoom(path.split('shop/zoom/')[1]);
      }
    });
    checkDeepZoom();
  }, []);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => actionSheetRef.current?.setModalVisible(true)}
          underlayColor='#fff'>
          <MaterialIcons name="sort" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={searchQuery} onChangeText={value => setSearchQuery(value)} keyboardType="default" placeholder={Localization.t('search')} placeholderTextColor="#555555" returnKeyType="search" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={styles.spacer}></View>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView showsVerticalScrollIndicator={false} style={styles.defaultTabScrollContent} contentContainerStyle={[{alignItems: 'flex-start', width: '90%', marginLeft: '5%', paddingTop: 10}, appearance === 'thumb' ? {justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap'} : {justifyContent: 'center'}]} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={() => load(true)} />}>
        {
          featured.length > 0 && searchQuery === '' &&
          <>
            <Text style={[styles.baseText, styles.fullWidth, styles.centerText, styles.primary, styles.bold, {marginTop: 20, marginBottom: 10}]}>{Localization.t('featured')}</Text>
            {
              featured.map((release) => {
                return (
                  <TouchableOpacity activeOpacity={1} key={release.getID()} onPress={() => navigation.navigate('Zoom', {session: SESSION, identifier: release.getIdentifier(), release: release})} style={appearance === 'thumb' ? styles.defaultStoreCardMini : styles.fullStoreCard}>
                    <Image style={appearance === 'thumb' ? styles.defaultSCImage : styles.fullSCImage} source={{uri: formats.cloudinarize(release.getCover(), '', true)}} />
                    <View style={styles.defaultColumnContainer, styles.fullWidth, appearance === 'thumb' ? styles.featuredSCContent : styles.fullSCContent}>
                      <View style={{position: 'absolute', left: 0, bottom: 0, padding: 5, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 5, borderBottomLeftRadius: 5, backgroundColor: stylesheet.SecondaryTint}}>
                        <Text style={[styles.baseText, styles.primary, styles.bold, {marginTop: 2}]}>${release.getPrice()}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              })
            }
              <View style={{marginTop: 30, width: '100%', marginBottom: 30}}></View>
          </>
        }
        {
          merch.map((release) => {
            return (
              <TouchableOpacity activeOpacity={1} key={release.getID()} onPress={() => navigation.navigate('Zoom', {session: SESSION, identifier: release.getIdentifier(), release: release})} style={appearance === 'thumb' ? styles.defaultStoreCard : styles.fullStoreCard}>
                <Image style={appearance === 'thumb' ? styles.defaultSCImage : styles.fullSCImage} source={{uri: formats.cloudinarize(release.getCover(), '', true)}} />
                <View style={styles.defaultColumnContainer, styles.fullWidth, appearance === 'thumb' ? styles.defaultSCContent : styles.fullSCContent}>
                  <View style={[styles.defaultRowContainer, styles.fullWidth]}>
                    <View style={[styles.defaultColumnContainer]}>
                      <Text numberOfLines={1} style={[styles.baseText, styles.nunitoText, styles.tertiary, {marginBottom: 2}]}>{release.getName()}</Text>
                    </View>
                    {
                      appearance === 'full' &&
                      <>
                        <View style={styles.spacer}></View>
                        <View style={[styles.defaultColumnContainer]}>
                          <Text style={[styles.tinyText, styles.tertiary, styles.opaque, {marginTop: 2}]}>${release.getPrice()}</Text>
                        </View>
                      </>
                    }
                  </View>
                  <View style={[styles.spacer, styles.defaultRowContainer, styles.fullWidth, {alignItems: 'center'}]}>
                    <Text style={[styles.tinyText, styles.primary, styles.bold, styles.center]}>{release.designer.getName()}</Text>
                  </View>
                  <View style={{position: 'absolute', right: 0, bottom: 0, padding: 5, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 5, borderBottomRightRadius: 5, backgroundColor: stylesheet.Primary}}>
                    <Text style={[styles.tinyText, styles.secondary, {marginTop: 2}]}>${release.getPrice()}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.StatusBar === 'light-content' ? stylesheet.Primary : stylesheet.Secondary}} indicatorColor={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 10}]}>{Localization.t('sortBy')}</Text>
          <View style={styles.line}></View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('newest')} disabled={sortBy === 'newest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'newest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('newest')}</Text>
              {
                sortBy === 'newest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Secondary} />
              }
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setSortBy('lowestPrice')} disabled={sortBy === 'lowestPrice'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'lowestPrice' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('lowestPrice')}</Text>
              {
                sortBy === 'lowestPrice' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Secondary} />
              }
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setSortBy('highestPrice')} disabled={sortBy === 'highestPrice'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'highestPrice' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('highestPrice')}</Text>
              {
                sortBy === 'highestPrice' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}
