import React from 'react';
import { View, SafeAreaView, Text, Switch, TextInput, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons, Ionicons, MaterialIcons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';
import * as Linking from 'expo-linking';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function Settings({navigation, route}) {
  const SESSION = route.params.session;

  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [fullName, setFullName] = React.useState(SESSION.name);
  const [password, setPassword] = React.useState('');
  const onPasswordChange = (value) => {
    setPassword(value);
  };
  const [smsUpdates, setSMSUpdates] = React.useState(true);
  const [smsUpdatesLoading, setSMSUpdatesLoading] = React.useState(false);
  const onSMSUpdatesChanged = () => {
    setSMSUpdatesLoading(true);
    setSMSUpdates(!smsUpdates);
    const body = { auth: SESSION.crypto, smsUpdates: !smsUpdates };

    API.post('settings/permissions', body).then(res => {
      setSMSUpdates(res.data.smsUpdates);
      setSMSUpdatesLoading(false);
    }).catch(() => setSMSUpdatesLoading(false));
  };

  React.useEffect(() => {

  }, [useColorScheme()])

  const onPasswordUpdate = () => {
    const passwordState = password;
    setPassword('');

    const body = { auth: SESSION.crypto, password };

    API.post('settings/password', body).then(res => {
      if (res.isError) {
        setPassword(passwordState);
        return;
      }
    }).catch(() => setPassword(passwordState));
  };

  React.useEffect(() => {
    API.get('settings/account', {auth: SESSION.crypto}).then(res => {
      setPhoneNumber(res.data.account._phone);
      setSMSUpdates(res.data.permissions._sms);
    }).catch(() => {});
  }, []);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Notifications')}
          underlayColor='#fff'>
          <MaterialCommunityIcons name="bell-outline" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('account')}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          onPress={() => navigation.navigate('History')}
          underlayColor='#fff'>
          <MaterialIcons name="history" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
      </View>
      <View style={styles.defaultTabContent}>
        <View style={{flex: 0.05}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>{Localization.t('name')}</Text>
        <View style={{flex: 0.01}}></View>
        <TextInput disabled={true} editable={false} style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.disabled, styles.tertiary]} value={fullName} readOnly={true} placeholder={Localization.t('name')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary} textContentType="name" />
        <View style={{flex: 0.05}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>{Localization.t('phoneNumber')}</Text>
        <View style={{flex: 0.01}}></View>
        <TextInput disabled={true} editable={false} style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.disabled, styles.tertiary]} value={phoneNumber} readOnly={true} placeholder={Localization.t('phoneNumber')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary} textContentType="username" />
        <View style={{flex: 0.15}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>{Localization.t('password')}</Text>
        <View style={{flex: 0.01}}></View>
        <TextInput style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.tertiary]} maxLength={16} value={password} onChangeText={value => onPasswordChange(value)} placeholder={Localization.t('password')} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} placeholderTextColor="#555555" returnKeyType="done" selectionColor={stylesheet.Primary} secureTextEntry={true} textContentType="newPassword" passwordrules="required: lower; required: upper; required: digit; minlength: 5; maxlength: 16;" />
        {
          password.length > 5 &&
          <>
            <View style={{flex: 0.04}}></View>
            <TouchableOpacity
              style={[styles.roundedButton, styles.clear]}
              onPress={() => onPasswordUpdate()}
              underlayColor='#fff'>
              <Text style={[styles.primary, styles.baseText, styles.centerText]}>{Localization.t('update')}</Text>
            </TouchableOpacity>
          </>
        }
        <View style={{flex: 0.15}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>{Localization.t('legal')}</Text>
        <View style={{flex: 0.03}}></View>
        <Text onPress={() => navigation.navigate('Document', {name: 'privacy'})} style={[styles.baseText, styles.tertiary, styles.baseInputHeader]}>&nbsp;{Localization.t('privacy')}</Text>
        <View style={{flex: 0.02}}></View>
        <Text onPress={() => navigation.navigate('Document', {name: 'terms'})} style={[styles.baseText, styles.tertiary, styles.baseInputHeader]}>&nbsp;{Localization.t('terms')}</Text>
        <View style={{flex: 0.02}}></View>
        <Text onPress={() => navigation.navigate('Document', {name: 'dmca'})} style={[styles.baseText, styles.tertiary, styles.baseInputHeader]}>&nbsp;{Localization.t('dmca')}</Text>
        <View style={{flex: 0.2}}></View>
        <TouchableOpacity
          onPress={() => navigation.navigate("Logout")}
          style={[styles.baseInputHeader]}
          underlayColor='#fff'>
          <Text style={[styles.baseText, styles.primary, styles.bold]}>{Localization.t('logout')}</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}
