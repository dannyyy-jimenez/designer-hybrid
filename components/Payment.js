import React from 'react';
import { View, SafeAreaView, KeyboardAvoidingView, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import env from '../env';
import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

const STRIPE_PUBLISHABLE_KEY = env.stripeKey;

export default function Payment({navigation, route}) {
  const SESSION = route.params.session;
  const [number, setNumber] = React.useState('');
  const [name, setName] = React.useState('');
  const [date, setDate] = React.useState('');
  const [cvc, setCVC] = React.useState('');
  const [canUpdate, setCanUpdate] = React.useState(false);
  const [secure, setSecure] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {

  }, [useColorScheme()])

  const getToken = async (card) => {
    return fetch('https://api.stripe.com/v1/tokens', {
      headers: {
        // Use the correct MIME type for your server
        Accept: 'application/json',
        // Use the correct Content Type to send data to Stripe
        'Content-Type': 'application/x-www-form-urlencoded',
        // Use the Stripe publishable key as Bearer
        Authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`
      },
      // Use a proper HTTP method
      method: 'post',
      // Format the credit card data to a string of key-value pairs
      // divided by &
      body: Object.keys(card)
        .map(key => key + '=' + card[key])
        .join('&')
    }).then(response => response.json());
  };

  const onUpdate = async () => {
    setCanUpdate(false);

    let stripeQuery = {
      'card[number]': number.replace(/ /g, ''),
      'card[exp_month]': date.split('/')[0],
      'card[exp_year]': date.split('/')[1],
      'card[cvc]': cvc,
      'card[name]': name
    }

    let token;
    try {
      token = await getToken(stripeQuery);
      if (token.error) throw 'CARD_ERROR';
    } catch(e) {
      setError(true);
      return;
    }

    const body = { auth: SESSION.crypto, token: token.id};
    API.post('settings/payment', body).then(res => {
      if (res.isError) {
        setError(true);
        return;
      }

      setSecure(true);
      setNumber(token.card.last4);
      setCVC('');
    }).catch((e) => {
      setError(true)
    });
  };

  React.useEffect(() => {
    setError(false);
    if (number.length != 4) {
      setSecure(false);
    }
    if (number.length < 13 || number.length > 19) return setCanUpdate(false);
    if (date.split('/').length != 2 || date.split('/')[0].length != 2 || (date.split('/')[1].length != 2 &&  date.split('/')[1].length != 4)) return setCanUpdate(false);
    if (cvc.length !== 3 && cvc.length !== 4) return setCanUpdate(false);
    if (name === '') return setCanUpdate(false);
    if (name.includes('*')) {
      setName('');
      setCanUpdate(false);
      return;
    };

    setCanUpdate(true);
  }, [number, name, date, cvc]);

  React.useEffect(() => {
    setIsLoading(true);

    API.get('settings/payment', { auth: SESSION.crypto }).then(res => {
      if (res.isError) {
        return;
      }

      setName(res.data.name);
      setNumber(res.data.last4);
      setDate(res.data.exp);
      setSecure(res.data.secure);
      setIsLoading(false);
    }).catch((e) => navigation.goback());
  }, []);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      <KeyboardAvoidingView style={styles.defaultTabContent} behavior="padding">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            width: '100%'
          }}>
            {
              isLoading &&
              <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
                <View style={{flex: 0.1}}></View>
                <ActivityIndicator size="small" color={stylesheet.Primary} />
              </View>
            }
            <View style={{flex: 0.5}}></View>
            <View style={styles.cardContainer}>
              <Text style={[styles.nunitoText, styles.subHeaderText, styles.secondary]}>Tailori</Text>
              <View style={styles.spacer}></View>
              {
                secure &&
                <Text style={[styles.baseText, styles.cardNumber, styles.secondary, styles.bold]}>**** **** **** {number}</Text>
              }
              {
                !secure &&
                <Text style={[styles.baseText, styles.cardNumber, styles.secondary, styles.bold]}>{number}</Text>
              }
              <View style={{flex: 0.5}}></View>
              <View style={{display: 'flex', flexDirection: 'row'}}>
                <Text style={[styles.baseText, styles.secondary, styles.caps]}>{name}</Text>
                <View style={styles.spacer}></View>
                <Text style={[styles.baseText, styles.secondary]}>{formats.ccExpPretty(date)}</Text>
              </View>
              <View style={{flex: 0.2}}></View>
              <View style={{flexDirection: 'row'}}>
                <View style={styles.spacer}></View>
                {
                  !secure &&
                  <FontAwesome name={formats.cardIcon(number)} size={24} color={stylesheet.Secondary}></FontAwesome>
                }
                {
                  secure &&
                  <FontAwesome name="user-secret" size={24} color={stylesheet.Secondary}></FontAwesome>
                }
              </View>
            </View>
            <View style={styles.spacer}></View>
            <TextInput keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={number} onChangeText={(value) => setNumber(formats.ccNumber(value))} keyboardType="numeric" placeholder={Localization.t('cardNumber')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary} textContentType="creditCardNumber" />
            <View style={{flex: 0.06}}></View>
            <TextInput keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={name} onChangeText={(value) => setName(value)} keyboardType="default" placeholder={Localization.t('cardName')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary} textContentType="name" />
            <View style={{flex: 0.06}}></View>
            <View style={styles.baseInputContainer}>
              <TextInput keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.filledInput, styles.inputMulti, styles.tertiary]} value={date} onChangeText={(value) => setDate(formats.ccExp(value, date))} keyboardType="default" placeholder={Localization.t('cardExp')} placeholderTextColor="#555555" maxLength={7} selectionColor={stylesheet.Primary} textContentType="none" />
              <View style={{flex: 0.06}}></View>
              <TextInput keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.filledInput, styles.inputMulti, styles.tertiary]} value={cvc} onChangeText={(value) => setCVC(value)} keyboardType="number-pad" placeholder={Localization.t('cardCVC')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary} textContentType="none" />
            </View>
            <View style={{flex: 0.2}}></View>
            <TouchableOpacity
              style={[styles.roundedButton, styles.filled, !canUpdate ? styles.disabled : {opacity: 1}]}
              disabled={!canUpdate}
              onPress={() => onUpdate()}
              underlayColor='#fff'>
              <Text style={[styles.tertiary, styles.baseText,  styles.centerText, styles.bold]}>{Localization.t('update')}</Text>
            </TouchableOpacity>
            {
              error &&
              <>
                <View style={{flex: 0.04}}></View>
                <Text style={[styles.centerText, styles.primary, styles.baseText]}>{Localization.t('EcardVerif')}</Text>
              </>
            }
            <View style={{flex: 0.1}}></View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
