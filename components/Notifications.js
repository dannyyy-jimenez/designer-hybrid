import React from 'react';
import { View, SafeAreaView, Text, StyleSheet, ScrollView, RefreshControl, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';
import Notification from '../models/Notification';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

export default function Notifications({navigation, route}) {
  const SESSION = route.params.session;
  const [notifications, setNotifications] = React.useState([]);
  const [refreshing, setRefreshing] = React.useState(false);

  React.useEffect(() => {

  }, [useColorScheme()])

  const load = async () => {
    setRefreshing(true);
    try {
      let res = await API.get('notifications', {auth: SESSION.crypto});
      setNotifications(res.data._n.map((notification) => new Notification(notification.header, notification.subheader, notification.action, notification.metadata)));
      setRefreshing(false);
    } catch(e) {
      setRefreshing(false);
      return;
    }
  };

  React.useEffect(() => {
    load();
  }, []);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Settings')}
          underlayColor='#fff'>
          <Ionicons name="ios-settings" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('notifications')}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          onPress={() => navigation.navigate('History')}
          underlayColor='#fff'>
          <MaterialIcons name="history" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={load} />}>
        <View style={{flex: 0.05}}></View>
        {
          notifications.map((notification, index) => {
            if (notification.getAction() === 'track') {
              return (
                <TouchableOpacity key={index} style={[Styles.notificationContainer, {backgroundColor: stylesheet.SecondaryTint}]} onPress={() => navigation.navigate('Tracking', {orderNumber: notification.metadata.orderNumber})}>
                  <View style={[styles.spacer, Styles.notificationContentContainer]}>
                    <Text style={[styles.baseText, styles.tertiary]}>{notification.getHeader()}</Text>
                    <Text style={[styles.tinyText, styles.tertiary, styles.opaque, {marginTop: 8}]}>{notification.getSubheader()}</Text>
                  </View>
                  <Ionicons name="chevron-forward" color={stylesheet.Tertiary} size={24} />
                </TouchableOpacity>
              )
            }

            return (
              <View key={index} style={[Styles.notificationContainer, {backgroundColor: stylesheet.SecondaryTint}]}>
                <View style={[styles.spacer, Styles.notificationContentContainer]}>
                  <Text style={[styles.baseText, styles.tertiary]}>{notification.getHeader()}</Text>
                  {
                    notification.hasSubheader() &&
                    <Text style={[styles.tinyText, styles.tertiary, styles.opaque, {marginTop: 8}]}>{notification.getSubheader()}</Text>
                  }
                </View>
              </View>
            )
          })
        }
      </ScrollView>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  notificationContainer: {
    width: '100%',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginBottom: 10
  }
});
