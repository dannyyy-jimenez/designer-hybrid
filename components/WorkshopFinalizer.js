import React from 'react';
import { View, SafeAreaView, Text, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();
let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

export default function WorkshopFinalizer({navigation, route}) {
  const SESSION = route.params.session;

  const [name, setName] = React.useState('');
  const [description, setDescription] = React.useState('');
  const type = route.params.type
  const mode = route.params.mode;
  const base = route.params.base;
  const metrics = route.params.metrics;
  const [isError, setIsError] = React.useState(false);

  const [isLoading, setIsLoading] = React.useState(false);

  const onAdd = () => {
    setIsLoading(true);
    setIsError(false);

    const body = {auth: SESSION.crypto, name, description, type, mode, base, metrics};
    API.post('portfolio/design', body).then(res => {
      setIsLoading(false);
      if (res.isError) {
        setIsError(true);
        return;
      }
      navigation.navigate('Portfolio', {SESSION, success: true});
    }).catch(() => {
      setIsLoading(false);
      setIsError(true);
    });
  };

  React.useEffect(() => {

  }, [useColorScheme()])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Workshop', {session: SESSION, type, mode})}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      <KeyboardAvoidingView behavior="padding" style={styles.defaultTabContainer}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            width: '100%'
          }}>
            <View style={{flex: 0.15}}></View>
            <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('almostDone')}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.centerText, styles.tertiary, styles.baseText]}>{Localization.t('bitMoreInfo')}</Text>
            <View style={{flex: 0.08}}></View>
            <Text style={[styles.centerText, styles.tertiary, styles.opaque, styles.tinyText]}>{Localization.t('designAppear')}</Text>
            {
              isError &&
              <>
                <View style={{flex: 0.08}}></View>
                <Text style={[styles.centerText, styles.primary, styles.baseText]}>{Localization.t('EuploadDesign')}</Text>
              </>
            }
            <View style={styles.spacer}></View>
            <TextInput onStartShouldSetResponder={() => true} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={name} autoFocus={true} onChangeText={value => setName(value)} keyboardType="default" placeholder={Localization.t('designName')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} textContentType="none" />
            <View style={{flex: 0.1}}></View>
            <TextInput onStartShouldSetResponder={() => true} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.inputMultiline, styles.filledInput, styles.tertiary]} value={description} onChangeText={value => setDescription(value)} keyboardType="default" multiline={true} placeholder={Localization.t('description')} maxLength={140} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} textContentType="none" />
            <View style={styles.spacer}></View>
            <TouchableOpacity
              disabled={isLoading || name.replace(/ /g, '').length === 0 || description.replace(/ /g, '').length === 0}
              style={[styles.roundedButton, styles.filled, isLoading || name.replace(/ /g, '').length === 0 || description.replace(/ /g, '').length === 0 ? styles.disabled : {opacity: 1}]}
              onPress={() => onAdd()}
              underlayColor='#fff'>
              <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('addToPortfolio')}</Text>
            </TouchableOpacity>
            <View style={{flex: 0.1}}></View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
