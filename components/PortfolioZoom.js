import React from 'react';
import { View, SafeAreaView, Platform, Dimensions, TextInput, ScrollView, Image, StyleSheet, Alert, TouchableOpacity, Text, Share } from 'react-native';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import * as Linking from 'expo-linking';
import * as MediaLibrary from 'expo-media-library';
import * as Permissions from 'expo-permissions';
import { useIsFocused } from '@react-navigation/native';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';
import ActionSheet from "react-native-actions-sheet";

import API from '../Api';
import env from '../env';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

const actionSheetRef = React.createRef();

export default function PortfolioZoom({navigation, route}) {
  const SESSION = route.params.session;

  const design = route.params.design;

  const [inBrand, setInBrand] = React.useState(design.hasDropped);
  const [remaining, setRemaining] = React.useState(-1);
  const [distribution, setDistribution] = React.useState(0);
  const [royalties, setRoyalties] = React.useState(0);
  const [price, setPrice] = React.useState('');
  const [hasBrand, setHasBrand] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [approved, setApproved] = React.useState(false);
  const [identifier, setIdentifier] = React.useState('');
  const [brandIdentifier, setBrandIdentifier] = React.useState('');
  const [currentShot, setCurrentShot] = React.useState(design.getCover());

  const [actionSheetSection, setActionSheetSection] = React.useState('');

  const [royalty, setRoyalty] = React.useState(0);
  const [cop, setCOP] = React.useState('0.00');

  // editables

  const [targetPricing, setTargetPricing] = React.useState('');

  React.useEffect(() => {

  }, [useColorScheme()])

  const onShare = () => {
    try {
      Share.share({
        url: `${env.baseUrl}/shop/zoom/${identifier}`,
        title: Localization.t('shareText')
      }, {tintColor: stylesheet.Primary});
    } catch (error) {
      console.log(error);
    }
  };

  const onShareBrand = () => {
    try {
      Share.share({
        url: `${env.baseUrl}/shop/brand/${brandIdentifier}`,
        title: Localization.t('shareText')
      }, {tintColor: stylesheet.Primary});
    } catch (error) {
      console.log(error);
    }
  };

  const askForCameraRollAsync = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
  }

  const onDownload = () => {
    for (let shot of design.shots) {
      MediaLibrary.saveToLibraryAsync(formats.cloudinarize(shot, 'b_rgb:FFFFFF/l_tailori:iconion-flat,g_south_east,x_10,y_10,w_90/') + '.png');
    }
    Alert.alert(
      Localization.t('shotsSaved'),
      '',
      [
        { text: Localization.t('cool'), onPress: () => {} }
      ],
      { cancelable: true }
    );
  };

  const onChain = async () => {
    const link = `https://www.tailorii.app/uploader/${brandIdentifier}/${design.id}`;
    const supported = await Linking.canOpenURL(link);

    if (supported) {
      await Linking.openURL(link);
    }
  };

  const onDelete = () => {
    setIsLoading(true);
    const body = {auth: SESSION.crypto, design: design.getID()};
    API.post('portfolio/remove', body).then((res) => {
      if (res.isError) throw 'error';

      navigation.goBack();
      setIsLoading(false);
    }).catch(e => setIsLoading(false));
  };

  const onWithhold = () => {
    setIsLoading(true);

    const body = {auth: SESSION.crypto, design: design.getID()};
    API.post('portfolio/withhold', body).then((res) => {
      if (res.isError) throw 'error';

      setInBrand(false);
      setDistribution(0);
      setRoyalties(0);
      setRemaining(-1);
      setPrice('');
      setIsLoading(false);
    }).catch(e => setIsLoading(false));
  };

  const onUpdatePricing = () => {
    actionSheetRef.current?.setModalVisible(true);
    setIsLoading(true);

    const body = {auth: SESSION.crypto, design: design.getID(), targetPrice: targetPricing};
    API.post('portfolio/pricing', body).then((res) => {
      if (res.isError) throw 'error';

      setPrice(res.data._p);
      setTargetPricing('');
      setRoyalty(res.data._r);
      setIsLoading(false);
      actionSheetRef.current?.setModalVisible(false);
    }).catch(e => setIsLoading(false));
  }

  React.useEffect(() => {
    if (actionSheetSection === '') {
      return;
    }

    actionSheetRef.current?.setModalVisible(true);
  }, [actionSheetSection])

  React.useEffect(() => {
    if (!navigation.isFocused()) {
      return;
    };

    API.get('portfolio/zoom', {auth: SESSION.crypto, design: design.getID()}).then((res) => {
      if (res.isError) return;
      setInBrand(res.data._iB);
      setHasBrand(res.data._hB);

      if (res.data._hB) {
        setBrandIdentifier(res.data._bI);
      }

      if (!res.data._iB) return;

      setDistribution(res.data._d);
      setRoyalties(res.data._rP);
      setRoyalty(res.data._rRA);
      setCOP(res.data._rCOP);
      setRemaining(res.data._r);
      setPrice(res.data._p);
      setApproved(res.data._rA);
      if (res.data._rA && res.data._iB) {
        setActionSheetSection('share');
      }
      setIdentifier(res.data._rI);
      setInBrand(res.data._iB);
    });
  }, [useIsFocused()]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          disabled={isLoading}
          style={isLoading ? styles.disabled : {opacity: 1}}
          onPress={() => onChain()}
          underlayColor='#fff'>
          <Ionicons name="link" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
        <View style={{flex: 0.1}}></View>
        {
          !inBrand &&
          <TouchableOpacity
            disabled={isLoading}
            style={isLoading ? styles.disabled : {opacity: 1}}
            onPress={() => onDelete()}
            underlayColor='#fff'>
            <Ionicons name="md-trash" size={24} color={stylesheet.Primary} />
          </TouchableOpacity>
        }
        {
          inBrand && approved &&
          <TouchableOpacity
            onPress={() => setActionSheetSection('share')}
            underlayColor='#fff'>
            <MaterialCommunityIcons name="share" size={24} color={stylesheet.Primary} />
          </TouchableOpacity>
        }
        {
          inBrand && approved &&
          <>
            <View style={{flex: 0.1}}></View>
            <TouchableOpacity
              onPress={() => onDownload()}
              underlayColor='#fff'>
              <Ionicons name="ios-images" size={24} color={stylesheet.Primary} />
            </TouchableOpacity>
          </>
        }
      </View>
      <View style={[styles.defaultTabContent, styles.spacer, styles.marginWidth]}>
        {
          !design.isMultiShot() &&
          <Image
            style={styles.defaultZoomImage}
            source={{ uri: formats.cloudinarize(design.getCover())}}
          />
        }
        {
          design.isMultiShot() &&
          <TouchableOpacity style={styles.defaultZoomImageContainer} onPress={() => setCurrentShot(design.getNextShot(currentShot))}>
            <Image
              style={styles.defaultZoomMultiImage}
              source={{ uri: formats.cloudinarize(currentShot)}}
            />
            <View style={[{position: 'absolute', left: '25%', bottom: 10, backgroundColor: stylesheet.Tertiary, width: '50%', height: 3, borderRadius: 3}]}>
              <View style={{position: 'absolute', width: `${100 / design.shots.length}%`, left: `${(100 / design.shots.length) * design.getShotIndex(currentShot)}%`, borderRadius: 3, backgroundColor: stylesheet.Primary, height: '100%'}}></View>
            </View>
          </TouchableOpacity>
        }
        <View style={[styles.defaultRowContainer, styles.fullWidth]}>
          <View style={[styles.fullHeight, styles.defaultColumnContainer]}>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.subHeaderText, styles.nunitoText, styles.tertiary]}>{design.getName()}</Text>
            <View style={{flex: 0.01}}></View>
            <Text style={[styles.baseText, styles.nunitoText, styles.tertiary, styles.opaque]}>{design.isReactive() ? <Text style={styles.primary}>{Localization.t('reactive')}&nbsp;</Text> : ''}{design.getType()}</Text>
          </View>
          <View style={styles.spacer}></View>
          <View style={[styles.defaultColumnContainer, styles.center]}>
            {
              inBrand && remaining === -1 && approved && price !== '' &&
              <>
                <Text onPress={() => setActionSheetSection('pricing')} style={[styles.subHeaderText, styles.tertiary, styles.opaque]}>${price}</Text>
                <Text onPress={() => setActionSheetSection('pricing')} style={[styles.baseText, styles.primary, {marginTop: 2}, styles.opaque]}>{Localization.t('edit')}</Text>
              </>
            }
            {
              inBrand && remaining === -1 && !approved && price !== '' &&
              <>
                <Text style={[styles.subHeaderText, styles.tertiary, styles.opaque]}>${price}</Text>
              </>
            }
            {
              inBrand && remaining > 0 && price !== '' &&
              <>
                <Text style={[styles.subHeaderText, styles.tertiary, styles.opaque]}>${price}</Text>
                <Text style={[styles.baseText, styles.primary, styles.opaque]}>{Localization.t('left', {amount: remaining})}</Text>
              </>
            }
            {
              inBrand && remaining === 0 &&
              <Text style={[styles.subHeaderText, styles.primary, styles.opaque]}>{Localization.t('CsoldOut')}</Text>
            }
          </View>
        </View>
        <View style={{flex: 0.3}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.opaque, styles.fullWidth]}>{design.getDescription()}</Text>
        {
          inBrand &&
          <>
            <View style={{flex: 0.2}}></View>
            <View style={[styles.defaultRowContainer, styles.fullWidth, {alignItems: 'center'}]}>
              <Ionicons name="md-shirt" color={stylesheet.Tertiary} size={24} />
              <View style={{flex: 0.03}}></View>
              <Text style={[styles.tertiary, styles.baseText]}>{distribution}</Text>
            </View>
            <View style={{flex: 0.1}}></View>
            <View style={[styles.defaultRowContainer, styles.fullWidth, {alignItems: 'center'}]}>
              <Ionicons name="md-cash" color={stylesheet.Tertiary} size={24} />
              <View style={{flex: 0.03}}></View>
              <Text style={[styles.tertiary, styles.baseText]}>${royalties}</Text>
            </View>
          </>
        }
        <View style={styles.spacer}></View>
        {
          !inBrand &&
          <TouchableOpacity
            disabled={!hasBrand}
            onPress={() => navigation.navigate('Release', {session: SESSION, designer: brandIdentifier, design: design.id, raw: design})}
            style={[styles.roundedButton, styles.filled, !hasBrand ? styles.disabled : {opacity: 1}]}
            underlayColor='#fff'>
            <Text style={[styles.secondary, styles.baseText, styles.centerText]}>{Localization.t('addToBrand')}</Text>
          </TouchableOpacity>
        }
        {
          inBrand && approved &&
          <TouchableOpacity
            disabled={isLoading}
            onPress={() => onWithhold()}
            style={[styles.roundedButton, styles.clear, isLoading ? styles.disabled : {opacity: 1}]}
            underlayColor='#fff'>
            <Text style={[styles.primary, styles.baseText, styles.centerText]}>{Localization.t('removeFromBrand')}</Text>
          </TouchableOpacity>
        }
        {
          inBrand && !approved &&
          <>
            <Text style={[styles.primary, styles.baseText, styles.centerText]}>{Localization.t('awaitingApproval')}</Text>
          </>
        }
        <View style={{flex: 0.05}}></View>
      </View>
      <ActionSheet onClose={() => setActionSheetSection('')} containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.StatusBar === 'light-content' ? stylesheet.Primary : stylesheet.Secondary}} defaultOverlayOpacity={0.85} indicatorColor={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        {
          actionSheetSection === 'pricing' &&
          <View>
            <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 10}]}>{Localization.t('targetPricing', {type: formats.getType(design.type)})}</Text>
            <Text style={[styles.baseText, styles.fullWidth, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 5}]}>{Localization.t('minTargetPrice', {cop: (parseFloat(cop) + 1)})}</Text>
            <View style={styles.line}></View>
            {
              (targetPricing === "" || targetPricing === 0) &&
              <Text style={[styles.baseText, styles.fullWidth, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 2, marginBottom: 20}]}>{Localization.t('currentRoyalty', {royalty})}</Text>
            }
            {
              targetPricing !== "" && targetPricing !== 0 &&
              <Text style={[styles.baseText, styles.fullWidth, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 2, marginBottom: 20}]}>{formats.getEstimatedRoyalty(design.type, targetPricing, cop)}</Text>
            }
            <TextInput keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.filledInput, {marginLeft: '7.5%', marginBottom: 10}, styles.tertiary]} value={targetPricing} onChangeText={value => setTargetPricing(value)} keyboardType="decimal-pad" placeholder={Localization.t('enterAmount')} placeholderTextColor="#555555" returnKeyType="none" selectionColor={stylesheet.Primary} textContentType="none" />

            <TouchableOpacity disabled={parseFloat(targetPricing) < (cop + 1) || isLoading} onPress={() => onUpdatePricing()} style={[styles.roundedButton, stylesheet.StatusBar === 'light-content' ? styles.filledTertiary : styles.filled, {marginLeft: '7.5%', marginTop: 20, marginBottom: 10}, parseFloat(targetPricing) < (cop + 1) || isLoading ? styles.disabled : {}]}>
              <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.primary : styles.secondary]}>{Localization.t('update')}</Text>
            </TouchableOpacity>
          </View>
        }
        {
          actionSheetSection === 'share' &&
          <View>
            <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, {marginTop: 10}]}><MaterialCommunityIcons name="share" size={36} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} /></Text>
            <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 10}]}>{Localization.t('shareInfo')}</Text>
            <View style={styles.line}></View>

            <TouchableOpacity onPress={() => onShare()} style={[styles.roundedButton, stylesheet.StatusBar === 'light-content' ? styles.filledTertiary : styles.filled, {marginLeft: '7.5%', marginTop: 20, marginBottom: 10}]}>
              <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.primary : styles.secondary]}>{Localization.t('share')}</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onShareBrand()} style={[styles.roundedButton, stylesheet.StatusBar === 'light-content' ? styles.filledPrimary : styles.clear, {marginLeft: '7.5%', marginTop: 20, marginBottom: 10}]}>
              <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary]}>{Localization.t('shareBrand')}</Text>
            </TouchableOpacity>
          </View>
        }
      </ActionSheet>
    </SafeAreaView>
  );
}
