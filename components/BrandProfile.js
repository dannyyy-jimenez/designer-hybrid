import React from 'react';

import { View, SafeAreaView, RefreshControl, Alert, ScrollView, Image, StyleSheet, TouchableOpacity, Text, TextInput } from 'react-native';
import { Ionicons, AntDesign, Octicons, FontAwesome5 } from '@expo/vector-icons';
import Localization from '../localization/config';
import Clipboard from 'expo-clipboard';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';
import Merch from '../models/Merch';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function BrandProfile({navigation, route}) {
  const SESSION = route.params.session;
  const designer = route.params.designer;
  const [isFavorite, setIsFavorite] = React.useState(route.params.designer.isFavorite);
  const [refreshing, setRefreshing] = React.useState(false);
  const [ideas, setIdeas] = React.useState(0);
  const [hearts, setHearts] = React.useState(0);
  const [deliveries, setDeliveries] = React.useState(0);
  const [yt, setYT] = React.useState(false);
  const [fb, setFB] = React.useState(false);
  const [ig, setIG] = React.useState(false);
  const [sc, setSC] = React.useState(false);
  const [merch, setMerch] = React.useState([]);

  const onCopy = async (username) => {
    Clipboard.setString(username);
    Alert.alert(
      Localization.t('usernameCopied'),
      username,
      [],
      { cancelable: true }
    );
  }

  React.useEffect(() => {

  }, [useColorScheme()])

  const toggleFavoriteBrand = async () => {
    setIsFavorite(!isFavorite);
    setRefreshing(true);

    try {
      const body = {auth: SESSION.crypto, designer: designer.identifier};
      const res =  await API.post('store/favorite', body);
      if (res.isError) throw 'error';

      setIsFavorite(res.data._f);
      setRefreshing(false);
    } catch (e) {
      console.log(e);
      setIsFavorite(!isFavorite);
      setRefreshing(false);
    };
  };

  const load = async () => {
    setRefreshing(true);

    try {
      const res =  await API.get('store/brand', {auth: SESSION.crypto, designer: designer.identifier});
      if (res.isError) throw 'error';

      setIsFavorite(res.data._f);
      setHearts(res.data._h);
      setIdeas(res.data._i);
      setDeliveries(res.data._d);

      setYT(res.data._sm._yt);
      setFB(res.data._sm._fb);
      setIG(res.data._sm._ig);
      setSC(res.data._sm._sc);

      setMerch(res.data._m.map(release => new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions)));
      setRefreshing(false);
    } catch (e) {
      setRefreshing(false);
    };
  }

  React.useEffect(() => {
    load();
  }, [])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{designer.identifier}</Text>
        <View style={styles.spacer}></View>
        {
          !isFavorite &&
          <View style={[styles.center]}>
            <TouchableOpacity
              onPress={() => toggleFavoriteBrand()}
              disabled={refreshing}
              underlayColor='#fff'>
              <Ionicons name="heart-outline" size={24} color={stylesheet.Primary} />
            </TouchableOpacity>
          </View>
        }
        {
          isFavorite &&
          <View style={[styles.center]}>
            <TouchableOpacity
              onPress={() => toggleFavoriteBrand()}
              disabled={refreshing}
              underlayColor='#fff'>
              <Ionicons name="md-heart" size={24} color={stylesheet.Primary} />
            </TouchableOpacity>
          </View>
        }
      </View>
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={[{alignItems: 'center', width: '90%', marginLeft: '5%', paddingTop: 10, justifyContent: 'center'}]} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={() => load(true)} />}>
        <View style={[styles.defaultRowContainer, styles.fullWidth, {marginTop: 20}]}>
          <Image style={styles.profileBrandLogo} source={{ uri: formats.cloudinarize(designer.logo, 'c_scale,w_300/'), cache: 'force-cache'}} />
          <View style={styles.spacer}></View>
          <View style={[styles.defaultColumnContainer, styles.center]}>
            <AntDesign name="bulb1" color={stylesheet.Primary} size={32} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.abbreviate(ideas)}</Text>
          </View>
          <View style={{flex: 0.5}}></View>
          <View style={[styles.defaultColumnContainer, styles.center]}>
            <AntDesign name="hearto" color={stylesheet.Primary} size={32} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.abbreviate(hearts)}</Text>
          </View>
          <View style={{flex: 0.5}}></View>
          <View style={[styles.defaultColumnContainer, styles.center]}>
            <Octicons name="package" color={stylesheet.Primary} size={32} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.abbreviate(deliveries)}</Text>
          </View>
          <View style={styles.spacer}></View>
        </View>
        <Text style={[styles.baseText, styles.fullWidth, styles.tertiary, styles.bold, {marginTop: 20}]}>{designer.name}</Text>
        <Text style={[styles.baseText, styles.tertiary, styles.fullWidth, {marginTop: 5, marginBottom: 15}]}>{designer.description}</Text>
        {
          (yt || ig || fb || sc) &&
          <View style={[styles.defaultRowContainer, styles.fullWidth, styles.center, {justifyContent: 'space-evenly'}, {marginTop: 15}]}>
            {
              yt &&
              <FontAwesome5 name="youtube" onPress={() => onCopy(yt)} color={stylesheet.PrimaryOpaque} size={32} />
            }
            {
              ig &&
              <FontAwesome5 name="instagram" onPress={() => onCopy(ig)} color={stylesheet.PrimaryOpaque} size={32} />
            }
            {
              fb &&
              <FontAwesome5 name="facebook-f" onPress={() => onCopy(fb)} color={stylesheet.PrimaryOpaque} size={32} />
            }
            {
              sc &&
              <FontAwesome5 name="snapchat-ghost" onPress={() => onCopy(sc)} color={stylesheet.PrimaryOpaque} size={32} />
            }
          </View>
        }
        {
          !refreshing && merch.length === 0 &&
          <Text style={[styles.opaque, styles.tertiary, {marginTop: 20}, styles.centerText, styles.fullWidth, styles.baseText]}>No releases at this moment :(</Text>
        }
        <View style={[styles.defaultRowContainer, {justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap', marginTop: 20}]}>
          {
            merch.map((release) => {
              return (
                <TouchableOpacity activeOpacity={1} key={release.getID()} onPress={() => navigation.navigate('Zoom', {session: SESSION, identifier: release.getIdentifier(), release: release})} style={styles.defaultStoreCardMini}>
                  <Image style={styles.defaultSCImage} source={{uri: formats.cloudinarize(release.getCover(), '', true)}} />
                  <View style={styles.defaultColumnContainer, styles.fullWidth, styles.featuredSCContent}>
                    <View style={{position: 'absolute', left: 0, bottom: 0, padding: 5, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 5, borderBottomLeftRadius: 5, backgroundColor: stylesheet.SecondaryTint}}>
                      <Text style={[styles.baseText, styles.primary, styles.bold, {marginTop: 2}]}>${release.getPrice()}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            })
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
