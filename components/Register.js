import React from 'react';
import { StyleSheet, Text, Platform, View, KeyboardAvoidingView, TextInput, TouchableWithoutFeedback, Keyboard, TouchableOpacity } from 'react-native';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function Register({navigation, route}) {
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [dob, setDob] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [validNumber, setValidNumber] = React.useState(false);
  const [validPersonal, setValidPersonal] = React.useState(false);
  const [validPassword, setValidPassword] = React.useState(false);
  const [currentSection, setCurrentSection] = React.useState(1);
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  const onPhoneNumberChange = (value) => {
    setPhoneNumber(formats.formatPhoneNumber(value));
    setValidNumber(value.length > 5);
    setError('');
  };

  const onDobChange = (value) => {
    setDob(formats.formatDob(value));
    setValidPersonal(value.length == 10 && lastName !== "" && firstName !== "");
    setError('');
  };

  const onFNChange = (value) => {
    setFirstName(value);
    setValidPersonal(value !== "" && lastName !== "" && dob !== "");
    setError('');
  }

  const onLNChange = (value) => {
    setLastName(value);
    setValidPersonal(value !== "" && firstName !== "" && dob !== "");
    setError('');
  }

  const onPasswordChange = (value) => {
    setPassword(value);
    setValidPassword(value.length > 5);
    setError('');
  }

  React.useEffect(() => {

  }, [useColorScheme()])

  const onRegister = () => {
    setIsLoading(true);
    Keyboard.dismiss();

    const body = { phoneNumber, firstName, lastName, password, dob };

    API.post('register', body).then(res => {
      if (res.isError) {
        setIsLoading(false);
        setError(res.response);
        if (res.response === "INVALID_NUM" || res.response === "DUP_NUMBER") {
          setCurrentSection(1);
          setValidNumber(false);
        }
        if (res.response === "PASS_LEN") {
          setCurrentSection(3);
          setValidPassword(false);
        }
        if (res.response === "AGE_ERR") {
          setCurrentSection(2);
          setValidPersonal(false);
        }
        return;
      }

      setPhoneNumber('');
      setValidNumber(false);
      setDob('');
      setFirstName('');
      setLastName('');
      setValidPersonal(false);
      setPassword('');
      setValidPassword('');
      navigation.navigate('Auth', {name: res.data.fullName, crypto: res.data.crypto});
      setIsLoading(false);
    }).catch(() => setIsLoading(false));
  };

  return (
    <KeyboardAvoidingView behavior="padding" style={styles.defaultTabContainer, {width: '100%', height: '100%'}}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          width: '100%'
        }}>
          {
            currentSection === 1 &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('phoneNumberPrompt')}</Text>
              {
                error === "DUP_NUMBER" &&
                <>
                  <View style={{flex: 0.04}}></View>
                  <Text style={[styles.centerText, styles.primary, styles.baseText]}>{Localization.t('EusedNumber')}</Text>
                </>
              }
              {
                error === "INVALID_NUM" &&
                <>
                  <View style={{flex: 0.04}}></View>
                  <Text style={[styles.centerText, styles.primary, styles.baseText]}>{Localization.t('EinvalidNumber')}</Text>
                </>
              }
              <View style={styles.spacer}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={phoneNumber} onChangeText={value => onPhoneNumberChange(value)} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} autoCorrect={true} keyboardType={(Platform.OS === 'ios') ? "numbers-and-punctuation" : "phone-pad"} placeholder={Localization.t('phoneNumber')} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} textContentType="telephoneNumber" />
              <View style={styles.spacer}></View>
              <TouchableOpacity
                disabled={!validNumber}
                style={[styles.roundedButton, styles.filled, !validNumber ? styles.disabled : null]}
                onPress={() => setCurrentSection(2)}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('continue')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            currentSection === 2 &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('dobPrompt')}</Text>
              {
                error === "AGE_ERR" &&
                <>
                  <View style={{flex: 0.04}}></View>
                  <Text style={[styles.centerText, styles.primary, styles.baseText]}>{Localization.t('Edob')}</Text>
                </>
              }
              <View style={styles.spacer}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} value={firstName} onChangeText={value => onFNChange(value)} autoCapitalize="words" keyboardType={(Platform.OS === 'ios') ? "numbers-and-punctuation" : "default"} placeholder={Localization.t('firstName')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} autoCorrect={true} textContentType="givenName" />
              <View style={{flex: 0.1}}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} value={lastName} onChangeText={value => onLNChange(value)} autoCapitalize="words" keyboardType={(Platform.OS === 'ios') ? "numbers-and-punctuation" : "default"} placeholder={Localization.t('lastName')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} autoCorrect={true} textContentType="familyName" />
              <View style={{flex: 0.1}}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} value={dob} onChangeText={value => onDobChange(value)} keyboardType="decimal-pad" placeholder={Localization.t('dob')} maxLength={10} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} />
              <View style={styles.spacer}></View>
              <TouchableOpacity
                disabled={!validPersonal}
                style={[styles.roundedButton, styles.filled, !validPersonal ? styles.disabled : null]}
                onPress={() => setCurrentSection(3)}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('continue')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            currentSection === 3 &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('securePasswordPrompt')}</Text>
              {
                error === "PASS_LEN" &&
                <>
                  <View style={{flex: 0.04}}></View>
                  <Text style={[styles.centerText, styles.primary, styles.baseText]}>{Localization.t("EpassLen")}</Text>
                </>
              }
              <View style={styles.spacer}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} maxLength={16} value={password} onChangeText={value => onPasswordChange(value)} placeholder={Localization.t('password')} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} secureTextEntry={true} textContentType="newPassword" passwordrules="required: lower; required: upper; required: digit; minlength: 5; maxlength: 16;" />
              <View style={styles.spacer}></View>
              <TouchableOpacity
                disabled={!validPassword || isLoading}
                style={[styles.roundedButton, styles.filled, !validPassword || isLoading ? styles.disabled : {opacity: 1}]}
                onPress={() => onRegister()}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('register')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.tinyText, styles.paddedSides]}>{Localization.t('agreement')} <Text onPress={() => navigation.navigate('Document', {name: 'terms'})} style={styles.primary}>{Localization.t('terms')}</Text> & <Text onPress={() => navigation.navigate('Document', {name: 'privacy'})} style={styles.primary}>{Localization.t('privacy')}</Text></Text>
              <View style={{flex: 0.15}}></View>
            </>
          }
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}
