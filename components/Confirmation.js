import React from 'react';
import { View, SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

export default function Confirmation({navigation, route}) {
  const { orderNumber, supportedModel } = route.params;

  React.useEffect(() => {

  }, [useColorScheme()])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.navigate('Checkout')}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      <View style={styles.defaultTabContent}>
        <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('orderPlaced')}</Text>
        <View style={{flex: 0.05}}></View>
        <Text selectable={true} style={[styles.centerText, styles.tertiary, styles.baseText]}>{Localization.t('orderConfirm')} <Text selectable={true} style={styles.primary}>#{orderNumber}</Text></Text>
        <View style={{flex: 0.05}}></View>
        <Text style={[styles.centerText, styles.tertiary, styles.baseText, styles.marginWidth]}>{Localization.t('orderUpdates')}</Text>
        {
          supportedModel &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.centerText, styles.primary, styles.bold, styles.baseText]}>{Localization.t('modelThank')}</Text>
          </>
        }
      </View>
    </SafeAreaView>
  );
}
