import React from 'react';
import { View, SafeAreaView, Text, ScrollView, RefreshControl, ActivityIndicator, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native';
import { Ionicons, FontAwesome5, AntDesign } from '@expo/vector-icons';
import { useIsFocused } from '@react-navigation/native';
import Swipeable from 'react-native-swipeable-row';
import * as Haptics from 'expo-haptics';
import Localization from '../localization/config';
import ActionSheet from "react-native-actions-sheet";
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';
import OrderItem from '../models/OrderItem';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

const actionSheetRef = React.createRef();

export default function Checkout({navigation, route}) {
  const SESSION = route.params.session;

  const [isLoading, setIsLoading] = React.useState(true);
  const [cart, setCart] = React.useState([]);
  const [streetAddress, setStreetAddress] = React.useState('');
  const [streetAddressCont, setStreetAddressCont] = React.useState('');
  const [city, setCity] = React.useState('');
  const [state, setState] = React.useState('');
  const [last4, setLast4] = React.useState('3577');
  const [postalCode, setPostalCode] = React.useState('');
  const [country, setCountry] = React.useState('');
  const [canOrder, setCanOrder] = React.useState(false);
  const [subtotal, setSubtotal] = React.useState('');
  const [tax, setTax] = React.useState('');
  const [shipping, setShipping] = React.useState('');
  const [total, setTotal] = React.useState('');
  const [error, setError] = React.useState('');
  const [modelCode, setModelCode] = React.useState('');
  const [modelMode, setModelMode] = React.useState(false);
  const [modelError, setModelError] = React.useState(false);
  const [discount, setDiscount] = React.useState('');
  const [discountType, setDiscountType] = React.useState('');
  const [validPayment, setValidPayment] = React.useState(false);
  const [validShipping, setValidShipping] = React.useState(false);
  const [leftSheetOpen, setLeftSheetOpen] = React.useState(false);

  let actionSheet;

  React.useEffect(() => {

  }, [useColorScheme()])

  const onCheckout = () => {
    actionSheetRef.current?.setModalVisible()
    setLeftSheetOpen(true);
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Small);
  }

  const onPay = async () => {
    setIsLoading(true);
    actionSheetRef.current?.setModalVisible(false)

    const body = {auth: SESSION.crypto, modelCode};

    try {
      const res = await API.post('store/checkout', body);
      if (res.isError) {
        if (res.response === 'INEX_RELEASE') {
          setError(Localization.t('EinexCartItem'));
        }
        if (res.response === 'PAYMENT_ERROR') {
          setError(Localization.t('EpaymentError'));
        }
        if (res.response === 'BAD_SHIP') {
          setError(Localization.t('EinvalidShip'));
        }
        if (res.response === 'INEX_LIM_RELEASE') {
          setError(Localization.t('EexceededRelease'));
        }
        setCanOrder(false);
        setIsLoading(false);
        return;
      }
      setCanOrder(false);
      setStreetAddress("");
      setStreetAddressCont("");
      setCity("");
      setState("");
      setPostalCode("");
      setCountry("");
      setLeftSheetOpen(false);
      navigation.navigate('Confirmation', {orderNumber: res.data._o, supportedModel: modelCode !== ''})
    } catch (e) {
      setIsLoading(false);
    }
  }

  const loadCart = async () => {
    setIsLoading(true);
    setError('');
    setCanOrder(false);
    setValidShipping(false);

    try {
      const res =  await API.get('store/cart', {auth: SESSION.crypto});
      if (res.isError) throw 'error';

      setCart(res.data._c.map(item => new OrderItem(item.id, item.release, item.quantity, item.size, item.color, item.total)));
      setSubtotal(res.data._st);
      setTax(res.data._tx);
      setShipping(res.data._s);
      setTotal(res.data._t);
      setValidPayment(res.data._p);
      setLast4(res.data._last4);

      if (res.data._sd.v) {
        setStreetAddress(res.data._sd._sa);
        setStreetAddressCont(res.data._sd._sac);
        setCity(res.data._sd._c);
        setState(res.data._sd._s);
        setPostalCode(res.data._sd._p);
        setCountry(res.data._sd._cn);
        setValidShipping(true);

        if (res.data._c.length > 0 && res.data._p) {
          setCanOrder(true);
          if (leftSheetOpen) {
            onCheckout();
          }
        }
      }
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
    }
  }

  const removeItem = async (item, index) => {
    if (isLoading) return;
    setIsLoading(true);

    try {
      const res = await API.post('store/unbag', {auth: SESSION.crypto, item, index});
      if (res.isError) throw 'error';

      setDiscount('');
      setDiscountType('');
      setModelCode('');
      await loadCart();
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Small);
    } catch (e) {
      setIsLoading(false);
    }
  }

  const onValidateModel = () => {
    setIsLoading(true);

    const body = {auth: SESSION.crypto, modelCode};

    API.post('store/model', body).then(res => {
      if (res.isError) throw 'error';

      setModelError(false);
      setModelMode(false);
      setIsLoading(false);

      setDiscountType(res.data._dt);
      setDiscount(res.data._discount);
      setTax(res.data._tx);
      setTotal(res.data._t);
      setShipping(res.data._s);
    }).catch(e => {
      console.log(e);
      setModelError(true);
      setIsLoading(false);
    });
  };

  const onModelCodeChange = async (value) => {
    if (discount !== '') {
      setDiscount('');
      setDiscountType('');
      setIsLoading(true);
      await loadCart();
      setIsLoading(false);
    }
    setModelCode(value.toUpperCase());
    setModelError(false);
  };

  const onSheetClose = () => {
    if (navigation.isFocused()) {
      setLeftSheetOpen(false);
    };
  }

  React.useEffect(() => {
    if (!navigation.isFocused()) {
      actionSheetRef.current?.setModalVisible(false)
      return;
    };

    setIsLoading(true);
    loadCart();
  }, [useIsFocused()]);

  const rightButtons = [
    <View style={{width: 100, height: '100%', justifyContent: 'center', alignItems: 'center'}}>
      <Text style={[styles.baseText, styles.fullWidth, styles.centerText, styles.primary]}>{Localization.t('remove')}</Text>
    </View>
  ];

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('checkout')}</Text>
        <View style={styles.spacer}></View>
      </View>
      <KeyboardAvoidingView style={styles.defaultTabContent} behavior="padding">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView showsVerticalScrollIndicator={false} style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', minHeight: '100%', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}} refreshControl={<RefreshControl refreshing={isLoading} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={() => loadCart()} />}>
            <View style={{flex: 0.05}}></View>
            {
            cart.length === 0 &&
              <>
                <View style={styles.spacer}></View>
                <Text style={[styles.centerText, styles.tertiary, styles.opaque, styles.fullWidth, {marginBottom: 20}, styles.subHeaderText]}>{Localization.t('emptyCart')}</Text>
                <TouchableOpacity
                  style={[styles.roundedButton, styles.filled, {marginLeft: '7.5%'}]}
                  onPress={() => navigation.navigate('Store')}
                  underlayColor='#fff'>
                  <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('visitShop')}</Text>
                </TouchableOpacity>
                <View style={styles.spacer}></View>
              </>
            }
            {
              cart.length !== 0 &&
              <Text style={[styles.centerText, styles.tertiary, styles.opaque, styles.fullWidth, {marginTop: 10, marginBottom: 10}, styles.baseText]}>{Localization.t('swipeRemove')}</Text>
            }
            {
              cart.map((item, index) => {
                return (
                  <Swipeable rightButtons={rightButtons} onRightActionRelease={() => removeItem(item.getID(), index)} rightActionActivationDistance={50} rightButtonWidth={100}>
                    <TouchableOpacity key={item.getID()} onPress={() => navigation.navigate('Zoom', {session: SESSION, identifier: item.release.getIdentifier(), release: item.release})} style={[styles.defaultRowContainer, {marginTop: 10, marginBottom: 10, backgroundColor: stylesheet.SecondaryTint}, styles.fullWidth, Styles.itemContainer]}>
                      <Image style={Styles.itemImage} source={{uri: formats.cloudinarize(item.release.getCover())}} />
                      <View style={[styles.spacer, {marginLeft: 10}, styles.defaultColumnContainer, styles.fullHeight]}>
                        <View style={styles.spacer}></View>
                        <Text style={[styles.baseText, styles.tertiary]}>{item.release.getName()}</Text>
                        <View style={{flex: 0.05}}></View>
                        <Text style={[styles.tinyText, styles.tertiary]}>{Localization.t('size')}: {formats.formatSize(item.getSize())}</Text>
                        <View style={{flex: 0.05}}></View>
                        <Text style={[styles.tinyText, styles.tertiary]}>{Localization.t('color')}: {item.getColor()}</Text>
                        <View style={styles.spacer}></View>
                        <Text style={[styles.tinyText, styles.tertiary, styles.opaque]}>{item.quantity} x ${item.release.getPrice()} </Text>
                        <View style={{flex: 0.1}}></View>
                      </View>
                      <View style={{flex: 0.01}}></View>
                      <View style={[styles.defaultColumnContainer, styles.fullHeight]}>
                        <View style={styles.spacer}></View>
                        <Text style={[styles.tertiary, styles.opaque, styles.baseText, {display: 'flex', justifyContent: 'center', alignItems: 'center'}]}>${item.total}</Text>
                        <View style={styles.spacer}></View>
                      </View>
                      <View style={{flex: 0.02}}></View>
                    </TouchableOpacity>
                  </Swipeable>
                )
              })
            }
            {
              cart.length > 0 &&
              <>
                <View style={{marginTop: 20, marginBottom: 20}}></View>
                <View style={styles.line}></View>
                <TouchableOpacity
                  style={[styles.fullWidth]}
                  onPress={() => setModelMode(!modelMode)}
                  underlayColor='#fff'>
                  <>
                    <Text style={[styles.primary, styles.baseText, styles.fullWidth, styles.bold]}>{Localization.t('modelCodePrompt')}</Text>
                    {
                      modelCode !== "" && !modelMode &&
                      <Text style={[styles.primary, styles.baseText, styles.fullWidth, styles.opaque]}>{modelCode}</Text>
                    }
                    {
                      modelMode &&
                      <>
                        {
                          modelError &&
                          <>
                            <Text style={[styles.centerText, styles.primary, styles.marginWidth, styles.baseText, {marginTop: 10}]}>{Localization.t('EinvalidModel')}</Text>
                          </>
                        }
                        <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary, {marginLeft: '7.5%', marginTop: 15}]} value={modelCode.toUpperCase()} onChangeText={value => onModelCodeChange(value)} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} autoCapitalize="characters" placeholder={Localization.t('modelCode')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary}/>
                        <TouchableOpacity
                          disabled={modelCode === ''}
                          style={[styles.fullWidth, {marginTop: 15, padding: 5}]}
                          onPress={() => onValidateModel()}
                          underlayColor='#fff'>
                          <Text style={[styles.primary, styles.baseText, styles.centerText, styles.bold, modelCode === '' ? styles.disabled : {},]}>{Localization.t('applyCode')}</Text>
                        </TouchableOpacity>
                      </>
                    }
                  </>
                </TouchableOpacity>
                <View style={styles.line}></View>
                <View style={[styles.defaultColumnContainer, styles.fullWidth]}>
                  <View style={[styles.defaultRowContainer, styles.fullWidth, {marginTop: 5}]}>
                    <Text style={[styles.opaque, styles.baseText, styles.tertiary]}>{Localization.t('subtotal')}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.opaque, styles.baseText, styles.tertiary]}>${subtotal}</Text>
                  </View>
                  {
                    discount !== '' && discountType !== '$1 Shipping' &&
                    <>
                      <View style={{flex: 0.05}}></View>
                      <View style={[styles.defaultRowContainer, {paddingLeft: 10}]}>
                        <Text style={[styles.opaque, styles.baseText, styles.primary]}>{Localization.t('modelGift')} <Text style={styles.tinyText}>({discountType})</Text></Text>
                        <View style={styles.spacer}></View>
                        <Text style={[styles.opaque, styles.baseText, styles.primary]}>-${discount}</Text>
                      </View>
                      <View style={{flex: 0.1}}></View>
                    </>
                  }
                  <View style={[styles.defaultRowContainer, styles.fullWidth, {marginTop: 5}]}>
                    <Text style={[styles.opaque, styles.baseText, styles.tertiary]}>{Localization.t('shipping')}</Text>
                    <View style={styles.spacer}></View>
                    {
                      shipping === '0.00' &&
                      <Text style={[styles.opaque, styles.baseText, styles.tertiary]}>{Localization.t('Cfree')}</Text>
                    }
                    {
                      shipping !== '0.00' &&
                      <Text style={[styles.opaque, styles.baseText, styles.tertiary]}>${shipping}</Text>
                    }
                  </View>
                  {
                    discount !== '' && discountType === '$1 Shipping' &&
                    <>
                      <View style={{flex: 0.05}}></View>
                      <View style={[styles.defaultRowContainer, {paddingLeft: 10}]}>
                        <Text style={[styles.opaque, styles.baseText, styles.primary]}>{Localization.t('modelGift')} <Text style={styles.tinyText}>({discountType})</Text></Text>
                        <View style={styles.spacer}></View>
                        <Text style={[styles.opaque, styles.baseText, styles.primary]}>-${discount}</Text>
                      </View>
                      <View style={{flex: 0.1}}></View>
                    </>
                  }
                  <View style={{flex: 0.05}}></View>
                  <View style={[styles.defaultRowContainer, styles.fullWidth, {marginTop: 5}]}>
                    <Text style={[styles.opaque, styles.baseText, styles.tertiary]}>{Localization.t('taxes')}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.opaque, styles.baseText, styles.tertiary]}>${tax}</Text>
                  </View>
                  <View style={[styles.defaultRowContainer, styles.fullWidth, {marginTop: 15}]}>
                    <Text style={[styles.opaque, styles.baseText, styles.bold, styles.primary]}>{Localization.t('orderTotal')}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.opaque, styles.baseText, styles.bold, styles.primary]}>${total}</Text>
                  </View>
                  <View style={{flex: 0.1}}></View>
                </View>
                <TouchableOpacity disabled={isLoading} onPress={() => onCheckout()} style={[styles.roundedButton, styles.fullWidth, styles.filled, {marginTop: 40}, isLoading ? styles.disabled : {}]}>
                  <Text style={[styles.baseText, styles.secondary]}>{Localization.t('checkout')}</Text>
                </TouchableOpacity>
                {
                  error !== "" &&
                  <Text style={[styles.centerText, styles.primary, styles.baseText, styles.fullWidth, styles.centerText, {marginTop: 10}]}>{error}</Text>
                }
              </>
            }
            <ActionSheet onClose={() => onSheetClose()} containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.StatusBar === 'light-content' ? stylesheet.Primary : stylesheet.Secondary}} indicatorColor={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
              <View>
                <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 10}]}>{Localization.t('confirmOrder')}</Text>
                <View style={styles.line}></View>
                <TouchableOpacity onPress={() => navigation.navigate('Address')} style={[styles.defaultRowContainer, Styles.paddedWidth]}>
                  <View style={[styles.defaultColumnContainer, styles.spacer]}>
                    <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.bold]}>{Localization.t('address')}</Text>
                    {
                      !validShipping &&
                      <Text style={[styles.tinyText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.bold, styles.opaque, {marginTop: 5}]}>{Localization.t('tapShippingPrompt')}</Text>
                    }
                    {
                      validShipping &&
                      <Text style={[styles.tinyText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.opaque, {marginTop: 5}]}>{streetAddress} {streetAddressCont}, {city} {state} {postalCode}</Text>
                    }
                  </View>
                  <View style={[styles.defaultColumnContainer]}>
                    <Ionicons name="chevron-forward" size={24} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} />
                  </View>
                </TouchableOpacity>
                <View style={styles.line}></View>
                <TouchableOpacity onPress={() => navigation.navigate('Payment')} style={[styles.defaultRowContainer, Styles.paddedWidth]}>
                  <View style={[styles.defaultColumnContainer, styles.spacer]}>
                    <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.bold]}>{Localization.t('payment')}</Text>
                    {
                      !validPayment &&
                      <Text style={[styles.tinyText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.bold, styles.opaque, {marginTop: 5}]}>{Localization.t('tapPaymentPrompt')}</Text>
                    }
                    {
                      validPayment &&
                      <Text style={[styles.tinyText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.opaque, {marginTop: 5}]}>**** **** **** {last4}</Text>
                    }
                  </View>
                  <View style={[styles.defaultColumnContainer]}>
                  <Ionicons name="chevron-forward" size={24} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} />
                  </View>
                </TouchableOpacity>
                <View style={styles.line}></View>
                <Text style={[styles.baseText, Styles.paddedWidth, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.bold]}>{Localization.t('purchaseSummary')} - ${total}</Text>
                <Text style={[styles.tinyText, Styles.paddedWidth, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.opaque, {marginTop: 5}]}>{Localization.t('subtotal')}: ${subtotal}</Text>
                <Text style={[styles.tinyText, Styles.paddedWidth, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.opaque, {marginTop: 5}]}>{Localization.t('shipping')}: ${shipping}</Text>
                <Text style={[styles.tinyText, Styles.paddedWidth, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.opaque, {marginTop: 5}]}>{Localization.t('taxes')}: ${tax}</Text>
                <View style={styles.line}></View>
                <Text style={[stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, styles.opaque, styles.tinyText, Styles.marginWidth, styles.centerText, {marginTop: 5}]}>{Localization.t('noRefunds')}</Text>

                <TouchableOpacity disabled={!canOrder} onPress={() => onPay()} style={[styles.roundedButton, Styles.paddedWidth, stylesheet.StatusBar === 'light-content' ? styles.filledTertiary : styles.filled, !canOrder ? styles.disabled : {opacity: 1}, {marginTop: 25}]}>
                  <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.primary : styles.secondary]}>{Localization.t('pay')}</Text>
                </TouchableOpacity>
              </View>
            </ActionSheet>
            <View style={{flex: 0.1}}></View>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}


const Styles = StyleSheet.create({
  itemContainer: {
    height: 120,
    borderRadius: 20,
    padding: 5
  },
  paddedWidth: {
    width: '80%',
    marginLeft: '10%'
  },
  itemImage: {
    height: '100%',
    width: '20%',
    resizeMode: 'contain'
  }
});
