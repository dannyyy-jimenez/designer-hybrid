import React from 'react';
import { View, SafeAreaView, Text, Switch, ActivityIndicator, Linking, Keyboard, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function BrandStarter({navigation, route}) {
  const SESSION = route.params.session;

  const [currentSection, setCurrentSection] = React.useState(1);
  const [name, setName] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [logo, setLogo] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [accountLink, setAccountLink] = React.useState('');

  const onConnect = async () => {
    setIsLoading(true);
    const body = {auth: SESSION.crypto};
    API.post('brand/verify', body).then(res => {
      setIsLoading(false);
      if (res.isError) {
        return;
      }
      navigation.goBack();
    }).catch((e) => setIsLoading(false));
  };

  React.useEffect(() => {

  }, [useColorScheme()])

  const onCreate = async () => {
    setIsLoading(true);
    const body = {auth: SESSION.crypto, name, description, logo};

    API.post('brand/starter', body).then(res => {
      setIsLoading(false);
      if (res.isError) {
        setError(true);
        return;
      }

      setAccountLink(res.data._u);

      setCurrentSection(3);
    }).catch((e) => setIsLoading(false));
  };

  React.useEffect(() => {
    if (currentSection === 3) {
      setTimeout(() => {  
        Linking.openURL(accountLink);
        setTimeout(() => {
          setCurrentSection(4);
        }, 3000);
      }, 1500);
    }
  }, [currentSection])

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert(Localization.t('EcameraRoll'));
      }
    }
  };

  const pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
        base64: true,
        exif: true
      });
      if (!result.cancelled) {
        setLogo(result.base64);
      } else {
        setLogo('');
      }
    } catch (E) {
      console.log(E);
    }
  };

  React.useEffect(() => {
    getPermissionAsync();
  });

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.navigate('Brand')}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      <KeyboardAvoidingView behavior="height" style={styles.defaultTabContainer}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            width: '100%'
          }}>
            {
              currentSection === 1 &&
              <>
                <View style={{flex: 0.15}}></View>
                <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('nameBrand')}</Text>
                <View style={styles.spacer}></View>
                <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={name} onChangeText={value => setName(value)} autoFocus={true} keyboardType="default" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} placeholder={Localization.t('brandName')} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} textContentType="none" />
                <View style={styles.spacer}></View>
                <TouchableOpacity
                  disabled={name.length === 0}
                  style={[styles.roundedButton, styles.filled, name.length === 0 ? styles.disabled : {opacity: 1}]}
                  onPress={() => setCurrentSection(2)}
                  underlayColor='#fff'>
                  <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('next')}</Text>
                </TouchableOpacity>
                <View style={{flex: 0.1}}></View>
              </>
            }
            {
              currentSection === 2 &&
              <>
                <View style={{flex: 0.15}}></View>
                <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('describeBrand')}</Text>
                <View style={{flex: 0.1}}></View>
                <Text style={[styles.centerText, styles.tertiary, styles.tinyText]}>{Localization.t('sizeRec')}</Text>
                <View style={styles.spacer}></View>
                <TextInput style={[styles.baseInput, styles.inputMultiline, styles.filledInput, styles.tertiary]} value={description} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} onChangeText={value => setDescription(value)} autoFocus={true} keyboardType="default" multiline={true} placeholder={Localization.t('description')} maxLength={140} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} textContentType="none" />
                <View style={{flex: 0.1}}></View>
                <TouchableOpacity
                  style={[styles.roundedButton, styles.clear]}
                  onPress={() => pickImage()}
                  underlayColor='#fff'>
                  {
                    logo !== '' &&
                    <Ionicons name="md-checkmark-circle-outline" size={24} color={stylesheet.Primary}/>
                  }
                  {
                    logo === '' &&
                    <Text style={[styles.primary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('selectLogo')}</Text>
                  }
                </TouchableOpacity>
                <View style={styles.spacer}></View>
                <TouchableOpacity
                  disabled={description.length === 0 || logo === '' || isLoading}
                  style={[styles.roundedButton, styles.filled, description.length === 0 || logo === '' || isLoading ? styles.disabled : {opacity: 1}]}
                  onPress={() => onCreate()}
                  underlayColor='#fff'>
                  <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('next')}</Text>
                </TouchableOpacity>
                <View style={{flex: 0.1}}></View>
              </>
            }
            {
              currentSection === 3 &&
              <>
                <View style={styles.spacer}></View>
                <ActivityIndicator size="small" color={stylesheet.Primary} />
                <View style={{flex: 0.05}}></View>
                <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('redirecting')}</Text>
                <View style={{flex: 0.05}}></View>
                <Text style={[styles.centerText, styles.marginWidth, styles.tertiary, styles.baseText]}>{Localization.t('redirectPrompt')}</Text>
                <View style={styles.spacer}></View>
                <View style={{flex: 0.1}}></View>
              </>
            }
            {
              currentSection === 4 &&
              <>
                <View style={styles.spacer}></View>
                <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('oneLastStep')}</Text>
                <View style={{flex: 0.05}}></View>
                <Text style={[styles.centerText, styles.marginWidth, styles.tertiary, styles.baseText]}>{Localization.t('brandActivatePrompt')}</Text>
                <View style={styles.spacer}></View>
                <TouchableOpacity
                  onPress={() => onConnect()}
                  style={[styles.roundedButton, styles.filled, false ? styles.disabled : null]}
                  underlayColor='#fff'>
                  <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('didIt')}</Text>
                </TouchableOpacity>
                <View style={{flex: 0.1}}></View>
              </>
            }
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
