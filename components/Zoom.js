import React from 'react';
import { View, SafeAreaView, Dimensions, ScrollView, Image, StyleSheet, TouchableOpacity, Text, TextInput } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import * as Haptics from 'expo-haptics';
import Localization from '../localization/config';
import ActionSheet from "react-native-actions-sheet";
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();
let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

const actionSheetRef = React.createRef();

export default function Zoom({navigation, route}) {
  const SESSION = route.params.session;

  const { identifier, release } = route.params;
  const [itemInCart, setItemInCart] = React.useState(false);
  const [favoriteBrand, setFavoriteBrand] = React.useState(release.designer.isFavorite);
  const [isLoading, setIsLoading] = React.useState(false);
  const [isSoldOut, setIsSoldOut] = React.useState(release.isSoldOut());
  const [isUnavailable, setIsUnavailable] = React.useState(false);
  const [currentShot, setCurrentShot] = React.useState(release.getCover());
  const [baseColor, setBaseColor] = React.useState('BLCK');
  const [shots, setShots] = React.useState([]);
  let [baseShots, setBaseShots] = React.useState([]);

  const [size, setSize] = React.useState('default');
  const [color, setColor] = React.useState('default');
  const [quantity, setQuantity] = React.useState(1);
  const [actions, setActions] = React.useState(release.actions.slice());
  const [canAdd, setCanAdd] = React.useState(false);
  const [firstLoad, setFirstLoad] = React.useState(true);

  const [colors, setColors] = React.useState([]);
  const [showAllColors, setShowAllColors] = React.useState(false);
  const [sizes, setSizes] = React.useState([]);
  const [reactives, setReactives] = React.useState(release.reactives.map(reactive => {return {...reactive, set: false}}));
  const [reactivesSet, setReactivesSet] = React.useState(false);

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }

    let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!");
      return;
    }
  };

  const onSizePick = (size) => {
    setSize(size);
  };
  const onColorPick = (color) => {
    setColor(color);
  };

  const onReactiveImageChoose = async (index) => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: false,
        quality: 1,
        exif: false,
        base64: true
      });
      if (!result.cancelled) {

        let metric = {
          ...reactives[index],
          file: result.uri,
          uri: result.base64,
          set: true
        };

        let copy = reactives.slice();
        copy[index] = metric;
        setReactives(copy);
        setReactivesSet(copy.find(side => !side.set) == null);
      }
    } catch (E) {
      console.log(E);
    }
  };

  React.useEffect(() => {

  }, [useColorScheme()])

  const onReactiveTextChange = async (value, index) => {
    let copy = reactives.slice();
    copy[index].artwork = value;
    copy[index].set = true;
    setReactives(copy);
    setReactivesSet(copy.find(side => !side.set) == null);
  }

  const onSelect = async () => {
    setIsLoading(true);

    try {
      if (size === '' || color === '' || quantity < 1) throw 'NO';

      const body = {auth: SESSION.crypto, release: identifier, size, color, quantity, reactives};
      const res =  await API.post('store/bag', body);
      if (res.isError) throw 'error';

      actionSheetRef.current?.setModalVisible(true);
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Hard);

      setIsLoading(false);
      setItemInCart(true);
      setSize('default');
      setColor('default');
      setReactives(release.reactives.map(reactive => {return {...reactive, artwork: null, set: false}}));
      setReactivesSet(false);
    } catch (e) {
      setIsLoading(false);
    };
  };

  const onReviewCart = () => {
    navigation.navigate('Checkout');
    actionSheetRef.current?.setModalVisible(false);
  }

  const getShotIndex = (currentShot) => {
    if (shots.length === 1) return 0;

    return shots.indexOf(currentShot);
  }

  const getNextShot = (currentShot, number = false) => {
    if (shots.indexOf(currentShot) === shots.length - 1) {
      if (number) return 0;
      return shots[0];
    }

    if (number) return shots.indexOf(currentShot) + 1;
    return shots[shots.indexOf(currentShot) + 1];
  }

  React.useEffect(() => {
    API.get('store/zoom', {auth: SESSION.crypto, release: identifier}).then((res) => {
      if (res.isError) {
        if (res.response === "INEX_RELEASE") {
          setIsUnavailable(true);
        }
        return;
      };

      setBaseColor(res.data._bc);
      setBaseShots(res.data._r.design.shots);
      setCurrentShot(res.data._r.design.shots[0]);
      setShots(res.data._r.design.shots);
      setColor(res.data._bc);
      setIsSoldOut(res.data._r.remaining == 0);
    }).catch(e => {});
  }, []);

  React.useEffect(() => {
    if (actions.includes('sizeable') && size === 'default') {
      setCanAdd(false);
      return;
    }

    if (actions.includes('reactive') && !reactivesSet) {
      setCanAdd(false);
      return;
    }

    setCanAdd(true);
  }, [size, color, reactivesSet])

  React.useEffect(() => {
    setColors(formats.getAvailableColors(release.type).filter(c => c.prioritize));
    setSizes(formats.getAvailableSizes(release.type));
  }, []);

  const onColorsToggle = (showAll) => {
    setShowAllColors(showAll);

    if (showAll) {
      setColors(formats.getAvailableColors(release.type));
      return;
    }

    setColors(formats.getAvailableColors(release.type).filter(c => c.prioritize));
  }

  React.useEffect(() => {
    if (color === 'default' || color === baseColor) {
      if (baseShots.length > 0) {
        setCurrentShot(baseShots[0]);
        setShots(baseShots);
      }
      return;
    }

    setIsLoading(true);

    API.get('store/zoom/color', {auth: SESSION.crypto, release: release.identifier, color: color}).then((res) => {
      if (res.isError) throw 'error';

      setCurrentShot(res.data._s[0]);
      setShots(res.data._s);
      setIsLoading(false);
    }).catch(e => {      setIsLoading(false);
    });

  }, [color]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        {
          !isUnavailable &&
          <Text style={[styles.baseText, styles.bold, styles.tertiary]}>#{identifier}</Text>
        }
        {
          isUnavailable &&
          <Text style={[styles.baseText, styles.bold, styles.primary]}>UNAVAILABLE</Text>
        }
        <View style={styles.spacer}></View>
      </View>
      <View style={[styles.defaultTabContent, styles.defaultColumnContainer, {justifyContent: 'flex-start'}, styles.marginWidth]}>
        <ScrollView showsVerticalScrollIndicator={false} style={{minHeight: '80%', width: '100%', marginBottom: 10}} contentContainerStyle={[{alignItems: 'flex-start', width: '100%'}]}>
          {
            !release.isMultiShot() &&
            <Image
              style={styles.defaultZoomImage}
              source={{ uri: formats.cloudinarize(shots[0])}}
            />
          }
          {
            release.isMultiShot() &&
            <TouchableOpacity style={styles.defaultZoomImageContainer} onPress={() => setCurrentShot(getNextShot(currentShot))}>
              <Image
                style={styles.defaultZoomMultiImage}
                source={{ uri: formats.cloudinarize(currentShot)}}
              />
              <View style={[{position: 'absolute', left: '25%', bottom: 10, backgroundColor: stylesheet.Tertiary, width: '50%', height: 3, borderRadius: 3}]}>
                <View style={{position: 'absolute', width: `${100 / release.shots.length}%`, left: `${(100 / release.shots.length) * getShotIndex(currentShot)}%`, borderRadius: 3, backgroundColor: stylesheet.Primary, height: '100%'}}></View>
              </View>
            </TouchableOpacity>
          }
          <View style={[styles.defaultRowContainer, styles.fullWidth]}>
            <View style={[styles.fullHeight, styles.defaultColumnContainer]}>
              <View style={{flex: 0.02}}></View>
              <Text style={[styles.subHeaderText, styles.nunitoText, styles.tertiary]}>{release.getName()}</Text>
              <View style={{flex: 0.01}}></View>
              <Text style={[styles.baseText, styles.nunitoText, styles.tertiary, styles.opaque]}>{release.isReactive() ? <Text style={styles.primary}>{Localization.t('reactive')}&nbsp;</Text> : ''}{release.getType()}</Text>
            </View>
            <View style={styles.spacer}></View>
            <View style={[styles.defaultColumnContainer, styles.center]}>
              {
                !release.isLimited() &&
                <Text style={[styles.subHeaderText, styles.tertiary, styles.opaque]}>${release.getPrice()}</Text>
              }
              {
                release.isLimited() && !isSoldOut &&
                <>
                  <Text style={[styles.subHeaderText, styles.tertiary, styles.opaque]}>${release.getPrice()}</Text>
                  <Text style={[styles.baseText, styles.primary, styles.opaque]}>{Localization.t('limited')}</Text>
                </>
              }
              {
                release.isLimited() && isSoldOut &&
                <Text style={[styles.subHeaderText, styles.primary, styles.opaque]}>{Localization.t('CsoldOut')}</Text>
              }
            </View>
          </View>
          <Text style={[styles.baseText, styles.tertiary, styles.fullWidth, styles.opaque, {marginTop: 20}]}>{release.getDescription()}</Text>
          {
            !isUnavailable && actions.includes('colorable') &&
            <>
              <Text style={[styles.baseText, styles.fullWidth, styles.centerText, styles.bold, styles.tertiary, {marginTop: 30}]}>{Localization.t('chooseColor', {type: formats.getType(release.type)})}</Text>
              <View style={[styles.defaultRowContainer, styles.fullWidth, styles.spacer, {justifyContent: 'space-around', flexWrap: 'wrap', marginTop: 15}]}>
                {
                  colors.map((c, index) => {
                    return (
                      <TouchableOpacity activeOpacity={1} key={index} onPress={() => onColorPick(c.value)} disabled={color === c.value} style={[Styles.colorBox, styles.defaultColumnContainer]}>
                        <View style={[styles.spacer, {backgroundColor: c.color, borderRadius: 8}, color === c.value ? {opacity: 0.1} : {opacity: 0.95}]}></View>
                        <View style={{flex: 0.1}}></View>
                        <View style={[Styles.colorName, {backgroundColor: stylesheet.SecondaryTint}, styles.center, color === c.value ? {opacity: 0.1} : {opacity: 0.95}]}>
                          <Text style={[styles.tinyText, styles.tertiary, styles.bold]}>{c.label}</Text>
                        </View>
                      </TouchableOpacity>
                    )
                  })
                }
                {
                  showAllColors &&
                  <Ionicons onPress={() => onColorsToggle(false)} style={[styles.fullWidth, styles.centerText, styles.bold, styles.primary, {marginTop: 20, marginBottom: 20}]} name="ios-arrow-up" size={28} color={stylesheet.Primary} />
                }
                {
                  !showAllColors &&
                  <Text onPress={() => onColorsToggle(true)} style={[styles.baseText, styles.fullWidth, styles.centerText, styles.bold, styles.primary, {marginTop: 20, marginBottom: 20}]}>{Localization.t('moreColors')}</Text>
                }
              </View>
            </>
          }
          {
            !isUnavailable && actions.includes('sizeable') &&
            <>
              <Text style={[styles.baseText, styles.fullWidth, styles.centerText, styles.bold, styles.tertiary, {marginTop: 30}]}>{Localization.t('chooseSize', {type: formats.getType(release.type)})}</Text>
              <View style={[styles.defaultRowContainer, styles.fullWidth, styles.spacer, {justifyContent: 'space-around', flexWrap: 'wrap', marginTop: 15}]}>
                {
                  sizes.map((s, index) => {
                    return (
                      <TouchableOpacity activeOpacity={1} key={"SIZE_INDEX_"+index} disabled={s === size} onPress={() => onSizePick(s)} style={[styles.center, {flexBasis: '23%', marginBottom: 20, height: 70, backgroundColor: stylesheet.SecondaryTint, borderRadius: 5}, s === size ? {backgroundColor: stylesheet.Primary, opacity: 0.8} : styles.opaque]}>
                        <Text style={[styles.center, styles.baseText, styles.bold, styles.tertiary, s === size ? {color: '#FFF'} : styles.opaque]}>{s}</Text>
                      </TouchableOpacity>
                    )
                  })
                }
              </View>
            </>
          }
          {
            actions.includes('reactive') &&
            <>
              <Text style={[styles.baseText, styles.fullWidth, styles.centerText, styles.bold, styles.tertiary, {marginTop: 30}]}>{Localization.t('reactiveWarning')}</Text>
              <Text style={[styles.tinyText, styles.fullWidth, styles.centerText, styles.tertiary, {marginTop: 10}]}>{Localization.t('reactiveTap')}</Text>
              <View style={[styles.defaultRowContainer, styles.fullWidth, styles.spacer, {justifyContent: 'space-around', flexWrap: 'wrap', marginTop: 20, marginBottom: 10}]}>
                {
                  reactives.map((side, index) => {
                    if (side.reactivity === 'image') {
                      return (
                        <TouchableOpacity key={"RE"+index} onPress={() => onReactiveImageChoose(index)} style={[Styles.colorBox, styles.defaultColumnContainer, styles.center, {marginTop: 10, width: 170}]}>
                          {
                            side.set &&
                            <Image source={{uri: side.file }} style={[styles.spacer, {backgroundColor: stylesheet.Primary, opacity: 0.9, borderRadius: 8, width: 100}]}></Image>
                          }
                          {
                            !side.set &&
                            <View style={[styles.spacer, {borderColor: stylesheet.Primary, borderWidth: 2, borderStyle: 'dashed',  opacity: 0.9, borderRadius: 8, width: 100}]}></View>
                          }
                          <View style={{flex: 0.1}}></View>
                          <View style={[Styles.colorName, {backgroundColor: stylesheet.SecondaryTint, width: 170}, styles.center]}>
                            <Text style={[styles.baseText, styles.primary, styles.bold, side.set ? {opacity: 0.4} : {opacity: 1}]}>{Localization.t('reactivePicture', {side: formats.formatSide(side.side)})}</Text>
                          </View>
                        </TouchableOpacity>
                      )
                    }

                    return (
                      <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary, {marginTop: 10}]} value={side.artwork} onChangeText={value => onReactiveTextChange(value, index)} keyboardType="default" placeholder={formats.getReactivePlaceholder(side.reactivity)} placeholderTextColor="#555555" returnKeyType="return" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor={stylesheet.Primary} textContentType="none" />
                    )
                  })
                }
              </View>
              <View style={{flex: 0.02}}></View>
            </>
          }

          {
            !isUnavailable &&
            <>
              {
                !release.isLimited() &&
                <View style={[styles.defaultRowContainer, styles.center, styles.fullWidth, {marginTop: 30, marginBottom: 20}]}>
                  <TouchableOpacity
                    disabled={quantity == 1}
                    style={quantity == 1 ? styles.disabled : {opacity: 1}}
                    onPress={() => setQuantity(quantity > 1 ? quantity - 1 : 1)}
                    underlayColor='#fff'>
                    <Ionicons styles={{padding: 8}} name="md-remove-circle-outline" size={28} color={stylesheet.Primary} />
                  </TouchableOpacity>
                  <View style={{flex: 0.1}}></View>
                  <Text style={[styles.subHeaderText, styles.tertiary]}>{quantity}</Text>
                  <View style={{flex: 0.1}}></View>
                  <TouchableOpacity
                    disabled={release.getRemaining() + 1 > quantity}
                    style={release.getRemaining() + 1 > quantity ? style.disabled : {opacity: 1}}
                    onPress={() => setQuantity(quantity + 1)}
                    underlayColor='#fff'>
                    <Ionicons styles={{padding: 8}} name="md-add-circle-outline" size={28} color={stylesheet.Tertiary} />
                  </TouchableOpacity>
                </View>
              }
              <TouchableOpacity
                disabled={isLoading || !canAdd}
                style={[styles.roundedButton, styles.filled, {marginLeft: '7.5%', marginTop: 5, marginBottom: 20}, isLoading || !canAdd ? styles.disabled : {opacity: 1}]}
                onPress={() => onSelect()}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('addToCart')}</Text>
              </TouchableOpacity>
            </>
          }
        </ScrollView>
        <View style={styles.spacer}></View>
        <TouchableOpacity onPress={() => navigation.navigate('BrandProfile', {session: SESSION, designer: release.designer.getJSON()})} style={[styles.defaultRowContainer, {alignItems: 'center', marginBottom: 5}]}>
          <View style={[styles.spacer, styles.defaultColumnContainer]}>
            <Text style={[styles.baseText, styles.nunitoText, styles.tertiary]}>{Localization.t('moreBy')} <Text style={styles.primary}>{release.designer.getName()}</Text></Text>
            <View style={{flex: 0.3}}></View>
            <Text style={[styles.tinyText, styles.tertiary, styles.opaque]}>{Localization.t('brandReleasesPrompt')}</Text>
          </View>
          <View style={{flex: 0.04}}></View>
          <Ionicons name="chevron-forward" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
      </View>
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.StatusBar === 'light-content' ? stylesheet.Primary : stylesheet.Secondary}} defaultOverlayOpacity={0.85} indicatorColor={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, {marginTop: 10}]}><Ionicons name="ios-checkmark-circle-outline" size={36} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} /></Text>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 10}]}>{Localization.t('addedToBag')}</Text>
          <View style={styles.line}></View>
          <TouchableOpacity onPress={() => onReviewCart()} style={[styles.roundedButton, stylesheet.StatusBar === 'light-content' ? styles.filledTertiary : styles.filled, {marginLeft: '7.5%'}]}>
            <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.primary : styles.secondary]}>{Localization.t('reviewCart')}</Text>
          </TouchableOpacity>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  colorBox: {
    height: 120,
    width: 90,
    margin: 6
  },
  colorName: {
    height: 30,
    width: 90,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8
  },
  reactiveBox: {
    height: 50,
    width: 110,
    margin: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: stylesheet.SecondaryTint,
    borderRadius: 8
  }
});
