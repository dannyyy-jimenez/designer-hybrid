import React from 'react';
import { View, SafeAreaView, Text, Switch, Linking, Keyboard, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function Release({navigation, route}) {
  const SESSION = route.params.session;
  const design = route.params.design;
  const raw = route.params.raw;
  const designer = route.params.designer;

  const [section, setSection] = React.useState(1);
  const [limited, setLimited] = React.useState(false);
  const [limit, setLimit] = React.useState('');
  const [colorable, setColorable] = React.useState(false);
  const [royalty, setRoyalty] = React.useState('');
  const [isError, setIsError] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);

  const onRelease = () => {
    setIsLoading(true);
    setIsError(false);

    if (isNaN(parseFloat(royalty))) {
      setIsLoading(false);
      setIsError(true);
      return;
    }

    const body = {auth: SESSION.crypto, design, limited, limit, colorable, royalty };
    API.post('portfolio/release', body).then((res) => {
      if (res.isError) throw 'error';

      navigation.goBack();
    }).catch(e => {
      setIsLoading(false);
      setIsError(true);
    });
  }

  const onLinkTap = async () => {
    const link = `https://www.tailorii.app/uploader/${designer}/${raw.id}`;
    const supported = await Linking.canOpenURL(link);

    if (supported) {
      await Linking.openURL(link);
    }
  };

  React.useEffect(() => {

    console.log(route.params);

  }, [useColorScheme()])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      <KeyboardAvoidingView behavior="padding" style={styles.defaultTabContainer}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            width: '100%'
          }}>
          {
            section === 1 &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('releasePrompt')}</Text>
              <View style={{flex: 0.07}}></View>
              <Text style={[styles.centerText, styles.tertiary, {paddingLeft: 10, paddingRight: 10}, styles.baseText]}>{Localization.t('releaseMakeSure')} <Text style={styles.primary} onPress={() => navigation.navigate('Document', {name: 'guidelines'})}>{Localization.t('guidelines')}</Text>, <Text onPress={() => navigation.navigate('Document', {name: 'release-notes'})} style={styles.primary}>{Localization.t('notes')}</Text>, {Localization.t('and')} <Text onPress={() => navigation.navigate('Document', {name: 'royalties-explanation'})} style={styles.primary}>{Localization.t('explanation')}</Text> {Localization.t('befCont')}</Text>
              <View style={[styles.spacer]}></View>
              <TouchableOpacity
                style={[styles.roundedButton, styles.filled]}
                onPress={() => setSection(2)}
                underlayColor='#fff'>
                <Text style={[styles.tertiary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('continue')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            section === 2 && !limited &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('limitedPrompt')}</Text>
              <View style={styles.spacer}></View>
              <TouchableOpacity
                style={[styles.roundedButton, styles.clear]}
                onPress={() => {setSection(3);setLimit('0')}}
                underlayColor='#fff'>
                <Text style={[styles.primary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('no')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.05}}></View>
              <TouchableOpacity
                style={[styles.roundedButton, styles.filled]}
                onPress={() => setLimited(true)}
                underlayColor='#fff'>
                <Text style={[styles.tertiary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('yes')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            section === 2 && limited &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('limitPrompt')}</Text>
              <View style={styles.spacer}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={limit} onChangeText={value => setLimit(value.replace(/\D/g,''))} keyboardType="number-pad" placeholder={Localization.t('enterLimit')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} textContentType="none" />
              <View style={styles.spacer}></View>
              <TouchableOpacity
                disabled={parseInt(limit) < 5 || limit === ''}
                style={[styles.roundedButton, styles.filled, parseInt(limit) < 5 || limit === '' ? styles.disabled : null]}
                onPress={() => setSection(3)}
                underlayColor='#fff'>
                <Text style={[styles.tertiary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('next')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            section === 3 &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('colorablePrompt')}</Text>
              <View style={styles.spacer}></View>
              <TouchableOpacity
                style={[styles.roundedButton, styles.clear]}
                onPress={() => setSection(4)}
                underlayColor='#fff'>
                <Text style={[styles.primary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('no')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.05}}></View>
              <TouchableOpacity
                style={[styles.roundedButton, styles.filled]}
                onPress={() => {setColorable(true); setSection(4)}}
                underlayColor='#fff'>
                <Text style={[styles.tertiary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('yes')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            section === 4 &&
            <>
              <View style={{flex: 0.15}}></View>
              <Ionicons name="link" size={72} color={stylesheet.Primary} />
              <View style={{flex: 0.08}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('artworkfilePrompt')}</Text>
              <View style={{flex: 0.07}}></View>
              <Text style={[styles.centerText, styles.tertiary, {paddingLeft: 20, paddingRight: 20}, styles.baseText]}>{Localization.t('artworkfilePromptm')}</Text>
              <View style={{flex: 0.4}}></View>
              <Text selectable={true} onPress={() => onLinkTap()} style={[styles.centerText, styles.primary, {paddingLeft: 20, paddingRight: 20}, styles.baseText]}>https://www.tailorii.app/uploader/{designer}/{raw.id}</Text>
              <View style={styles.spacer}></View>
              <TouchableOpacity
                style={[styles.roundedButton, styles.clear]}
                onPress={() => setSection(5)}
                underlayColor='#fff'>
                <Text style={[styles.primary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('noApply')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.05}}></View>
              <TouchableOpacity
                style={[styles.roundedButton, styles.filled]}
                onPress={() => {setSection(5)}}
                underlayColor='#fff'>
                <Text style={[styles.tertiary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('yes')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            section === 5 &&
            <>
              <View style={{flex: 0.15}}></View>
              <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t('minRoyalty')}</Text>
              <View style={{flex: 0.08}}></View>
              {
                royalty !== "" &&
                <Text style={[styles.centerText, styles.paddedSides, styles.opaque, styles.tertiary, styles.baseText, {marginTop: 20}]}>{formats.getEstimatedPricing(raw.type, royalty)}</Text>
              }
              <Text style={[styles.centerText, styles.paddedSides, styles.opaque, styles.tertiary, styles.baseText]}>{Localization.t('learnMoreRoyalties')} <Text onPress={() => navigation.navigate('Document', {name: 'pricing'})} style={styles.primary}>{Localization.t('pricingAlgo')}</Text></Text>
              <View style={{flex: 0.05}}></View>
              {
                isError &&
                <Text style={[styles.centerText, styles.opaque, styles.primary, styles.baseText]}>Error releasing merch. Try again.</Text>
              }
              <View style={styles.spacer}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={royalty} onChangeText={value => setRoyalty(value)} keyboardType="decimal-pad" placeholder={Localization.t('enterMinRoy')} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} textContentType="none" />
              <View style={styles.spacer}></View>
              <Text style={[styles.centerText, styles.paddedSides, styles.opaque, styles.primary, styles.bold, styles.baseText]}>{Localization.t('seeOur')} <Text onPress={() => navigation.navigate('Document', {name: 'post-release'})} style={styles.primary}>{Localization.t('postRel')}</Text></Text>
              <View style={{flex: 0.05}}></View>
              <TouchableOpacity
                disabled={royalty === '' || isLoading}
                style={[styles.roundedButton, styles.filled, royalty === '' || isLoading ? styles.disabled : {opacity: 1}]}
                onPress={() => onRelease()}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('release')}</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
