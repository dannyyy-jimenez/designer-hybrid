import React from 'react';
import { StyleSheet, Text, View, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity, TextInput } from 'react-native';
import localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function Forgot({navigation}) {
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [canSubmit, setCanSubmit] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  React.useEffect(() => {

  }, [useColorScheme()])

  const onNumberChange = (value) => {
    setPhoneNumber(formats.formatPhoneNumber(value));
    setCanSubmit(value.length > 5);
    setError('');
  };

  const onSubmit = () => {
    setIsLoading(true);

    const body = { phoneNumber };

    API.post('forgot', body).then(res => {
      if (res.isError) {
        setIsLoading(false);
        setError(res.response);
        setCanSubmit(false);
        return;
      }

      navigation.goBack();
    }).catch(() => setIsLoading(false));
  };

  return (
    <KeyboardAvoidingView behavior="padding" style={styles.defaultTabContainer, {width: '100%', height: '100%'}}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          width: '100%'
        }}>
          <View style={{flex: 0.15}}></View>
          <Text style={[styles.centerText, styles.tertiary, styles.baseText]}>{localization.t('forgotPasswordTask')}</Text>
          <View style={{flex: 0.2}}></View>
          <TextInput style={[styles.baseInput, styles.tertiary, styles.fullWidth]} value={phoneNumber} onChangeText={value => onNumberChange(value)} keyboardType="phone-pad" placeholder={localization.t('phoneNumber')} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} autoCompleteType="tel" textContentType="telephoneNumber" />
          {
            error === "INVALID_NUM" &&
            <>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.centerText, styles.primary, styles.baseText]}>{localization.t('unrecognizedNumber')}</Text>
            </>
          }
          <View style={styles.spacer}></View>
          <TouchableOpacity
            disabled={!canSubmit || isLoading}
            style={[styles.roundedButton, styles.filled, !canSubmit || isLoading ? styles.disabled : {opacity: 1}]}
            onPress={() => onSubmit()}
            underlayColor='#fff'>
            <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{localization.t('continue')}</Text>
          </TouchableOpacity>
          <View style={{flex: 0.1}}></View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}
