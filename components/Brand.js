import React from 'react';
import { View, Dimensions, SafeAreaView, Share, Linking, Platform, ActivityIndicator, Text, Switch, StyleSheet, Image, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome5, MaterialCommunityIcons, AntDesign, Octicons } from '@expo/vector-icons';
import { useIsFocused } from '@react-navigation/native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';
import env from '../env';
import Release from '../models/Release';
import Sale from '../models/Sale';

let colorScheme = Appearance.getColorScheme();
let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

export default function Brand({navigation, route}) {
  let colorScheme = useColorScheme();
  const SESSION = route.params.session;

  const [isLoading, setIsLoading] = React.useState(true);
  const [hasBrand, setHasBrand] = React.useState(SESSION.hasBrand);
  const [needsAttention, setNeedsAttention] = React.useState(false);
  const [loginLink, setLoginLink] = React.useState('');
  const [hearts, setHearts] = React.useState(0);
  const [ideas, setIdeas] = React.useState(0);
  const [identifier, setIdentifier] = React.useState('');
  const [name, setName] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [logo, setLogo] = React.useState('');
  const [releases, setReleases] = React.useState([]);
  // Passed down to sales

  const [profit, setProfit] = React.useState(0);
  const [distribution, setDistribution] = React.useState(0);
  const [history, setHistory] = React.useState([]);

  // passed down to edit
  const [yt, setYT] = React.useState("");
  const [fb, setFB] = React.useState("");
  const [ig, setIG] = React.useState("");
  const [sc, setSC] = React.useState("");

  React.useEffect(() => {

  }, [colorScheme])

  const onRemove = (index, outcome = false) => {
    if (outcome) return;

    releases.splice(index, 1);
    setReleases(releases.map(release => new Release(release.id, release.type, release.shots, release.name, release.description, release.reactive, release.identifier, release.price, release.remaining)));
  };

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert(Localization.t('EcameraRoll'));
      }
    }

    let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!");
      return;
    }
  };

  const onPictureSelect = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [3, 3],
        quality: 1,
        base64: true
      });
      if (!result.cancelled) {

        const res = await API.post('brand/edit/logo', {auth: SESSION.crypto, logo: result.base64});
        if (res.isError) throw 'e';

        setLogo(res.data._l);
      }
    } catch (E) {
      console.log(E);
    }
  }

  React.useEffect(() => {
    if (!navigation.isFocused()) return;

    API.get('brand', {auth: SESSION.crypto}).then(res => {
      if (res.isError) throw 'error';

      setHasBrand(res.data.hasBrand);
      setNeedsAttention(res.data.needsAttention);
      setLoginLink(res.data.loginLink);
      setHearts(res.data._b.hearts);
      setIdeas(res.data._b.ideas);
      setName(res.data._b.name);
      setDescription(res.data._b.desc);
      setLogo(res.data._b.logo);

      setIdentifier(res.data._b.identifier);
      setYT(res.data._b.socials._yt);
      setFB(res.data._b.socials._fb);
      setSC(res.data._b.socials._sc);
      setIG(res.data._b.socials._ig);

      setProfit(res.data._s.profit);
      setDistribution(res.data._s.dist);
      setHistory(res.data._s.history.map((sale) => {
        return new Sale(sale.identifier, sale.royalty, sale.fulfilled, sale.date);
      }));

      setReleases(res.data._r.map((release) => {
        return new Release(release.id, release.type, release.shots, release.name, release.description, release.reactive, release.identifier, release.price, release.remaining, release.production, release.royalties);
      }));

      if (isLoading) {
        setIsLoading(false);
      }
    }).catch((e) => {
      setIsLoading(false)
    });
  }, [useIsFocused()]);

  const onLogin = () => {
    Linking.openURL(loginLink);
  }

  const onShare = () => {
    try {
      Share.share({
        url: `${env.baseUrl}/shop/brand/${identifier}`,
        title: Localization.t('shareBrandText')
      }, {tintColor: stylesheet.Primary});
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        {
          hasBrand &&
          <TouchableOpacity
            onPress={() => navigation.navigate('Sales', {crypto: SESSION.crypto, profit, distribution, history})}
            underlayColor='#fff'>
            <MaterialIcons name="account-balance" size={24} color={stylesheet.Primary} />
          </TouchableOpacity>
        }
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('brand')}</Text>
        <View style={styles.spacer}></View>
        {
          hasBrand &&
          <TouchableOpacity
            onPress={() => navigation.navigate('Portfolio', {crypto: SESSION.crypto, profit, distribution, history})}
            underlayColor='#fff'>
            <MaterialCommunityIcons name="folder-multiple-image" size={24} color={stylesheet.Primary} />
          </TouchableOpacity>
        }
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      {
        !hasBrand && !isLoading &&
        <View style={[styles.defaultTabContent, styles.marginWidth]}>
          <View style={styles.spacer}></View>
          <Ionicons name="ios-rocket" size={96} color={stylesheet.Primary} />
          <View style={{flex: 0.15}}></View>
          <Text style={[styles.headerText, styles.tertiary]}>{Localization.t('blastOffPrompt')}</Text>
          <View style={{flex: 0.2}}></View>
          <Text style={[styles.tiny, styles.opaque, styles.centerText, styles.tertiary]}>{Localization.t('brandExplainer')}</Text>
          <View style={styles.spacer}></View>
          <TouchableOpacity style={[styles.roundedButton, styles.filled]} onPress={() => navigation.navigate('BrandStarter')}>
            <Text style={[styles.baseText, colorScheme === 'light' ? styles.secondary : styles.tertiary]}>{Localization.t('continue')}</Text>
          </TouchableOpacity>
          <View style={{flex: 0.1}}></View>
        </View>
      }
      {
        hasBrand &&
        <ScrollView style={styles.defaultTabScrollContent} showsVerticalScrollIndicator={false} contentContainerStyle={[{alignItems: 'center', width: '90%', marginLeft: '5%', paddingTop: 10, justifyContent: 'center'}]}>
          <View style={[styles.defaultRowContainer, styles.fullWidth, {marginTop: 20}]}>
            <TouchableOpacity onPress={() => onPictureSelect()}>
              {
                logo !== '' &&
                <Image style={styles.profileBrandLogo} source={{ uri: formats.cloudinarize(logo, 'c_scale,w_300/'), cache: 'force-cache'}} />
              }
            </TouchableOpacity>
            <View style={styles.spacer}></View>
            <View style={[styles.defaultColumnContainer, styles.center]}>
              <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.abbreviate(ideas)}</Text>
              <View style={{flex: 0.05}}></View>
              <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('releases')}</Text>
            </View>
            <View style={{flex: 0.5}}></View>
            <View style={[styles.defaultColumnContainer, styles.center]}>
              <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.abbreviate(hearts)}</Text>
              <View style={{flex: 0.05}}></View>
              <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('hearts')}</Text>
            </View>
            <View style={{flex: 0.5}}></View>
            <View style={[styles.defaultColumnContainer, styles.center]}>
              <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.abbreviate(distribution)}</Text>
              <View style={{flex: 0.05}}></View>
              <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('sales')}</Text>
            </View>
            <View style={styles.spacer}></View>
          </View>
          <Text style={[styles.tinyText, styles.fullWidth, styles.tertiary, styles.opaque, {marginTop: 20}]}>@{identifier}</Text>
          <Text style={[styles.baseText, styles.fullWidth, styles.tertiary, styles.bold, {marginTop: 5}]}>{name}</Text>
          <Text style={[styles.baseText, styles.tertiary, styles.fullWidth, {marginTop: 5, marginBottom: 15}]}>{description}</Text>
          <View style={[styles.defaultRowContainer, styles.fullWidth, styles.center]}>
            <TouchableOpacity
              style={[styles.roundedButton, styles.clear, {padding: 10, width: '44%', marginLeft: 5, marginRight: 5}]}
              onPress={() => navigation.navigate('EditBrand', {name, description, sc, fb, yt, ig})}
              underlayColor='#fff'>
              <Text style={[styles.primary, styles.baseText, styles.centerText]}>{Localization.t('editBrand')}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.roundedButton, styles.clear, {padding: 10, width: '44%', marginLeft: 5, marginRight: 5}]}
              onPress={() => onShare()}
              underlayColor='#fff'>
              <Text style={[styles.primary, styles.baseText, styles.centerText]}>{Localization.t('shareBrand')}</Text>
            </TouchableOpacity>
          </View>
          {
            needsAttention &&
            <View style={[styles.spacer, styles.defaultColumnContainer, styles.marginWidth, styles.center]}>
              <Text style={[styles.subHeaderText, styles.primary, styles.centerText, styles.bold, {marginTop: 30}]}>{Localization.t('attention')}</Text>
              <Text style={[styles.baseText, styles.primary, styles.centerText, {marginTop: 30}]}>{Localization.t('accountNeedsAttention')}</Text>
              <TouchableOpacity
                style={[styles.roundedButton, styles.filled, {marginTop: 30, marginBottom: 30}]}
                onPress={() => onLogin()}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('takeMe')}</Text>
              </TouchableOpacity>
            </View>
          }
          {
            !isLoading && releases.length === 0 &&
            <View style={[styles.spacer, styles.defaultColumnContainer, styles.marginWidth, styles.center]}>
              <Text style={[styles.subHeaderText, styles.tertiary, styles.centerText, styles.bold, {marginTop: 60}]}>{Localization.t('releasesAppearHere')}</Text>
              <Text style={[styles.baseText, styles.tertiary, styles.centerText, {marginTop: 30}]}>{Localization.t('viewSales')} <MaterialIcons name="account-balance" size={18} color={stylesheet.Tertiary}/></Text>
              <Text style={[styles.baseText, styles.tertiary, styles.centerText, {marginTop: 30}]}>{Localization.t('addDesignPortPrompt')} <MaterialCommunityIcons name="folder-multiple-image" size={18} color={stylesheet.Tertiary}/></Text>
              <Text style={[styles.baseText, styles.tertiary, styles.centerText, {marginTop: 30, marginBottom: 60}]}>{Localization.t('releaseDesignPrompt')} <Text style={styles.primary}>{Localization.t('addToBrand')}</Text> {Localization.t('inPortfolio')}</Text>
            </View>
          }
          <View style={[styles.defaultRowContainer, {justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap'}]}>
            {
              releases.map((release, index) => {
                return (
                  <TouchableOpacity activeOpacity={1} key={release.getID()} onPress={() => navigation.navigate('PortfolioZoom', {session: SESSION, index, design: release.getRawParent()})} style={styles.defaultStoreCardMini}>
                    <Image
                      style={styles.defaultSCImage}
                      source={{ uri: formats.cloudinarize(release.getCover(), '', true)}}
                    />
                    <View style={styles.defaultColumnContainer, styles.fullWidth, styles.featuredSCContent}>
                      <View style={{position: 'absolute', left: 0, bottom: 0, padding: 5, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 5, borderBottomLeftRadius: 5, backgroundColor: stylesheet.SecondaryTint}}>
                        <Text style={[styles.baseText, styles.tertiary, styles.opaque, {marginTop: 2}]}>${release.getPrice()}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              })
            }
            <View style={[styles.defaultRowContainer, styles.fullWidth, styles.center, {marginTop: 30, marginBottom: 15}]}>
              <FontAwesome5 name="youtube" color={stylesheet.PrimaryOpaque} size={32} />
              <View style={{flex: 0.15}}></View>
              <FontAwesome5 name="instagram" color={stylesheet.PrimaryOpaque} size={32} />
              <View style={{flex: 0.15}}></View>
              <FontAwesome5 name="facebook-f" color={stylesheet.PrimaryOpaque} size={32} />
              <View style={{flex: 0.15}}></View>
              <FontAwesome5 name="snapchat-ghost" color={stylesheet.PrimaryOpaque} size={32} />
            </View>
          </View>
        </ScrollView>
      }
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: Dimensions.get('window').width * 0.7,
    marginLeft: Dimensions.get('window').width * 0.025,
    marginRight: Dimensions.get('window').width * 0.025,
    height: 350,
    borderRadius: 30,
    flexDirection: 'column',
    backgroundColor: stylesheet.SecondaryTint,
    ...Platform.select({
      android: {
        height: 300
      }
    }),
  },
  itemImage: {
    height: '85%',
    width: 'auto',
    resizeMode: 'contain'
  }
});
