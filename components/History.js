import React from 'react';
import { View, SafeAreaView, Text, ScrollView, StyleSheet, ActivityIndicator, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';
import Order from '../models/Order';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

export default function History({navigation, route}) {
  const SESSION = route.params.session;

  const [orderNumber, setOrderNumber] = React.useState('');
  const [baseHistory, setBaseHistory] = React.useState([]);
  const [history, setHistory] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    if (orderNumber.replace(/ /g, '') === '') {
      return;
      setHistory(baseHistory.slice());
    }
    setHistory(baseHistory.filter((order) => {
      let likeRegex = new RegExp(orderNumber, 'gi');
      return likeRegex.test(order.getIdentifier());
    }));
  }, [orderNumber]);

  React.useEffect(() => {

  }, [useColorScheme()])

  React.useEffect(() => {
    setIsLoading(true);
    API.get('history', {auth: SESSION.crypto}).then((res) => {
      if (res.isError) return;

      setBaseHistory(res.data._h.map((order) => new Order(order.identifier, order.total, order.date, order.fulfilled)));
      setHistory(res.data._h.map((order) => new Order(order.identifier, order.total, order.date, order.fulfilled)));
      setIsLoading(false);
    }).catch(e => {
      setIsLoading(false);
      console.log(e);
    });
  }, []);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Payment')}
          underlayColor='#fff'>
          <Ionicons name="md-card" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('orderHistory')}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          onPress={() => navigation.navigate('Settings')}
          underlayColor='#fff'>
          <Ionicons name="ios-settings" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={[{alignItems: 'center', width: '98%', marginLeft: '1%', paddingTop: 10, justifyContent: 'center'}]}>
        <Text style={[styles.subHeaderText, styles.centerText, styles.tertiary, {marginTop: 5}]}>{Localization.t('viewOrders')}</Text>
        <Text style={[styles.baseText, styles.centerText, styles.tertiary, {marginTop: 5}]}>{Localization.t('orderNumberPrompt')}</Text>
        <TextInput keyboardAppearance={colorScheme === "light" ? "light" : "dark"} style={[styles.baseInput, styles.filledInput, styles.tertiary, {marginTop:25}]} value={orderNumber} onChangeText={(value) => setOrderNumber(value)} keyboardType="number-pad" placeholder={Localization.t('enterOrderNumber')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={[styles.spacer, {width: '85%', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', marginTop: 25}]}>
          {
            !isLoading && history.length === 0 &&
            <Text style={[styles.baseText, styles.centerText, styles.tertiary, styles.fullWidth, styles.opaque]}>{Localization.t('inexOrders')}</Text>
          }
          {
            isLoading &&
            <View style={[styles.defaultLoader, styles.fullWidth, styles.center, styles.defaultColumnContainer]}>
              <View style={{flex: 0.1}}></View>
              <ActivityIndicator size="small" color={stylesheet.Primary} />
            </View>
          }
          {
            !isLoading && history.map((order, index) => {
              return (
                <TouchableOpacity key={order.getIdentifier() + '_' + index} style={[Styles.orderContainer, {backgroundColor: stylesheet.SecondaryTint}, order.wasFulfilled() ? {opacity: 0.4} : {opacity: 1}]} onPress={() => navigation.navigate('Tracking', {session: SESSION, goBack: true, orderNumber: order.getIdentifier()})}>
                  <View style={{flexDirection: 'row', flexGrow: 1, alignItems: 'center'}}>
                    <Text style={styles.tertiary}>${order.getTotal()}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={styles.tertiary}>{order.getDate()}</Text>
                  </View>
                  <View style={styles.spacer}></View>
                  <Text style={[styles.tinyText, styles.tertiary, styles.opaque]}>#{order.getIdentifier()}</Text>
                </TouchableOpacity>
              )
            })
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  orderContainer: {
    height: 60,
    minWidth: 150,
    borderRadius: 8,
    margin: 5,
    padding: 10,
    flexDirection: 'column'
  }
});
