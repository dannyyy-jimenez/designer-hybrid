import React from 'react';
import { View, SafeAreaView, Dimensions, RefreshControl, ActivityIndicator, ScrollView, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useIsFocused } from '@react-navigation/native';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';
import Design from '../models/Design';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function Portfolio({navigation, route}) {
  const SESSION = route.params.session;

  const [isLoading, setIsLoading] = React.useState(true);
  const [refreshing, setRefreshing] = React.useState(false);
  const [designs, setDesigns] = React.useState([]);
  const load = async () => {
    setRefreshing(true);
    try {
      let res = await API.get('portfolio', {auth: SESSION.crypto});
      setDesigns(res.data._d.map((design) => new Design(design.id, design.t, design.s, design.n, design.d, design.hD, design.r)));
      setRefreshing(false);
      setIsLoading(false);

      if (route.params.success) {
        let newly = res.data._d[0];
        let newlyDesign = new Design(newly.id, newly.t, newly.s, newly.n, newly.d, newly.hD, newly.r);
        route.params.success = false;
        navigation.navigate('PortfolioZoom', {session: SESSION, design: newlyDesign});
      }
    } catch(e) {
      console.log(e);
      setRefreshing(false);
      setIsLoading(false);
      return;
    }
  };

  React.useEffect(() => {

  }, [useColorScheme()])

  React.useEffect(() => {
    if (!navigation.isFocused()) {
      return;
    };

    load();
  }, [useIsFocused()]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('portfolio')}</Text>
        <View style={styles.spacer}></View>
        <Ionicons name="chevron-back" size={24} color={stylesheet.Secondary} />
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      {
        designs.length === 0 &&
        <View style={styles.defaultTabContent}>
          <View style={{flex: 0.15}}></View>
          <Text style={[styles.centerText, styles.tertiary, styles.opaque, styles.baseText]}>{Localization.t('emptyPortfolio')}</Text>
          <View style={{flex: 0.15}}></View>
          <TouchableOpacity
            onPress={() => navigation.navigate('WorkshopStarter', {SESSION})}
            underlayColor='#fff'>
            <Ionicons name="md-construct" size={96} color={stylesheet.Primary} />
          </TouchableOpacity>
        </View>
      }
      {
        designs.length !== 0 &&
        <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'center', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={load} />}>
          {
            designs.map((design, index) => {
              return (
                <TouchableOpacity key={design.getID()} onPress={() => navigation.navigate('PortfolioZoom', {session: SESSION, index, newly: true, design})} style={[Styles.itemContainer, {backgroundColor: stylesheet.SecondaryTint}]}>
                  <Image
                    style={Styles.itemImage}
                    source={{ uri: formats.cloudinarize(design.getCover(), '', true)}}
                  />
                  <View style={[styles.defaultRowContainer, styles.fullWidth, {alignItems: 'center'}]}>
                    {
                      !design.hasDropped &&
                      <>
                        <View style={styles.spacer}></View>
                        <Text style={[styles.baseText, styles.primary]}>{design.getName()}</Text>
                        <View style={styles.spacer}></View>
                      </>
                    }
                    {
                      design.hasDropped &&
                      <>
                        <Text style={[styles.baseText, styles.primary]}>{design.getName()}</Text>
                        <View style={styles.spacer}></View>
                        <Text style={[styles.baseText, styles.primary, styles.opaque, styles.miniText]}>{Localization.t('inMarket')}</Text>
                      </>
                    }
                  </View>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      }
      <View style={[styles.fullWidth, styles.defaultCom]}>
        <TouchableOpacity
          style={[styles.roundedButton, {marginLeft: '7.5%'}, styles.clear]}
          onPress={() => navigation.navigate('WorkshopStarter')}
          underlayColor='#fff'>
          <Text style={[styles.primary, styles.baseText]}><Ionicons name="md-construct" size={20} color={stylesheet.Primary} /> {Localization.t('addToPortfolio')} <Ionicons name="md-construct" size={20} color={stylesheet.Primary} /></Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: '90%',
    height: 250,
    borderRadius: 5,
    flexDirection: 'column',
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 10,
    marginBottom: 10
  },
  itemImage: {
    height: '92%',
    width: 'auto',
    resizeMode: 'contain'
  }
});
