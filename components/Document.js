import React from 'react';
import { View, SafeAreaView, Text, StyleSheet, ActivityIndicator, ScrollView, Linking, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

export default function Tracking({navigation, route}) {
  const {name} = route.params;
  const [documentData, setDocumentData] = React.useState({});
  const [isValidDocument, setIsValidDocument] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const validDocuments = ['pricing', 'guidelines', 'release-notes', 'royalties-explanation', 'delivery', 'privacy', 'terms', 'attributions', 'dmca', 'post-release', 'merchandise', 'printing', 'styles'];

  React.useEffect(() => {
    // get document depending on name
    if (!name || name === '' || !validDocuments.includes(name) || name.includes('update')) {
      setIsValidDocument(false);
      setIsLoading(false);
      return;
    }

    fetch(`https://res.cloudinary.com/lgxy/raw/upload/tailori/documents/${name}.min.json`, {headers: {'Cache-Control': 'no-cache'}}).then(response => response.json()).then(data => {
      setDocumentData(data);
      setIsValidDocument(true);
      setIsLoading(false);
    }).catch((e) => {
      setIsValidDocument(false);
      setIsLoading(false);
    });
  }, [name]);

  React.useEffect(() => {

  }, [useColorScheme()])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="md-close" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
        {
          !isValidDocument &&
          <Text style={[styles.centerText, styles.fullWidth, styles.tertiary, styles.opaque, styles.headerText]}>Document not found!</Text>
        }
        {
          isValidDocument &&
          <>
            <View style={styles.spacer}></View>
            <Text style={[styles.centerText, styles.fullWidth, styles.primary, styles.bold, styles.subHeaderText]}>{documentData.title}</Text>
            <Text>{"\n\n\n"}</Text>
            {
              documentData.content.map((text, index) => {
                if (text.type === 'uri') {
                  return (
                    <>
                      <Text onPress={ ()=>{ Linking.openURL(text.external)}} style={[styles.fullWidth, styles.primary, styles.baseText]}>{text.content}</Text>
                      <Text>{"\n\n"}</Text>
                    </>
                  )
                }

                if (text.type === 'subheader') {
                  return (
                    <>
                      <Text style={[styles.fullWidth, styles.primary, styles.subHeaderText]}>{text.content}</Text>
                      <Text>{"\n"}</Text>
                    </>
                  )
                }

                if (text.type === 'base-inset') {
                  return (
                    <>
                      <Text style={[styles.insetWidth, styles.tertiary, styles.baseText]}>{text.content}</Text>
                      <Text>{"\n"}</Text>
                      {
                        documentData.content[index + 1] && documentData.content[index + 1].type !== 'base-inset' &&
                        <Text>{"\n"}</Text>
                      }
                    </>
                  )
                }

                return (
                  <>
                    <Text style={[styles.fullWidth, styles.tertiary, styles.baseText]}>{text.content}</Text>
                    {
                      documentData.content[index + 1] && documentData.content[index + 1].type === 'base-inset' &&
                      <Text>{"\n"}</Text>
                    }
                    {
                      documentData.content[index + 1] && documentData.content[index + 1].type !== 'base-inset' &&
                      <Text>{"\n\n"}</Text>
                    }
                  </>
                )
              })
            }
            <Text>{"\n"}</Text>
            <Text style={[styles.tertiary, styles.centerText, styles.fullWidth, styles.baseText]}>{documentData.footer[0]} <Text onPress={ ()=>{ Linking.openURL(`mailto:${documentData.footer[1]}`)}} style={styles.primary}>{documentData.footer[1]}</Text></Text>
            <Text>{"\n\n"}</Text>
            <View style={styles.spacer}></View>
          </>
        }
      </ScrollView>
    </SafeAreaView>
  );
}
