import React from 'react';
import { View, SafeAreaView, Text, Switch, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function EditBrand({navigation, route}) {
  const SESSION = route.params.session;
  const name = route.params.name;
  const [description, setDescription] = React.useState(route.params.description);
  const [yt, setYT] = React.useState(route.params.yt);
  const [fb, setFB] = React.useState(route.params.fb);
  const [ig, setIG] = React.useState(route.params.ig);
  const [sc, setSC] = React.useState(route.params.sc);
  const [isLoading, setIsLoading] = React.useState(false);

  React.useEffect(() => {

  }, [useColorScheme()])

  const onUpdate = async () => {
    setIsLoading(true);
    try {
      const res = await API.post('brand/edit', {auth: SESSION.crypto, description, yt, fb, ig, sc});
      if (res.isError) throw 'e';

      setIsLoading(false);
      navigation.goBack();
    } catch (e) {
      console.log(e);
      setIsLoading(false);
    }
  }

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
      </View>
      <View style={styles.defaultTabContent}>
        <View style={{flex: 0.05}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>{Localization.t('brandName')}</Text>
        <View style={{flex: 0.01}}></View>
        <TextInput disabled={true} editable={false} style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.disabled, styles.tertiary]} value={name} readOnly={true} placeholder={Localization.t('brandName')} placeholderTextColor="#555555" selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>{Localization.t('description')}</Text>
        <View style={{flex: 0.01}}></View>
        <TextInput style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.tertiary]} value={description} onChangeText={value => setDescription(value)} placeholder={Localization.t('description')} placeholderTextColor="#555555" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>
          <FontAwesome5 name="youtube" color={stylesheet.Primary} size={20} />
        </Text>
        <View style={{flex: 0.01}}></View>
        <TextInput style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.tertiary]} value={yt} onChangeText={value => setYT(value)} placeholder="@tailoriapp" placeholderTextColor="#555555" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>
          <FontAwesome5 name="instagram" color={stylesheet.Primary} size={20} />
        </Text>
        <View style={{flex: 0.01}}></View>
        <TextInput style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.tertiary]} value={ig} onChangeText={value => setIG(value)} placeholder="@tailoriapp" placeholderTextColor="#555555" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>
          <FontAwesome5 name="facebook-f" color={stylesheet.Primary} size={20} />
        </Text>
        <View style={{flex: 0.01}}></View>
        <TextInput style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.tertiary]} value={fb} onChangeText={value => setFB(value)} placeholder="@tailoriapp" placeholderTextColor="#555555" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary, styles.baseInputHeader, styles.opaque]}>
          <FontAwesome5 name="snapchat-ghost" color={stylesheet.Primary} size={20} />
        </Text>
        <View style={{flex: 0.01}}></View>
        <TextInput style={[styles.baseInput, styles.alignLeft, styles.filledInput, styles.inputHasHeader, styles.tertiary]} value={sc} onChangeText={value => setSC(value)} placeholder="@tailoriapp" placeholderTextColor="#555555" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor={stylesheet.Primary} textContentType="none" />
        <View style={{flex: 0.15}}></View>
        <TouchableOpacity
          onPress={() => onUpdate()}
          disabled={isLoading || description.length === 0}
          style={[styles.baseInputHeader, isLoading || description.length === 0 ? styles.disabled : {}]}
          underlayColor='#fff'>
          <Text style={[styles.baseText, styles.primary, styles.bold]}>{Localization.t('update')}</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}
