import React from 'react';
import { View, SafeAreaView, Platform, Text, StyleSheet, Picker, TextInput, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';
import ActionSheet from "react-native-actions-sheet";

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;
const DesignTypes = require('../models/Types').DesignTypes;
const DesignMetrics = require('../models/Metrics').Metrics;

const actionSheetRef = React.createRef();

export default function Workshop({navigation, route}) {
  const SESSION = route.params.session;
  const type = route.params.type;
  const mode = route.params.mode;
  const DesignType = DesignTypes.find(itemType => itemType.identifier === type);
  const [canSelectImage, setCanSelectImage] = React.useState(true);
  const [baseColor, setBaseColor] = React.useState('');
  const [activeSide, setActiveSide] = React.useState(DesignType.sides[0]);
  const [activeSideFit, setActiveSideFit] = React.useState(DesignType.sidesThumbnail[0]);
  const [metrics, setMetrics] = React.useState([]);

  const onColorPick = (color) => {
    setBaseColor(color);
    actionSheetRef.current?.setModalVisible(true);
  };

  const onSideChange = () => {
    const availableSides = DesignType.sides;
    const availableSidesFit = DesignType.sidesThumbnail;

    if (availableSides.indexOf(activeSide) === availableSides.length - 1) {
      setActiveSide(availableSides[0]);
      setActiveSideFit(availableSidesFit[0])
    } else {
      setActiveSideFit(availableSidesFit[availableSides.indexOf(activeSide) + 1])
      setActiveSide(availableSides[availableSides.indexOf(activeSide) + 1]);
    }
  }

  const pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: Platform.OS === 'android',
        quality: 1,
        exif: false,
        base64: true
      });
      if (!result.cancelled) {

        let metric = {
          file: result.uri,
          uri: result.base64,
          side: activeSide
        };

        let copy = metrics.slice();
        copy.push(metric);
        setMetrics(copy);
      }
    } catch (E) {
      console.log(E);
    }
  };

  const removeImage = () => {
    const index = metrics.findIndex((metric) => metric.side === activeSide);

    let copy = metrics.slice();
    copy.splice(index, 1);
    setMetrics(copy);
  };

  React.useEffect(() => {
     (async () => {
       if (Platform.OS !== 'web') {
         const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
         if (status !== 'granted') {
           alert('Sorry, we need camera roll permissions to make this work!');
         }
       }
     })();
   }, []);

  React.useEffect(() => {

  }, [useColorScheme()])

  const onContinue = async () => {
    navigation.navigate('WorkshopFinalizer', {type, mode, metrics, base: baseColor, session: SESSION});
  };

  const GetFitSample = () => {
    return (metrics.find(metric => metric.side === activeSide) || {file: ''}).file;
  }

  React.useEffect(() => {
    let metricExists = metrics.find((metric) => metric.side === activeSide) != undefined;

    setCanSelectImage(!metricExists);
  }, [activeSide, metrics])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        {
          baseColor === '' &&
          <TouchableOpacity
            style={styles.backArrow}
            onPress={() => navigation.navigate('Portfolio')}
            underlayColor='#fff'>
            <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
        {
          baseColor !== '' &&
          <TouchableOpacity
            style={styles.backArrow}
            onPress={() => navigation.navigate('Portfolio')}
            underlayColor='#fff'>
            <Ionicons name="md-close" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
        <View style={styles.spacer}></View>
        {
          baseColor !== '' &&
          <TouchableOpacity
            disabled={DesignType.sides.length === 1}
            style={styles.backArrow}
            onPress={() => onSideChange()}
            underlayColor='#fff'>
            <Text style={[styles.baseText, styles.bold, styles.tertiary, DesignType.sides.length === 1 ? styles.disabled : {}]}>{formats.formatSide(activeSide)} <Text style={styles.tinyText}>{Localization.t('tapToTurn')}</Text> <Ionicons name="chevron-forward" size={16} color={stylesheet.Tertiary} /></Text>
          </TouchableOpacity>
        }
        <View style={styles.spacer}></View>
        {
          baseColor !== '' &&
          <TouchableOpacity
            onPress={() => setBaseColor('')}
            underlayColor='#fff'>
            <Ionicons name="md-color-fill" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
      </View>
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={[{alignItems: 'center', justifyContent: 'flex-start', width: '94%', minHeight: '90%', marginLeft: '3%'}]}>
        {
          baseColor == '' &&
          <>
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.centerText, styles.tertiary, styles.fullWidth, styles.bold, styles.subHeaderText, {marginBottom: 20}]}>{Localization.t('pickColor', {type: formats.getType(type)})}</Text>
            <View style={[styles.defaultRowContainer, styles.fullWidth, styles.spacer, {justifyContent: 'space-around', flexWrap: 'wrap'}]}>
              {
                formats.getAvailableColors(type).map((color, index) => {
                  return (
                    <TouchableOpacity key={index} onPress={() => onColorPick(color.value)} style={[Styles.colorBox, styles.defaultColumnContainer]}>
                      <View style={[styles.spacer, {backgroundColor: color.color, opacity: 0.9, borderRadius: 8}]}></View>
                      <View style={{flex: 0.1}}></View>
                      <View style={[Styles.colorName, styles.center]}>
                        <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{color.label}</Text>
                      </View>
                    </TouchableOpacity>
                  )
                })
              }
            </View>
          </>
        }
        {
          baseColor !== '' &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.baseText, styles.centerText, styles.tertiary, styles.fullWidth]}>{Localization.t('samplingDemons')} <Text onPress={() => navigation.navigate('Document', {name: 'printing'})} style={styles.primary}>{Localization.t('conventions')}</Text> {Localization.t('and')} <Text onPress={() => navigation.navigate('Document', {name: 'styles'})} style={styles.primary}>{Localization.t('styles')}</Text></Text>
            <View style={{flex: 0.3}}></View>
            <View>
              <Image style={Styles.sideSample} source={{uri: formats.cloudinarize(activeSideFit, `w_500,e_replace_color:${formats.getColor(type, baseColor, true)}:10000/`), cache: 'force-cache'}}></Image>
              {
                !canSelectImage && GetFitSample() != "" &&
                <Image style={[Styles.fitSample, DesignMetrics[type][activeSide]]} source={{uri: GetFitSample()}} />
              }
            </View>
            <View style={{flex: 0.1}}></View>
            {
              !canSelectImage &&
              <TouchableOpacity
                style={[styles.roundedButton, styles.clear]}
                onPress={() => removeImage()}
                underlayColor='#fff'>
                <Text style={[styles.primary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('remove')}</Text>
              </TouchableOpacity>
            }
            {
              canSelectImage &&
              <TouchableOpacity
                disabled={!canSelectImage}
                style={[styles.roundedButton, styles.filled, !canSelectImage ? styles.disabled : {opacity: 1}]}
                onPress={() => pickImage()}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('chooseImage')}</Text>
              </TouchableOpacity>
            }
            <View style={styles.spacer}></View>
            <TouchableOpacity
              disabled={metrics.length === 0}
              style={[styles.roundedButton, styles.clear, metrics.length === 0 ? styles.disabled : {opacity: 1}]}
              onPress={() => onContinue()}
              underlayColor='#fff'>
              <Text style={[styles.primary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('continue')}</Text>
            </TouchableOpacity>
          </>
        }
      </ScrollView>
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.StatusBar === 'light-content' ? stylesheet.Primary : stylesheet.Secondary}} indicatorColor={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, {marginTop: 10}]}><MaterialCommunityIcons name="rotate-orbit" size={28} color={stylesheet.StatusBar === 'light-content' ? stylesheet.Tertiary : stylesheet.Primary} /></Text>
          <View style={styles.line}></View>

          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.primary, {marginTop: 10}]}>{Localization.t('tapToTurnInfo', {type: formats.getType(type)})}</Text>

          <TouchableOpacity onPress={() => actionSheetRef.current?.setModalVisible(false)} style={[styles.roundedButton, stylesheet.StatusBar === 'light-content' ? styles.filledTertiary : styles.filled, {marginLeft: '7.5%', marginTop: 20, marginBottom: 10}]}>
            <Text style={[styles.baseText, stylesheet.StatusBar === 'light-content' ? styles.primary : styles.secondary]}>{Localization.t('gotIt')}</Text>
          </TouchableOpacity>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  colorBox: {
    height: 148,
    width: 130,
    margin: 10
  },
  colorName: {
    height: 30,
    minWidth: 120,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: stylesheet.SecondaryTint,
    borderRadius: 8
  },
  sideSample: {
    width: 400,
    height: 400
  },
  fitSample: {
    resizeMode: 'cover', // cover when testing
    position: 'absolute',
    opacity: 0.9
  }
});
