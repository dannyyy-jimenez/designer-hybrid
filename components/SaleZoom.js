import React from 'react';
import { View, SafeAreaView, Text, Linking, StyleSheet, Image, ActivityIndicator, ScrollView, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function SaleZoom({navigation, route}) {
  const crypto = route.params.crypto;

  const {sale} = route.params;
  const [isLoading, setIsLoading] = React.useState(true);
  const [saleValid, setSaleValid] = React.useState(false);
  const [details, setDetails] = React.useState([]);

  React.useEffect(() => {
    setIsLoading(true);
    API.get('sales/zoom', {auth: crypto, sale}).then((res) => {
      if (res.isError) throw 'invalid';

      setDetails(res.data._d);
      setSaleValid(true);
      setIsLoading(false);
    }).catch((e) => {
      console.log(e);
      setSaleValid(false);
      setIsLoading(false);
    });
  }, [sale]);

  React.useEffect(() => {

  }, [useColorScheme()])

  const onOpenReactive = async (side) => {
    if (side.artwork.match(/tailori.*\/portfolios\/.+\/.+/g)) {
      const reactivePath = formats.cloudinarize(side.artwork);
      const supported = await Linking.canOpenURL(reactivePath);

      if (supported) {
        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
        // by some browser in the mobile
        await Linking.openURL(reactivePath);
      }
    }
  }

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>#{sale}</Text>
        <View style={styles.spacer}></View>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      {
        !isLoading && !saleValid &&
        <View style={[styles.fullWidth, styles.defaultTabContainer]}>
          <View style={{flex: 0.1}}></View>
          <Text style={[styles.baseText, styles.centerText, styles.tertiary, styles.fullWidth, styles.opaque]}>{Localization.t('EinvalidSale')}</Text>
        </View>
      }
      {
        !isLoading && saleValid &&
        <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'center', minHeight: '90%', justifyContent: 'flex-start', width: '96%'}}>
          <View style={{flex: 0.05}}></View>
          {
            details.map((detail, index) => {
              return (
                <View key={"D"+index} style={[Styles.itemContainer]}>
                  <Text key={"DQ"+index} style={[styles.primary, styles.bold, styles.baseText, {position: 'absolute', right: 0, top: 0}]}>x {detail.quantity}</Text>
                  <Image key={"DCO"+index} style={Styles.itemImage} source={{uri: formats.cloudinarize(detail.design.s[0])}}></Image>
                  <View key={"cardContainer"+index} style={[Styles.itemDC, styles.defaultColumnContainer]}>
                    <Text key={"DCS"+index} style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.formatSize(detail.design.s)} - {formats.getColor(detail.design.t, detail.color, false, true)}</Text>
                    <Text key={"DNM"+index} style={[styles.tinyText, styles.tertiary, styles.opaque, {marginTop: 5}]}>{detail.design.n}</Text>
                    <View style={[styles.paddedSides, styles.defaultColumnContainer, {marginTop: 10}]}>
                      {
                        detail.reactives.map((side) => {
                          return (
                            <TouchableOpacity onPress={() => onOpenReactive(side)} style={{marginTop: 10}}>
                              <Text style={[styles.baseText, styles.tertiary, styles.bold]}>{formats.formatSide(side.side)} - <Text style={styles.primary}>{side.artwork}</Text></Text>
                            </TouchableOpacity>
                          )
                        })
                      }
                    </View>
                  </View>
                </View>
              )
            })
          }
        </ScrollView>
      }
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: 280,
    margin: 5,
    padding: 10,
    flexDirection: 'column'
  },
  itemImage: {
    height: 150,
    width: 280,
    resizeMode: 'contain'
  },
  itemDC: {
    minHeight: 80,
    width: 280,
    padding: 10,
    borderRadius: 8,
    backgroundColor: stylesheet.SecondaryTint
  }
});
