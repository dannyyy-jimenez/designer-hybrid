import React from 'react';
import { View, SafeAreaView, Text, StyleSheet, ScrollView, Linking, TouchableOpacity } from 'react-native';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function Sales({navigation, route}) {
  const crypto = route.params.crypto;
  const [activeStat, setActiveStat] = React.useState('profit');
  const distribution = route.params.distribution;
  const profit = route.params.profit;
  const history = route.params.history;
  const [generatingLink, setGeneratingLink] = React.useState(false);
  const [stripeLink, setStripeLink] = React.useState('');

  const GenerateStripeLogin = async () => {
    setGeneratingLink(true);

    try {
      const res = await API.post('brand/dashboard', {auth: crypto});
      if (res.isError) throw 'error';

      setStripeLink(res.data._l);
    } catch(e) {
      setGeneratingLink(false);
    }
  }

  React.useEffect(() => {

  }, [useColorScheme()])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('sales')}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          disabled={generatingLink}
          style={generatingLink ? styles.disabled : {opacity: 1}}
          onPress={() => GenerateStripeLogin()}
          underlayColor='#fff'>
          <FontAwesome5 name="stripe" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
        <View style={{flex: 0.2}}></View>
        <View style={[styles.defaultRowContainer, {justifyContent: 'space-evenly', marginBottom: 40}, styles.fullWidth]}>
          <View style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
            <Text style={[styles.subHeaderText, styles.centerText, styles.nunitoText, styles.fullWidth, styles.tertiary]}>${formats.abbreviate(profit)}</Text>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.baseText, styles.primary, styles.centerText]}>{Localization.t('profit')}</Text>
          </View>
          <View style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
            <Text style={[styles.subHeaderText, styles.centerText, styles.nunitoText, styles.fullWidth, styles.tertiary]}>{formats.abbreviate(distribution)}</Text>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.baseText, styles.primary, styles.centerText]}>{Localization.t('unitsSold')}</Text>
          </View>
        </View>
        {
          stripeLink !== '' &&
          <>
            <Text onPress={() => { Linking.openURL(stripeLink)}} selectable={true} style={[styles.baseText, {marginBottom: 20}, styles.primary]}>{stripeLink}</Text>
          </>
        }
        <Text style={[styles.subHeaderText, {marginBottom: 20}, styles.tertiary]}>{Localization.t('history')}</Text>
        <View style={{flex: 0.1}}></View>
        {
          history.length === 0 &&
          <Text style={[styles.centerText, styles.fullWidth, styles.tertiary, styles.opaque, styles.baseText]}>{Localization.t('salesAppear')}</Text>
        }
        <View style={[styles.spacer, {width: '96%', marginLeft: '2%', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between'}]}>
          {
            history.map((sale) => {
              return (
                <TouchableOpacity key={sale.getRef()} onPress={() => navigation.navigate('SaleZoom', {crypto, sale: sale.getRef()})} style={[Styles.orderContainer, {backgroundColor: stylesheet.SecondaryTint}]}>
                  <View style={{flexDirection: 'row', flexGrow: 1, alignItems: 'center'}}>
                    <Text style={sale.wasFulfilled() ? styles.tertiary : styles.primary}>+${sale.getRoyalty()}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={styles.tertiary}>{sale.getDate()}</Text>
                  </View>
                  <View style={styles.spacer}></View>
                  <Text style={[styles.tinyText, styles.tertiary, styles.opaque]}>#{sale.getRef()}</Text>
                </TouchableOpacity>
              )
            })
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  orderContainer: {
    height: 60,
    minWidth: 150,
    borderRadius: 8,
    margin: 5,
    padding: 10,
    flexDirection: 'column'
  }
});
