import React from 'react';
import { View, SafeAreaView, Text, Linking, Image, StyleSheet, ActivityIndicator, ScrollView, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';
import * as Haptics from 'expo-haptics';
import Localization from '../localization/config';
import ActionSheet from "react-native-actions-sheet";
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});

const actionSheetRef = React.createRef();

export default function Tracking({navigation, route}) {
  const SESSION = route.params.session;

  const {orderNumber} = route.params;
  const [isLoading, setIsLoading] = React.useState(true);
  const [orderValid, setOrderValid] = React.useState(false);
  const [details, setDetails] = React.useState({
    subtotal: '',
    tax: '',
    taxPercentage: '',
    shipping: '',
    total: ''
  });
  const [tracking, setTracking] = React.useState('');
  const [carrier, setCarrier] = React.useState('');
  const [items, setItems] = React.useState([]);
  const [updates, setUpdates] = React.useState([]);

  const onTrack = () => {
    Linking.canOpenURL(tracking).then(supported => {
      if (supported) {
        Linking.openURL(tracking);
      } else {
        alert(tracking);
      }
    });
  }

  React.useEffect(() => {

  }, [useColorScheme()])

  React.useEffect(() => {
    setIsLoading(true);
    API.get('track', {auth: SESSION.crypto, orderNumber}).then((res) => {
      if (res.isError) throw 'invalid';

      setDetails({
        subtotal: res.data._d.s,
        tax: res.data._d.t,
        taxPercentage: res.data._d.tP,
        shipping: res.data._d.sP,
        total: res.data._d.tC
      });
      setTracking(res.data._t);
      setCarrier(res.data._c);
      setItems(res.data._i);
      setUpdates(res.data._u);
      setOrderValid(true);
      if (res.data._delivered) {
        actionSheetRef.current?.setModalVisible(true);
        Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Hard);
      }
      setIsLoading(false);
    }).catch((e) => {
      setOrderValid(false);
      setIsLoading(false);
    });
  }, [orderNumber]);

  const onHeart = () => {
    actionSheetRef.current?.setModalVisible(true);
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Hard);
  };

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.navigate("History")}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>#{orderNumber}</Text>
        <View style={styles.spacer}></View>
        {
          tracking !== '' &&
          <TouchableOpacity
            style={styles.backArrow}
            onPress={() => onTrack()}
            underlayColor='#fff'>
            <FontAwesome5 name="shipping-fast" size={24} color={stylesheet.Primary} />
          </TouchableOpacity>
        }
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => onHeart()}
          underlayColor='#fff'>
          <FontAwesome5 name="heart" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      {
        !isLoading &&
        <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', minHeight: '100%', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
          {
            !isLoading && !orderValid &&
            <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.baseText, styles.centerText, styles.tertiary, styles.fullWidth, styles.opaque]}>{Localization.t('EinvalidOrder')}</Text>
            </View>
          }
          <View style={{flex: 0.05}}></View>
          <Text style={[styles.subHeaderText, styles.tertiary]}>{Localization.t('details')}</Text>
          <View style={{flex: 0.05}}></View>
          {
            items.map((item, index) => {
              return (
                <>
                  <View key={"ITEM_"+index} style={{flexDirection: 'column', width: '90%', marginLeft: '5%', marginRight: '5%'}}>
                    <View style={styles.defaultRowContainer}>
                      <Text style={[styles.tertiary]}>${item.price}</Text>
                      <View style={styles.spacer}></View>
                      <Text style={[styles.tertiary, styles.opaque]}>${item.singlePrice} x {item.quantity}</Text>
                    </View>
                    <Text style={[styles.tertiary, styles.opaque, styles.tinyText, {marginTop: 5}]}>{item.designer} - {item.name}</Text>
                  </View>
                  <View key={"SPACER_I_"+index} style={{flex: 0.03}}></View>
                </>
              )
            })
          }
          <View style={{flex: 0.1}}></View>
          <View style={{flexDirection: 'column', width: '90%', marginLeft: '5%', marginRight: '5%'}}>
            <View style={styles.defaultRowContainer}>
              <Text style={[styles.tertiary]}>${details.subtotal}</Text>
              <View style={styles.spacer}></View>
              <Text style={[styles.tertiary, styles.opaque]}>${details.subtotal}</Text>
            </View>
            <Text style={[styles.tertiary, styles.opaque, styles.tinyText, {marginTop: 5}]}>{Localization.t('subtotal')}</Text>
          </View>
          <View style={{flex: 0.03}}></View>
          <View style={{flexDirection: 'column', width: '90%', marginLeft: '5%', marginRight: '5%'}}>
            <View style={styles.defaultRowContainer}>
              <Text style={[styles.tertiary]}>${details.tax}</Text>
              <View style={styles.spacer}></View>
              <Text style={[styles.tertiary, styles.opaque]}>{details.taxPercentage}%</Text>
            </View>
            <Text style={[styles.tertiary, styles.opaque, styles.tinyText, {marginTop: 5}]}>{Localization.t('taxes')}</Text>
          </View>
          <View style={{flex: 0.03}}></View>
          <View style={{flexDirection: 'column', width: '90%', marginLeft: '5%', marginRight: '5%'}}>
            <View style={styles.defaultRowContainer}>
              {
                details.shipping === 'FREE' &&
                <Text style={[styles.tertiary]}>$0.00</Text>
              }
              {
                details.shipping !== 'FREE' &&
                <Text style={[styles.tertiary]}>${details.shipping}</Text>
              }
              <View style={styles.spacer}></View>
              {
                details.shipping === 'FREE' &&
                <Text style={[styles.tertiary, styles.opaque]}>{Localization.t('Cfree')}</Text>
              }
              {
                details.shipping !== 'FREE' &&
                <Text style={[styles.tertiary, styles.opaque]}>${details.shipping}</Text>
              }
            </View>
            <Text style={[styles.tertiary, styles.opaque, styles.tinyText, {marginTop: 5}]}>{Localization.t('shipping')}</Text>
          </View>
          <View style={{flex: 0.15}}></View>
          <View style={{flexDirection: 'column', width: '90%', marginLeft: '5%', marginRight: '5%'}}>
            <View style={styles.defaultRowContainer}>
              <Text style={[styles.tertiary]}>${details.total}</Text>
              <View style={styles.spacer}></View>
            </View>
            <Text style={[styles.tertiary, styles.opaque, styles.tinyText, {marginTop: 5}]}>{Localization.t('orderTotal')}</Text>
          </View>
          <View style={{flex: 0.1}}></View>
          <Text style={[styles.subHeaderText, styles.tertiary]}>{Localization.t('updates')}</Text>
          <View style={{flex: 0.05}}></View>
          {
            updates.reverse().map((update, index) => {
              return (
                <>
                  <View key={"UPDATE_"+index} style={{flexDirection: 'column', width: '90%', marginLeft: '5%', marginRight: '5%'}}>
                    <View style={styles.defaultRowContainer}>
                      <Text style={[styles.tertiary]}>{update.content}</Text>
                      <View style={styles.spacer}></View>
                      <Text style={[styles.tertiary, styles.opaque]}>{update.date}</Text>
                    </View>
                    <Text style={[styles.tertiary, styles.opaque, styles.tinyText, {marginTop: 5}]}>{update.provider}</Text>
                  </View>
                  <View key={"SPACER_U_"+index} style={{flex: 0.06}}></View>
                </>
              )
            })
          }
        </ScrollView>
      }
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.Primary}} defaultOverlayOpacity={0.85} indicatorColor="#FFF" gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.subHeaderText, styles.fullWidth, styles.bold, styles.centerText, {marginTop: 10}]}>‍🚀 🌎 ❤️</Text>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.secondary, {marginTop: 10}]}>{Localization.t('tailoriCardHeader')}</Text>
          <Text style={[styles.baseText, styles.paddedWidth, styles.centerText, stylesheet.StatusBar === 'light-content' ? styles.tertiary : styles.secondary, {marginTop: 40, marginBottom: 20}]}>{Localization.t('tailoriCardMessage')}</Text>
          <View style={[styles.center, styles.marginWidth, {height: 70}]}>
            <Image style={{height: 70, resizeMode: 'contain'}} source={require("../assets/signature.png")} />
          </View>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}
