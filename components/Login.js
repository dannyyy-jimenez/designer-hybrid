import React from 'react';
import { StyleSheet, Text, View, Keyboard, Image, KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity, TextInput } from 'react-native';
import localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
Appearance.addChangeListener(({ colorScheme }) => {
  stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
  styles = stylesheet.Styles;
});
const formats = require('./Formats').Formats;

export default function Login({navigation, route}) {
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [canSubmit, setCanSubmit] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  React.useEffect(() => {

  }, [useColorScheme()])

  const onNumberChange = (value) => {
    setPhoneNumber(formats.formatPhoneNumber(value));
    setCanSubmit(value.length > 5 && password.length > 5);
    setError('');
  };

  const onPasswordChange = (value) => {
    setPassword(value);
    setCanSubmit(value.length > 5 && phoneNumber.length > 5);
    setError('');
  };

  const onLogin = () => {
    setIsLoading(true);
    Keyboard.dismiss();

    const body = { phoneNumber, password };

    API.post('login', body).then(res => {
      if (res.isError) {
        setIsLoading(false);
        setError(res.response);
        setCanSubmit(false);
        return;
      }
      setPhoneNumber('');
      setPassword('');
      setCanSubmit(false);
      navigation.navigate('Auth', {name: res.data.fullName, crypto: res.data.crypto});
      setIsLoading(false);
    }).catch(() => setIsLoading(false));
  };

  return (
    <KeyboardAvoidingView behavior="position" style={styles.defaultTabContainer, {width: '100%', height: '100%'}}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          width: '100%'
        }}>
          <View style={styles.spacer}></View>
          <Image style={styles.defaultLandingImageSmall} source={require('../assets/iconion.png')}></Image>
          <View style={{flex: 0.15}}></View>
          <TextInput style={[styles.baseInput, styles.fullWidth, styles.tertiary]} value={phoneNumber} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} onChangeText={value => onNumberChange(value)} keyboardType="phone-pad" placeholder={localization.t('phoneNumber')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} dataDetectorTypes="phoneNumber" autoCompleteType="tel" textContentType="telephoneNumber" />
          <View style={{flex: 0.1}}></View>
          <TextInput style={[styles.baseInput, styles.fullWidth, styles.tertiary]} value={password} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} onChangeText={value => onPasswordChange(value)} placeholder={localization.t('password')} placeholderTextColor="#555555" returnKeyType="go" selectionColor={stylesheet.Primary} secureTextEntry={true} textContentType="password" />
          {
            error === "INVALID_NUM" &&
            <>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.centerText, styles.primary, styles.baseText]}>{localization.t('unrecognizedNumber')}</Text>
            </>
          }
          {
            error === "INVALID_PASS" &&
            <>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.centerText, styles.primary, styles.baseText]}>{localization.t('incorrectPassword')}</Text>
            </>
          }
          <View style={styles.spacer}></View>
          <TouchableOpacity
            disabled={!canSubmit || isLoading}
            style={[styles.roundedButton, styles.filled, !canSubmit || isLoading ? styles.disabled : {opacity: 1}]}
            onPress={() => onLogin()}
            underlayColor='#fff'>
            <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{localization.t('login')}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.roundedButton, styles.clear]}
            onPress={() => navigation.navigate('Forgot')}
            underlayColor='#fff'>
            <Text style={[styles.primary, styles.baseText, styles.centerText]}>{localization.t('forgotPassword')}</Text>
          </TouchableOpacity>
          <View style={{flex: 0.1}}></View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}
