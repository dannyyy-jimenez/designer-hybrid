import React from 'react';
import { StyleSheet, Text, Image, View, SafeAreaView, ActivityIndicator, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity, TextInput } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from '../Api';

let colorScheme = Appearance.getColorScheme();

let stylesheet = colorScheme === 'light' ? require('../Styles') : require('../Styles.Dark');
let styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Address({navigation, route}) {
  const SESSION = route.params.session;
  const [canValidateAddress, setCanValidateAddress] = React.useState(false);
  const [isValidAddress, setIsValidAddress] = React.useState(false);
  const [streetAddress, setStreetAddress] = React.useState('');
  const [streetAddressCont, setStreetAddressCont] = React.useState('');
  const [city, setCity] = React.useState('');
  const [state, setState] = React.useState('');
  const [postalCode, setPostalCode] = React.useState('');
  const [country, setCountry] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(true);

  const formatURIQuery = () => {
    return [streetAddress, city, state, country].join(' ');
  };

  React.useEffect(() => {

  }, [useColorScheme()])

  React.useEffect(() => {
    if (!isLoading) {
      setIsValidAddress(false);
    }

    setCanValidateAddress(streetAddress.length > 0 && city.length > 0 && state.length > 0 && postalCode.length > 0 && country.length > 0);
  }, [streetAddress, streetAddressCont, city, state, postalCode, country]);

  const onAddressSet = async () => {
    setCanValidateAddress(false);
    setIsLoading(true);

    API.address(formatURIQuery()).then(async ([res, coordinates]) => {
      setStreetAddress(res.Label.split(',')[0]);
      setCity(res.City);
      setState(res.State);
      setPostalCode(res.PostalCode);
      setCountry(res.Country);

      const body = {auth: SESSION.crypto, coordinates, streetAddress: res.Label.split(',')[0], streetAddressCont, city: res.City, state: res.State, postalCode: res.PostalCode, country: res.Country};
      const callRes = await API.post('store/shipping', body);
      if (callRes.isError) throw 'error';
      setIsValidAddress(true);
      setIsLoading(false);
    }).catch((e) => {
      setIsLoading(false);
      setCanValidateAddress(true);
    });
  };

  const load = async () => {
    setIsLoading(true);

    try {
      const res = await API.get("settings/shipping", {auth: SESSION.crypto});
      if (res.isError) throw 'e';

      setStreetAddress(res.data.streetAddress);
      setStreetAddressCont(res.data.streetAddressCont);
      setCity(res.data.city);
      setState(res.data.state);
      setPostalCode(res.data.postal);
      setCountry(res.data.country);
      setIsValidAddress(res.data.valid);
      setIsLoading(false);
    } catch (e) {
      navigation.goBack();
    }
  }

  const onAddressReset = async () => {
    setIsLoading(true);

    try {
      const res = await API.post("store/shipping/remove", {auth: SESSION.crypto});
      if (res.isError) throw 'e';

      setStreetAddress("");
      setStreetAddressCont("");
      setCity("");
      setState("");
      setPostalCode("");
      setCountry("");
      setIsValidAddress(false);
      setCanValidateAddress(false);
      setIsLoading(false);
    } catch (e) {
      navigation.goBack();
    }
  }

  React.useState(() => {
    load();
  }, [])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="chevron-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('address')}</Text>
        <View style={styles.spacer}></View>
      </View>
      <View style={[styles.defaultTabContent, {width: '85%', marginLeft: '7.5%'}]}>
        {
          isLoading &&
          <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
            <View style={{flex: 0.1}}></View>
            <ActivityIndicator size="small" color={stylesheet.Primary} />
          </View>
        }
        <View style={{flex: 0.1}}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.fullWidth, styles.tertiary]} value={streetAddress} onChangeText={value => setStreetAddress(value)} keyboardType="default" placeholder={Localization.t('streetAddress')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} keyboardAppearance={colorScheme === "light" ? "light" : "dark"} textContentType="fullStreetAddress" />
        <View style={{flex: 0.03}}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.fullWidth, styles.tertiary]} value={streetAddressCont} onChangeText={value => setStreetAddressCont(value)} keyboardType="default" placeholder={Localization.t('streetAddressCont')} placeholderTextColor="#555555" returnKeyType="next" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} selectionColor="#00EB00" textContentType="streetAddressLine2" />
        <View style={{flex: 0.03}}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.fullWidth, styles.tertiary]} value={city} onChangeText={value => setCity(value)} keyboardType="default" autoCorrect={true} placeholder={Localization.t('city')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} textContentType="addressCity" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} />
        <View style={{flex: 0.03}}></View>
        <View style={[styles.baseInputContainer, styles.fullWidth]}>
          <TextInput style={[styles.baseInput, styles.filledInput, styles.inputMulti, styles.tertiary]} value={state} onChangeText={value => setState(value)} keyboardType="default" autoCorrect={true} placeholder={Localization.t('state')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} textContentType="addressState" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} />
          <View style={{flex: 0.1}}></View>
          <TextInput style={[styles.baseInput, styles.filledInput, styles.inputMulti, styles.tertiary]} value={postalCode} onChangeText={value => setPostalCode(value)} keyboardType="decimal-pad" placeholder={Localization.t('postal')} placeholderTextColor="#555555" returnKeyType="next" selectionColor={stylesheet.Primary} textContentType="postalCode" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} />
        </View>
        <View style={{flex: 0.03}}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.fullWidth, styles.tertiary]} value={country} onChangeText={value => setCountry(value)} keyboardType="default" placeholder={Localization.t('country')} placeholderTextColor="#555555" returnKeyType="done" selectionColor={stylesheet.Primary} textContentType="countryName" keyboardAppearance={colorScheme === "light" ? "light" : "dark"} />
        <View style={{flex: 0.2}}></View>
        <TouchableOpacity
          disabled={!canValidateAddress || isValidAddress}
          style={[styles.roundedButton, styles.filled, !canValidateAddress || isValidAddress ? styles.disabled : {}]}
          onPress={() => onAddressSet()}
          underlayColor='#fff'>
          {
            isValidAddress &&
            <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('addressSet')}</Text>
          }
          {
            !isValidAddress &&
            <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('updateAddy')}</Text>
          }
        </TouchableOpacity>
        {
          isValidAddress &&
          <TouchableOpacity
            disabled={isLoading}
            style={[styles.roundedButton, styles.clear, isLoading ? styles.disabled : {}, {marginTop: 10}]}
            onPress={() => onAddressReset()}
            underlayColor='#fff'>
            <Text style={[styles.primary, styles.baseText, styles.centerText, styles.opaque]}>{Localization.t('clearAddress')}</Text>
          </TouchableOpacity>
        }
      </View>
    </SafeAreaView>
  );
}
