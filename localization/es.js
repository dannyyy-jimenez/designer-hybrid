export default {
  slogan: "Arranca tu Marca 🚀",
  signup: "Regístrate",
  login: "Iniciar sesión",
  phoneNumber: "Número de Teléfono",
  password: "Contraseña",
  forgotPassword: "¿Olvidaste tu contraseña?",
  unrecognizedNumber: "Número de teléfono no reconocido",
  incorrectPassword: "Contraseña incorrecta",
  forgotPasswordTask: "Ingrese el número de teléfono asociado con su cuenta. Le enviaremos una nueva contraseña segura.",
  continue: 'Continuar',
  phoneNumberPrompt: '¿Cuál es tu número de teléfono?',
  EusedNumber: 'Número de teléfono ya registrado',
  EinvalidNumber: 'El número de teléfono no es válido',
  dobPrompt: '¿Cuál es su nombre y fecha de nacimiento?',
  Edob: "Debe tener 13 años o más para usar Tailori",
  firstName: "Primer nombre",
  lastName: "Apellido",
  dob: "Fecha de nacimiento (ej. 12/09/2002)",
  securePasswordPrompt: "Establezca una contraseña segura",
  EpassLen: "La contraseña debe tener al menos 6 caracteres",
  register: "Registrarse",
  agreement: "Al tocar 'Registrarse', usted acepta nuestros",
  terms: "Términos de Servicio",
  privacy: 'Políza de Privacidad',
  search: "Buscar en Tailori",
  minPrice: "Precio Mín.",
  maxPrice: "Precio Máx.",
  enterAmount: "Ingrese Cantidad...",
  sortBy: "Ordenar por",
  newest: "Nuevo",
  oldest: "Antiguo",
  lowestPrice: "Precio Más Bajo",
  highestPrice: "Precio Más Alto",
  reactive: "Reactivo",
  limited: "Limitado",
  CsoldOut: "AGOTADO",
  chooseColor: "Elija Color de %{type}",
  moreColors: "Mas colores",
  chooseSize: "Elija Talla de %{type}",
  reactivePicture: "%{side} Imagen",
  reviewCart: "Revisar Carrito",
  addToCart: "Añadir al Carrito",
  moreBy: "Más de",
  brandReleasesPrompt: "Mira toda su mercancía",
  addedToBag: "¡Agregado a la Bolsa!",
  usernameCopied: "Nombre de Usuario Copiado!",
  EinexCartItem: "Uno o más artículos del carrito ya no están disponibles",
  EpaymentError: "Error al procesar el método de pago. Verifique el método de pago en la configuración.",
  EinvalidShip: "La información de envío no es válida",
  EexceededRelease: "Uno o más artículos en el carrito tienen un límite que se ha excedido",
  remove: "Eliminar",
  checkout: "Finalizar Compra",
  order: "Orden",
  shipping: "Envío",
  review: "Revisión",
  emptyCart: "¡Tu carrito esta vacío!",
  tapPaymentPrompt: "¡Falta información de pago!",
  tapShippingPrompt: "¡Falta la dirección de envío!",
  swipeRemove: "Para eliminar elementos, deslice el elemento hacia la derecha.",
  size: "Talla",
  color: "Color",
  streetAddress: "Dirección",
  streetAddressCont: "Apt, Suite, Unidad, Edificio (opcional)",
  city: "Ciudad",
  state: "Estado",
  postal: "Código postal",
  country: "País",
  subtotal: "Subtotal",
  modelGift: "Regalo de Modelo",
  Cfree: "GRATIS",
  taxes: "Impuestos",
  orderTotal: "Total del Pedido",
  noRefunds: "No se permiten devoluciones, todos los pedidos son definitivos.",
  modelSupport: "¿Apoyando un modelo?",
  EinvalidModel: "Código de modelo no válido",
  modelCode: "Código de modelo",
  applyCode: "Aplicar código",
  cardNumber: "Número de Tarjeta",
  cardName: "Nombre en la Tarjeta",
  cardExp: "Fecha de vcto.",
  cardCVC: "CVC",
  update: "Actualizar",
  EcardVerif: "Error de verificación. Inténtalo de nuevo.",
  EcameraRoll: "Lo sentimos, necesitamos permisos de cámara para que esto funcione.",
  brand: "Marca",
  blastOffPrompt: "¡Prepárate para despegar!",
  brandExplainer: "Crea tu marca, lanza tus diseños y recibe un pago cada vez que alguien compre tu merchandising. ¡Tú perfeccionas tus diseños, nosotros nos encargamos del resto!",
  releases: "Lanzamientos",
  hearts: "Corazones",
  sales: "Ventas",
  editBrand: "Editar marca",
  releasesAppearHere: "Los diseños publicados aparecerán aquí",
  viewSales: "Para ver las ventas de la marca, toque",
  releaseDesignPrompt: "Para publicar un diseño, toque",
  addToBrand: "Agregar a marca",
  inPortfolio: "en tu portafolio",
  nameBrand: "Nombra tu marca",
  brandName: "Nombre de marca",
  next: "Siguiente",
  describeBrand: "Describe tu marca y selecciona el logo",
  sizeRec: "Tamaño recomendado 900 x 900",
  description: "Descripción (140 caracteres máx.)",
  selectLogo: "Seleccionar logo",
  oneLastStep: "Un ultimo paso",
  brandActivatePrompt: "¿Todo listo? Toca '¡Lo hice!' para verificar su configuración",
  didIt: "¡Lo hice!",
  orderPlaced: "¡Pedido realizado!",
  orderConfirm: "Tu numero de confirmación es",
  orderUpdates: "Su pedido se enviará para su entrega en 5 a 7 días hábiles. Puede rastrear su pedido en la página de Configuración, pero también recibirá actualizaciones a través de notificaciones en la aplicación o actualizaciones de texto por SMS.",
  modelThank: "¡Gracias por apoyar nuestros modelos!",
  orderHistory: "Historial de pedidos",
  viewOrders: "Ver su Pedido",
  orderNumberPrompt: "Introduzca su número de pedido.",
  enterOrderNumber: 'Ingrese el número de orden',
  inexOrders: "No tiene pedidos recientes o pasados",
  notifications: "Notificaciones",
  portfolio: "Portafolio",
  emptyPortfolio: "¡Tu portafolio está vacío! Toque el martillo para comenzar su primer diseño",
  inMarket: 'En el Mercado',
  shareText: "Mira el lanzamiento de mi marca en Tailori",
  shareInfo: "Compartir con tus amigos y redes sociales aumenta tus ventas y alcance",
  share: 'Compartir',
  shotsSaved: "¡Imágenes de Diseño Guardadas!",
  cool: "¡Genial!",
  left: "Quedan %{amount}",
  removeFromBrand: "Eliminar de marca",
  awaitingApproval: "Esperando Aprobacion...",
  releasePrompt: "¿Listo para Lanzar?",
  and: "y",
  releaseMakeSure: "Por favor asegúrese de haber leído nuestra",
  guidelines: "Guía de diseño",
  notes: "Notas de Lanzamiento",
  explanation: "Explicación de Regalías",
  befCont: "antes de continuar",
  limitedPrompt: "¿Es esta una versión limitada?",
  no: "No",
  yes: "Si",
  limitPrompt: "¿Límite de ventas?",
  enterLimit: "Ingrese el límite de ventas ...",
  colorablePrompt: "¿Puede el comprador personalizar el color del artículo?",
  minRoyalty: "Regalía mínima?",
  learnMoreRoyalties: "Obtenga más información sobre las regalías, lee nuestro",
  pricingAlgo: "Algoritmo de Precios",
  seeOur: "Por favor vea nuestras",
  postRel: "Instrucciones despues de lanzamiento",
  release: "Lanzamiento",
  profit: "Ganancia",
  unitsSold: "Unidades Vendidas",
  history: "Historial",
  salesAppear: "Todas las ventas que realice su marca aparecerán aquí",
  EinvalidSale: "¡Lo Sentimos! Información de venta no visualizable",
  account: "Cuenta",
  name: "Nombre",
  legal: "Legal",
  dmca: "DMCA",
  logout: "Cerrar sesión",
  EinvalidOrder: "¡Lo Sentimos! Información de pedido no disponible",
  details: "Detalles",
  updates: "Actualizaciones",
  tapToTurn: "(girar)",
  pickColor: "Elige Color de %{type}",
  samplingDemons: "El área de muestreo es solo una demostración. Echa un vistazo a nuestras",
  conventions: "Convenciones",
  styles: "Estilos",
  chooseImage: "Elegir imagen",
  almostDone: "Casi terminado ...",
  bitMoreInfo: "Solo se necesita un poco más de información",
  designAppear: "El diseño puede tardar unos segundos en aparecer en el portafolio.",
  EuploadDesign: "Intenta subir tu diseño nuevamente",
  designName: "Nombre del diseño",
  addToPortfolio: "Agregar a Portafolio",
  buildPrompt: "¿Qué Vas a Construir?",
  addDesignPortPrompt: "Para agregar un diseño, toque",
  addToPortfolio: "Agregar a Portafolio",
  shirtTxB: "Camisa Tie Dye",
  shirtTsleeve: "Camiseta de Manga Larga",
  shirtT: "Camiseta",
  shirt: "Camisa",
  hoodie: "Sudadera con Capucha",
  popsocket: "PopSocket",
  mug: "Taza",
  pillow: "Almohada",
  keychain: "Llavero",
  item: "Articulo",
  XS: 'Extra pequeña',
  S: "Pequeña",
  M: "Mediano",
  L: "Grande",
  XL: "Extra Grande",
  XXL: "Extra Extra Grande",
  default: "Predeterminado",
  songPlace: "Canción - Artista",
  reactiveTxt: "Texto Reactivo",
  fullfrontal: "Frontal",
  leftchest: "Pecho Izquierdo",
  rightchest: "Pecho Derecho",
  mediumfront: "Frente Medio",
  centerfront: "Frente Central",
  acrosschest: "A Través del Pecho",
  leftvertical: "Vertical Izquierdo",
  rightvertical: "Vertical Derecho",
  bottomleft: "Abajo a la Izquierda",
  bottomright: "Abajo a la Derecha",
  fullback: "Espalda Completa",
  mediumback: "Espalda Media",
  lockerpatch: "Parche de Casillero",
  acrossback: "A Través de la Espalda",
  fullbottomback: "Inferior Completa de Espalda",
  leftsleeve: "Manga Izquierda",
  rightsleeve: "Manga Derecha",
  frontal: "Frontal",
  acrosspocket: "A través del Bolsillo",
  leftwrist: "Muñeca Izquierda",
  leftshoulder: "Hombro Izquierdo",
  rightwrist: "Muñeca Derecha",
  rightshoulder: "Hombro Derecho",
  leftbackwrist: "Muñeca Trasera Izquierda",
  leftbacksleeve: "Manga Trasera Izquierda",
  leftbackshoulder: "Hombro Trasero Izquierdo",
  rightbackwrist: "Muñeca Trasera Derecha",
  rightbacksleeve: "Manga Trasera Derecha",
  rightbackshoulder: "Hombro Trasero Derecho",
  rightfullfrontal: "Derecha (Exterior) Frontal Completa",
  rightcenterfront: "Delantero Central Derecho (Exterior)",
  leftfullfrontal: "Izquierda (Exterior) Frontal Completa",
  leftcenterfront: "Izquierda (Exterior) Centro Delantero",
  leftuppershoulder: "Hombro Superior Izquierdo",
  leftbottomarm: "Brazo Inferior Izquierdo",
  leftbottomwrist: "Muñeca Inferior Izquierda",
  rightuppershoulder: "Hombro Superior Derecho",
  rightbottomarm: "Brazo Inferior Derecho",
  rightbottomwrist: "Muñeca Inferior Derecha",
  lefthand: "Mano Izquierda",
  righthand: "Mano Derecha",
  leftelbow: "Codo Izquierdo",
  rightelbow: "Codo derecho",
  leftupperbackshoulder: "Hombro Posterior Superior Izquierdo",
  rightupperbackshoulder: "Hombro Posterior Superior Derecho",
  songname: "Nombre de la Cancion",
  payment: "Pago",
  purchaseSummary: "Resumen de Compra",
  pay: "Pagar",
  addressSet: "¡Dirección Establecida!",
  address: "Dirección de Envío",
  updateAddy: "Actualizar Dirección",
  modelCodePrompt: "¿Tienes un código de promoción?",
  clearAddress: "Borrar Dirección de Envío",
  confirmOrder: "Confirmar Pedido",
  visitShop: "Visitar Tienda",
  featured: "Destacados",
  estPricing: "Precio Estimado del Artículo: $%{price}",
  estRoyalty: "Regalía Actualizada: $%{royalty}",
  currentRoyalty: "Regalía Actual: $%{royalty}",
  minTargetPrice: "Precio Minimo: $%{cop}",
  enterMinRoy: "Ingrese regalía mínima ...",
  shareBrand: "Compartir Marca",
  reactiveWarning: "Aplique Lados Reactivos",
  shareBrandText: "Echa un Vistazo a Mi Marca en Tailori",
  tailoriCardHeader: "¡Sacudes nuestro mundo!",
  targetPricing: "Precio Objetivo de %{type}",
  tapToTurnInfo: "Gire el/la %{type} tocando la ubicación de impresión en la barra de herramientas",
  gotIt: "¡Entendido!",
  crewneck: "Cuello Redondo",
  beanie: "Gorro",
  patch: "Parche",
  redirecting: "Redireccionando",
  redirectPrompt: "Está siendo redirigido a su navegador para terminar de configurar su cuenta.",
  attention: "ATENCIÓN!",
  accountNeedsAttention: "¡Tu cuenta de Stripe requiere tu atención! ¡Si no se hace cargo de esto, los pagos se pausarán!",
  takeMe: "¡Llévame!",
  artworkfilePrompt: "¿Ha vinculado la obra de arte original?",
  noApply: "No se aplica a mi",
  artworkfilePromptm: "Para aprobar diseños intrincados, debemos tener el archivo gráfico vectorial original (eps, svg, pdf, ai). Puede vincular esto en cualquier momento tocando el ícono de la cadena en su página de diseños o tocando el enlace a continuación.",
  reactiveTap: "Toque Marco Para Configurar",
  tailoriCardMessage: "Gracias por comprar en Tailori. Quiero que sepas que tu compra significa mucho para mí. Hay una anécdota en cada una de estas tarjetas de agradecimiento, espero que disfruten del mensaje subyacente. Mi objetivo es poder ayudarte a sentirte mejor emocional, mental o incluso físicamente, incluso en la más mínima forma."
}
