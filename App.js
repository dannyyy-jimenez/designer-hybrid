// Technical imports

import 'react-native-gesture-handler';
import React from 'react';
import * as Font from 'expo-font';
import { StatusBar, Vibration, View, Platform } from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import * as SplashScreen from 'expo-splash-screen';
import * as SecureStore from 'expo-secure-store';
import * as ExpoNotifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as Linking from 'expo-linking';
import AppLoading from 'expo-app-loading';
import { Appearance, AppearanceProvider, useColorScheme } from 'react-native-appearance';

import API from './Api';

const styles = require('./Styles');
const stylesDark = require('./Styles.Dark');

const AppTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: styles.Secondary,
    primary: styles.Primary
  },
};

const AppThemeDark = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: stylesDark.Secondary,
    primary: stylesDark.Primary
  },
};

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const StoreStack = createStackNavigator();
const SettingsStack = createStackNavigator();
const CheckoutStack = createStackNavigator();
const BrandStack = createStackNavigator();

// Components

import Landing from './components/Landing';
import Register from './components/Register';
import Login from './components/Login';
import Forgot from './components/Forgot';
import Store from './components/Store';
import Settings from './components/Settings';
import Portfolio from './components/Portfolio';
import Brand from './components/Brand';
import BrandProfile from './components/BrandProfile';
import EditBrand from './components/EditBrand';
import Notifications from './components/Notifications';
import Address from './components/Address';
import Payment from './components/Payment';
import History from './components/History';
import Sales from './components/Sales';
import SaleZoom from './components/SaleZoom';
import Tracking from './components/Tracking';
import BrandStarter from './components/BrandStarter';
import PortfolioZoom from './components/PortfolioZoom';
import WorkshopStarter from './components/WorkshopStarter';
import Checkout from './components/Checkout';
import Confirmation from './components/Confirmation';
import Release from './components/Release';
import Zoom from './components/Zoom';
import Workshop from './components/Workshop';
import WorkshopFinalizer from './components/WorkshopFinalizer';
import Document from './components/Document';

// Routes

function StoreHolder({navigation, route}) {
  return (
    <StoreStack.Navigator>
      <StoreStack.Screen name="Store" initialParams={{session: route.params.session}}  component={Store} options={{headerShown: false}} />
      <StoreStack.Screen name="BrandProfile" initialParams={{session: route.params.session}}  component={BrandProfile} options={{headerShown: false}} />
      <StoreStack.Screen name="Zoom" initialParams={{session: route.params.session}}  component={Zoom} options={{headerShown: false}} />
    </StoreStack.Navigator>
  )
}

function SettingsHolder({navigation, route}) {
  return (
    <SettingsStack.Navigator initialRouteName="Settings">
      <SettingsStack.Screen name="Settings" initialParams={{session: route.params.session}} component={Settings} options={{headerShown: false}} />
      <SettingsStack.Screen name="History" initialParams={{session: route.params.session}} component={History} options={{headerShown: false}} />
      <SettingsStack.Screen name="Tracking" initialParams={{session: route.params.session}} component={Tracking} options={{headerShown: false}} />
      <SettingsStack.Screen name="Payment" initialParams={{session: route.params.session}}  component={Payment} options={{headerShown: false}} />
      <SettingsStack.Screen name="Document" initialParams={{session: route.params.session}} component={Document} options={{headerShown: false}} />
      <SettingsStack.Screen name="Notifications" initialParams={{session: route.params.session}}  component={Notifications} options={{headerShown: false}} />
    </SettingsStack.Navigator>
  )
}

function CheckoutHolder({navigation, route}) {

  return (
    <CheckoutStack.Navigator initialRouteName="Checkout">
      <CheckoutStack.Screen name="Checkout" initialParams={{session: route.params.session}}  component={Checkout} options={{headerShown: false}} />
      <CheckoutStack.Screen name="Payment" initialParams={{session: route.params.session}}  component={Payment} options={{headerShown: false}} />
      <CheckoutStack.Screen name="Address" initialParams={{session: route.params.session}}  component={Address} options={{headerShown: false}} />
      <CheckoutStack.Screen name="Confirmation" initialParams={{session: route.params.session}}  component={Confirmation} options={{headerShown: false}} />
    </CheckoutStack.Navigator>
  )
}

function BrandHolder({navigation, route}) {
  return (
    <BrandStack.Navigator initialRouteName="Brand">
      <BrandStack.Screen name="Brand" initialParams={{session: route.params.session}} component={Brand} options={{headerShown: false}} />
      <BrandStack.Screen name="BrandStarter" initialParams={{session: route.params.session}} component={BrandStarter} options={{headerShown: false}} />
      <BrandStack.Screen name="EditBrand" initialParams={{session: route.params.session}} component={EditBrand} options={{headerShown: false}} />
      <BrandStack.Screen name="Sales" initialParams={{session: route.params.session}} component={Sales} options={{headerShown: false}} />
      <BrandStack.Screen name="SaleZoom" initialParams={{session: route.params.session}} component={SaleZoom} options={{headerShown: false}} />
      <BrandStack.Screen name="Portfolio" initialParams={{session: route.params.session}} component={Portfolio} options={{headerShown: false}} />
      <BrandStack.Screen name="PortfolioZoom" initialParams={{session: route.params.session}} component={PortfolioZoom} options={{headerShown: false}} />
      <BrandStack.Screen name="Release" initialParams={{session: route.params.session}} component={Release} options={{headerShown: false}} />
      <BrandStack.Screen name="WorkshopStarter" initialParams={{session: route.params.session}} component={WorkshopStarter} options={{headerShown: false}} />
      <BrandStack.Screen name="Workshop" initialParams={{session: route.params.session}} component={Workshop} options={{headerShown: false}} />
      <BrandStack.Screen name="WorkshopFinalizer" initialParams={{session: route.params.session}} component={WorkshopFinalizer} options={{headerShown: false}} />
      <BrandStack.Screen name="Document" initialParams={{session: route.params.session}} component={Document} options={{headerShown: false}} />

    </BrandStack.Navigator>
  )
}

function Home({navigation, route}) {
  let colorScheme = useColorScheme();
  const [deepZoom, setDeepZoom] = React.useState('');
  const { logout } = React.useContext(AuthContext);

  const onLinking = (data) => {
    let {path, queryParams} = Linking.parse(data.url);
    if (!path) return;
    let matches = path.match(/^shop\/zoom\/.*$/g);

    if (matches) {
      navigation.navigate('Home', {screen: 'Store', params: {deepZoom: path.split('shop/zoom/')[1]}});
    }
    if (path === 'track' && queryParams.serial) {
      navigation.navigate('Home', {screen: 'Settings', params: {screen: 'Tracking', params: {orderNumber: queryParams.serial}}})
    }
  }
  React.useEffect(() => {
    Linking.addEventListener('url', onLinking);
  }, []);

  return (
    <Tab.Navigator initialRouteName="Store" tabBarOptions={colorScheme === 'light' ? TabBarOptions : TabBarOptionsDark} screenOptions={TabBarScreenOptions}>
      <Tab.Screen name='Store' initialParams={{session: route.params.session}} component={StoreHolder} />
      <Tab.Screen name='Checkout' initialParams={{session: route.params.session }} component={CheckoutHolder} />
      <Tab.Screen name='Brand' initialParams={{session: route.params.session }} component={BrandHolder} />
      <Tab.Screen name='Settings' initialParams={{session: route.params.session}} component={SettingsHolder} />
    </Tab.Navigator>
  );
}

const AuthContext = React.createContext();

ExpoNotifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true
  }),
});

function App() {
  const linking = {
    prefixes: ['https://www.tailorii.app', 'exps://www.tailorii.app']
  }

  let colorScheme = useColorScheme();
  const [notification, setNotification] = React.useState(false);
  const notificationListener = React.useRef();
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            authenticated: action.authenticated,
            session: action.session,
            appIsReady: true
          };
        case 'LOG_IN':
          return {
            ...prevState,
            authenticated: true,
            session: {
              ...prevState.session,
              crypto: action.crypto,
              name: action.name
            }
          };
        case 'LOG_OUT':
          return {
            ...prevState,
            authenticated: false,
            session: {
              crypto: '',
              name: '',
              socket: ''
            },
          };
        case 'NOTIFICATION_TOKEN':
          return {
            ...prevState,
            session: {
              ...prevState.session,
              socket: action.token
            }
          }
      }
    },
    {
      appIsReady: false,
      authenticated : false,
      session: {
        crypto: '',
        name: '',
        socket: ''
      },
      notification: {}
    }
  );

  const authContext = React.useMemo(() => ({
    login: async ({name, crypto, pfp}) => {
      await SecureStore.setItemAsync('_DXCY', crypto);
      await SecureStore.setItemAsync('_DXSN', name);
      dispatch({type: "LOG_IN", name, crypto, pfp})
      if (state.session.socket) {
        const body = {auth: state.session.crypto, token: state.session.socket};
        await API.post('/settings/notifications', body);
      }
    },
    logout: async (data) => {
      if (state.session.socket) {
        const body = {auth: state.session.crypto, token: state.session.socket};
        const notifRemoveReq = await API.post('/settings/notifications/remove', body);
        if (notifRemoveReq.isError) return;
      }
      dispatch({type: "LOG_OUT"})
      await SecureStore.deleteItemAsync('_DXCY');
      await SecureStore.deleteItemAsync('_DXSN');
    }
  }), []);

  React.useEffect(() => {
    ExpoNotifications.setBadgeCountAsync(0);
    const load = async () => {
      try {
        await SplashScreen.preventAutoHideAsync();
      } catch (e) {
        console.log(e);
      }

      await prepareResources();
      notificationListener.current = ExpoNotifications.addNotificationReceivedListener(notification => {
        Vibration.vibrate();
        ExpoNotifications.setBadgeCountAsync(0);
      });

    }
    load();

    return () => {
      ExpoNotifications.removeNotificationSubscription(notificationListener);
    };
  }, []);

  React.useEffect(() => {
    const subscription = ExpoNotifications.addNotificationResponseReceivedListener(response => {
      const action = response.notification.request.content.data.action;
      const serial = response.notification.request.content.data.serial;
      if (!action) return;

      if (action === 'track' && serial) {
        Linking.openURL(Linking.makeUrl('track', {serial}));
      }
    });
    return () => subscription.remove();
  }, []);

  React.useEffect(() => {
    SplashScreen.hideAsync();
  }, [state.appIsReady])

   const prepareResources = async () => {
     await Font.loadAsync({
       'Nunito': require('./assets/fonts/Nunito-SemiBold.ttf'),
    });
    let session = await loadSession();
    dispatch({type: 'RESTORE_TOKEN', ...session});
    try {
      const token = await registerForPushNotificationsAsync();
      if (token !== "") {
        if (session.authenticated) {
          const body = {auth: session.session.crypto, token};
          const res = await API.post('settings/notifications', body);
        }
        dispatch({type: 'NOTIFICATION_TOKEN', token})
      }
    } catch (e) {
      console.log(e);
    }
  };

  const loadSession = async () => {
    let crypto = await SecureStore.getItemAsync('_DXCY');
    let name = await SecureStore.getItemAsync('_DXSN');
    let authenticated = (crypto && name) !== null;

    return {
      authenticated: authenticated,
      session: {
        crypto: authenticated ? crypto : '',
        name: authenticated ? name : '',
        socket: state.session.socket
      }
    }
  }

  return (
    <AppearanceProvider>
      <AuthContext.Provider value={authContext}>
        <StatusBar barStyle={colorScheme === 'light' ? styles.StatusBar : stylesDark.StatusBar} />
        {
          !state.appIsReady &&
          <AppLoading />
        }
        {
          state.appIsReady &&
          <NavigationContainer linking={linking} theme={colorScheme === 'light' ? AppTheme : AppThemeDark}>
            <Stack.Navigator screenOptions={{gestureEnabled: false}}>
              {
                state.authenticated ? (
                  <>
                    <Stack.Screen name="Home" component={Home} initialParams={{session: state.session}} options={{headerShown: false}}/>
                    <Stack.Screen name="Logout" options={{headerShown: false}}>
                      {props => {
                        React.useEffect(() => {
                          authContext.logout(props.route.params);
                        }, [])
                        return <View style={{background: styles.Secondary, height: '100%', width: '100%'}}></View>
                      }}
                    </Stack.Screen>
                  </>
                ) :
                (
                  <>
                    <Stack.Screen name="Landing" component={Landing} options={{headerShown: false}}/>
                    <Stack.Screen name="Register" component={Register} options={colorScheme === 'light' ? DefaultHeaderOptions : DefaultHeaderOptionsDark}/>
                    <Stack.Screen name="Login" component={Login} options={colorScheme === 'light' ? DefaultHeaderOptions : DefaultHeaderOptionsDark}/>
                    <Stack.Screen name="Auth" options={{headerShown: false}}>
                      {props => {
                        React.useEffect(() => {
                          authContext.login(props.route.params);
                        }, [])
                        return <View style={{background: styles.Secondary, height: '100%', width: '100%'}}></View>
                      }}
                    </Stack.Screen>
                    <Stack.Screen name="Forgot" component={Forgot} options={{headerStyle: {
                      borderBottomWidth: 0,
                      elevation: 0,
                      shadowRadius: 0,
                      shadowOffset: {
                          height: 0,
                      },
                      backgroundColor: styles.Secondary,
                    },
                    title: false, headerBackTitleVisible: false, headerBackImage: () => <Ionicons style={{marginLeft: 15}} name="chevron-back" size={28} color={styles.Primary} />}}/>
                  </>
                )
              }
              <Stack.Screen name="Document" component={Document} options={{headerShown: false}} />
            </Stack.Navigator>
          </NavigationContainer>
        }
      </AuthContext.Provider>
    </AppearanceProvider>
  )
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await ExpoNotifications.getExpoPushTokenAsync()).data;
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    ExpoNotifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: ExpoNotifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FFDB113B',
    });
  }

  return token;
};

const DefaultHeaderOptions = ({navigation}) => ({
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    shadowRadius: 0,
    shadowOffset: {
        height: 0,
    },
    backgroundColor: styles.Secondary,
  },
  title: false,
  headerBackImage: () => <Ionicons onPress={() => navigation.navigate('Landing')} style={{marginLeft: 15}} name="ios-close" size={36} color={styles.Primary} />,
  headerBackTitleVisible: false
})

const DefaultHeaderOptionsDark = ({navigation}) => ({
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    shadowRadius: 0,
    shadowOffset: {
        height: 0,
    },
    backgroundColor: stylesDark.Secondary,
  },
  title: false,
  headerBackImage: () => <Ionicons onPress={() => navigation.navigate('Landing')} style={{marginLeft: 15}} name="ios-close" size={36} color={styles.Primary} />,
  headerBackTitleVisible: false
})

const TabBarOptions = {
  activeTintColor: styles.Primary,
  inactiveTintColor: '#CCC',
  activeBackgroundColor: styles.Secondary,
  inactiveBackgroundColor: styles.Secondary,
  showLabel: false,
  showIcon: true,
  style: {
    backgroundColor: styles.Secondary,
    borderTopWidth: 0
  }
}

const TabBarOptionsDark = {
  activeTintColor: stylesDark.Tertiary,
  inactiveTintColor: '#555',
  activeBackgroundColor: stylesDark.Secondary,
  inactiveBackgroundColor: stylesDark.Secondary,
  showLabel: false,
  showIcon: true,
  style: {
    backgroundColor: stylesDark.Secondary,
    borderTopWidth: 0
  }
}

const TabBarScreenOptions = ({route}) => ({
  tabBarIcon: ({focused, color, size}) => {
    let iconName;
    if (route.name === 'Store') {
      iconName = "store";

      return <MaterialIcons name={iconName} size={28} color={color} />;
    } else if (route.name === 'Checkout') {
      iconName = "md-cart";
    } else if (route.name === 'Settings') {
      iconName = "md-person";
    } else if (route.name === 'Brand') {
      iconName = "ios-rocket";
    } else if (route.name === "Notifications") {
      iconName = "md-notifications";
    }

    return <Ionicons name={iconName} size={28} color={color} />;
  }
})

export default App;
