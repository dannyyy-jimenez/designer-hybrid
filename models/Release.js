import Design from './Design';
const formats = require('../components/Formats').Formats;

export default class Release extends Design {

  constructor(id, type, shots, name, description, reactive, identifier, price, remaining) {
    super(id, type, shots, name, description, true, reactive);
    this.identifier = identifier;
    this.price = price;
    this.remaining = remaining;
  }

  getRawParent() {
    return new Design(this.id, this.type, this.shots, this.name, this.description, true, this.reactive);
  }

  getPrice() {
    return this.price.toFixed(2);
  }

  isLimitedRelease() {
    return this.remaining > -1;
  }

  hasSoldOut() {
    return this.remaining == 0;
  }
}
