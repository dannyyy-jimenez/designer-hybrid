export const DesignTypes = [
  {
    identifier: 'shirtT',
    uri: 't-shirt',
    name: 'T-Shirt',
    sides: ['fullfrontal', 'mediumfront', 'centerfront', 'acrosschest', 'leftchest', 'rightchest', 'leftvertical', 'rightvertical', 'bottomleft', 'bottomright', 'fullback', 'mediumback', 'lockerpatch', 'acrossback', 'fullbottomback', 'leftsleeve', 'rightsleeve'],
    sidesThumbnail: [
      'pg_4;5/tailori/templates/static/t-shirt_layers.png',
      'pg_4;6/tailori/templates/static/t-shirt_layers.png',
      'pg_4;7/tailori/templates/static/t-shirt_layers.png',
      'pg_4;8/tailori/templates/static/t-shirt_layers.png',
      'pg_4;9/tailori/templates/static/t-shirt_layers.png',
      'pg_4;10/tailori/templates/static/t-shirt_layers.png',
      'pg_4;11/tailori/templates/static/t-shirt_layers.png',
      'pg_4;12/tailori/templates/static/t-shirt_layers.png',
      'pg_4;13/tailori/templates/static/t-shirt_layers.png',
      'pg_4;14/tailori/templates/static/t-shirt_layers.png',

      'pg_4;5/tailori/templates/static/t-shirt_back_layers.png',
      'pg_4;6/tailori/templates/static/t-shirt_back_layers.png',
      'pg_4;7/tailori/templates/static/t-shirt_back_layers.png',
      'pg_4;8/tailori/templates/static/t-shirt_back_layers.png',
      'pg_4;9/tailori/templates/static/t-shirt_back_layers.png',

      'pg_4;5/tailori/templates/static/t-shirt_left_layers.png',
      'pg_4;5/tailori/templates/static/t-shirt_right_layers.png'
    ],
  },
  {
    identifier: 'shirtTsleeve',
    uri: 't-shirt-sleeve',
    name: "Long Sleeve T-Shirt",
    sides: ['fullfrontal', 'mediumfront', 'centerfront', 'acrosschest', 'leftchest', 'rightchest', 'leftvertical', 'rightvertical', 'leftuppershoulder', 'rightuppershoulder', 'leftbottomarm', 'rightbottomarm', 'leftbottomwrist', 'rightbottomwrist', 'bottomleft', 'bottomright', 'lefthand', 'leftsleeve', 'leftshoulder', 'righthand', 'rightsleeve', 'rightshoulder', 'fullback', 'mediumback', 'lockerpatch', 'acrossback', 'fullbottomback', 'leftwrist', 'rightwrist', 'leftelbow', 'rightelbow', 'leftupperbackshoulder', 'rightupperbackshoulder'],
    sidesThumbnail: [
      'pg_4;5/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;6/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;7/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;8/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;9/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;10/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;11/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;12/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;13/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;14/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;15/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;16/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;17/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;18/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;19/tailori/templates/static/t-shirt-sleeve_front_layers.png',
      'pg_4;20/tailori/templates/static/t-shirt-sleeve_front_layers.png',

      'pg_4;5/tailori/templates/static/t-shirt-sleeve_left_layers.png',
      'pg_4;6/tailori/templates/static/t-shirt-sleeve_left_layers.png',
      'pg_4;7/tailori/templates/static/t-shirt-sleeve_left_layers.png',

      'pg_4;5/tailori/templates/static/t-shirt-sleeve_right_layers.png',
      'pg_4;6/tailori/templates/static/t-shirt-sleeve_right_layers.png',
      'pg_4;7/tailori/templates/static/t-shirt-sleeve_right_layers.png',

      'pg_4;5/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;6/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;7/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;8/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;9/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;10/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;11/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;12/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;13/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;14/tailori/templates/static/t-shirt-sleeve_back_layers.png',
      'pg_4;15/tailori/templates/static/t-shirt-sleeve_back_layers.png'
    ]
  },
  {
    identifier: 'hoodie',
    uri: 'sweatshirt-hoodie',
    name: 'Hoodie',
    sides: ['fullfrontal', 'mediumfront', 'centerfront', 'acrosspocket', 'leftchest', 'rightchest', 'leftwrist', 'leftsleeve', 'leftshoulder', 'rightwrist', 'rightsleeve', 'rightshoulder', 'fullback', 'mediumback', 'leftbackwrist', 'leftbacksleeve', 'leftbackshoulder', 'rightbackwrist', 'rightbacksleeve', 'rightbackshoulder'],
    sidesThumbnail: [
      'pg_3;4/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;5/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;6/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;7/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;8/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;9/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;10/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;11/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;12/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;13/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;14/tailori/templates/static/sweatshirt-hoodie_layers.png',
      'pg_3;15/tailori/templates/static/sweatshirt-hoodie_layers.png',

      'pg_4;5/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
      'pg_4;6/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
      'pg_4;7/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
      'pg_4;8/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
      'pg_4;9/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
      'pg_4;10/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
      'pg_4;11/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
      'pg_4;12/tailori/templates/static/sweatshirt-hoodie_back_layers.png',
    ]
  },
  {
    identifier: 'crewneck',
    uri: 'sweatshirt-crewneck',
    name: 'Crewneck',
    sides: ['fullfrontal', 'mediumfront', 'centerfront', 'leftchest', 'rightchest', 'fullback', 'mediumback', 'lockerpatch', 'acrossback', 'fullbottomback', 'rightwrist', 'leftwrist'],
    sidesThumbnail: [
      'pg_3;4/tailori/templates/static/sweatshirt-crewneck_layers.png',
      'pg_3;5/tailori/templates/static/sweatshirt-crewneck_layers.png',
      'pg_3;6/tailori/templates/static/sweatshirt-crewneck_layers.png',
      'pg_3;7/tailori/templates/static/sweatshirt-crewneck_layers.png',
      'pg_3;8/tailori/templates/static/sweatshirt-crewneck_layers.png',

      'pg_3;4/tailori/templates/static/sweatshirt-crewneck_back_layers.png',
      'pg_3;5/tailori/templates/static/sweatshirt-crewneck_back_layers.png',
      'pg_3;6/tailori/templates/static/sweatshirt-crewneck_back_layers.png',
      'pg_3;7/tailori/templates/static/sweatshirt-crewneck_back_layers.png',
      'pg_3;8/tailori/templates/static/sweatshirt-crewneck_back_layers.png',
      'pg_3;9/tailori/templates/static/sweatshirt-crewneck_back_layers.png',
      'pg_3;10/tailori/templates/static/sweatshirt-crewneck_back_layers.png'
    ]
  },
  {
    identifier: 'beanie',
    uri: 'beanie',
    name: 'Beanie',
    sides: ['patch'],
    sidesThumbnail: [
      'pg_3;4/tailori/templates/static/beanie_layers.png'
    ]
  },
  {
    identifier: 'popsocket',
    uri: 'popsocket',
    name: 'PopSocket',
    sides: ['fullfrontal'],
    sidesThumbnail: [
      'pg_4;5/tailori/templates/static/popsocket_layers.png',
    ]
  },
  {
    identifier: 'mug',
    uri: 'mug',
    name: 'Mug',
    sides: ['rightfullfrontal', 'rightcenterfront', 'leftfullfrontal', 'leftcenterfront'],
    sidesThumbnail: [
      'pg_4;5/tailori/templates/static/mug_layers.png',
      'pg_4;6/tailori/templates/static/mug_layers.png',

      'pg_4;5/tailori/templates/static/mug_left_layers.png',
      'pg_4;6/tailori/templates/static/mug_left_layers.png',
    ]
  },
  {
    identifier: 'pillow',
    uri: 'pillow',
    name: 'Square Pillow',
    sides: ['fullfrontal'],
    sidesThumbnail: [
      'tailori/templates/static/pillow.png',
    ]
  }
  // {
  //   identifier: 'cap',
  //   uri: 'cap',
  //   name: 'Cap',
  //   sides: ['frontal'],
  //   sidesThumbnail: [
  //     'pg_4;5/tailori/templates/static/cap_layers.png'
  //   ]
  // }
  // {
  //   identifier: 'shirt_Tank',
  //   uri: 'shirt-tank',
  //   name: 'Tank'
  // },
  // {
  //   identifier: 'shirt_TankStrap',
  //   uri: 'shirt-tank-strap',
  //   name: 'Wide Strap Tank'
  // },
  // {
  //   identifier: 'shirt_TankRacerback',
  //   uri: 'shirt-tank-racerback',
  //   name: 'Racerback Tank'
  // }
];
