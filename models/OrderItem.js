import Merch from './Merch';

export default class OrderItem {

  constructor(id, release, quantity, size, color, total) {
    this.id = id;
    this.release = new Merch(release.design, release.designer, release.identifier, release.price, release.remaining, release.customizables, release.actions);
    this.quantity = quantity;
    this.size = size;
    this.color = color;
    this.total = total;
  }

  getID() {
    return this.id;
  }

  getSize() {
    return this.size;
  }

  getQuantity() {
    return this.quantity;
  }

  getColor() {
    return this.color;
  }
}
