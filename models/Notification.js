export default class Notification {

  constructor(header, subheader, action, metadata) {
    this.header = header;
    this.subheader = subheader;
    this.action = action;
    this.metadata = metadata;
  }

  getHeader() {
    return this.header;
  }

  getSubheader() {
    return this.subheader;
  }

  hasSubheader() {
    return this.subheader !== '';
  }

  getAction() {
    return this.action;
  }

  getMetadata() {
    return this.metadata;
  }
}
