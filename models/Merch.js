import Design from './Design';

class Designer {
  constructor(identifier, logo, name, description, isFavorite) {
    this.identifier = identifier;
    this.logo = logo;
    this.name = name;
    this.description = description;
    this.isFavorite = isFavorite;
  }

  getIdentifier() {
    return this.identifier;
  }

  getLogo() {
    return this.logo;
  }

  getName() {
    return this.name;
  }

  getJSON() {
    return {
      identifier: this.identifier,
      name: this.name,
      logo: this.logo,
      description: this.description,
      isFavorite: this.isFavorite
    }
  }

  getDescription() {
    return this.description;
  }

  setIsFavorite(outcome) {
    this.isFavorite = outcome;
  }
}

export default class Merch extends Design {

  constructor(design, designer, identifier, price, remaining, customizables, actions) {
    super(design.id, design.type, design.shots, design.name, design.description, true, design.reactive, design.reactives);
    this.design = design;
    this.designer = new Designer(designer.identifier, designer.logo, designer.name, designer.description, designer.isFavorite);
    this.identifier = identifier;
    this.price = price;
    this.remaining = remaining;
    this.customizables = customizables;
    this.actions = actions;
  }

  getPrice() {
    return this.price;
  }

  getIdentifier() {
    return this.identifier;
  }

  getRemaining() {
    return this.remaining;
  }

  isLimited() {
    return this.remaining > -1;
  }

  isSoldOut() {
    return this.remaining == 0;
  }

  isColorable() {
    return this.customizables.color;
  }
}
