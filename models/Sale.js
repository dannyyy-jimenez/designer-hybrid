export default class Sale {

  constructor(identifier, royalty, fulfilled, date) {
    this.identifier = identifier;
    this.royalty = royalty;
    this.fulfilled = fulfilled;
    this.date = date;
  }

  getRef() {
    return this.identifier;
  }

  getRoyalty() {
    return this.royalty.toFixed(2);
  }

  wasFulfilled() {
    return this.fulfilled;
  }

  getDate() {
    return this.date;
  }
}
