export default {
  shirtT: [
    { label: 'Black', color: '#212121', value: 'BLCK', prioritize: true },
    { label: 'White', color: '#F9F9F9', value: 'WHTT', prioritize: true },
    { label: 'Bondi', color: '#01A3C5', value: 'BNDBL', prioritize: true },
    { label: 'Blaze Orange', color: '#FF5D02', value: 'BLZRNG', prioritize: true },
    { label: 'Salem', color: '#0E864C', value: 'SLM', prioritize: true },
    { label: 'Pink', color: '#FFC5C4', value: 'PNK', prioritize: true },
    { label: 'Victoria', color: '#503C7B', value: 'VCTR', prioritize: false },
    { label: 'Claret', color: '#74192B', value: 'CLRT', prioritize: false },
    { label: 'Deep Sapphire', color: '#09436D', value: 'DPSPPHR', prioritize: false },
    { label: 'Monza', color: '#B3002F', value: 'MNZ', prioritize: false },
    { label: 'Cerulean', color: '#02A7CB', value: 'CRLN', prioritize: false },
    { label: 'Yellow Sea', color: '#FBA70A', value: 'YLLWS', prioritize: false },
    { label: 'Biscay', color: '#1D4175', value: 'BSCY', prioritize: false },
    { label: 'Ebb', color: '#F2EEED', value: 'BB', prioritize: false },
    { label: 'Vanilla', color: '#CCBCA2', value: 'VNLL', prioritize: false },
    { label: 'Heather', color: '#C0CDD6', value: 'HTHR', prioritize: false },
    { label: 'Celtic', color: '#123022', value: 'CLTC', prioritize: false },
    { label: 'Cocoa Brown', color: '#382926', value: 'CCBRWN', prioritize: false },
    { label: 'Wafer', color: '#DED1C1', value: 'WFR', prioritize: false },
    { label: 'Wheat', color: '#F3E3B2', value: 'WHT', prioritize: false },
    { label: 'Buccaneer', color: '#562828', value: 'BCCNR', prioritize: false },
    { label: 'Bluewood', color: '#2F3C54', value: 'PCKLDBLWD', prioritize: false },
    { label: 'Pearl Bush', color: '#E4DACE', value: 'PRLBSH', prioritize: false },
    { label: 'Finch', color: '#585644', value: 'FNCH', prioritize: false }
  ],
  shirtTsleeve: [
    { label: 'Black', color: '#212121', value: 'BLCK', prioritize: true },
    { label: 'White', color: '#FFF', value: 'WHT', prioritize: true }
  ],
  shirtTxB: [
    { label: 'Black', color: '#212121', value: 'BLCK', prioritize: true},
    { label: 'White', color: '#F9F9F9', value: 'WHT', prioritize: true},
    { label: 'Denim', color: '#2262CB', value: 'DNM', prioritize: true},
    { label: 'Cadmium', color: '#E79F2f', value: 'CDMM', prioritize: true},
    { label: 'Lust', color: '#E72224', value: 'LST', prioritize: true}
  ],
  hoodie: [
    { label: 'Black', color: '#212121', value: 'BLCK', prioritize: true },
    { label: 'Peach Orange', color: '#FFD08A', value: 'PCHRNG', prioritize: true},
    { label: 'Powder Blue', color: '#B6EBE3', value: 'PWDRBL', prioritize: true},
    { label: 'Dust Storm', color: '#F2CECE', value: 'DSTSTRM'},
    { label: 'Mexican Red', color: '#A91F2E', value: 'MXCNRD', prioritize: true },
    { label: 'Lavender', color: '#DCC9E0', value: 'LNGDLVNDR', prioritize: false},
    { label: 'Baby Pink', color: '#F1CDD7', value: 'BBPNK', prioritize: false},
    { label: 'Jaffa', color: '#F27B37', value: 'JFF', prioritize: false },
    { label: 'Hippie Green', color: '#5D8F54', value: 'HPPGRN', prioritize: false },
    { label: 'White', color: '#F9F9F9', value: 'WHT', prioritize: false },
    { label: 'Jacarta', color: '#3C2B61', value: 'JCRT', prioritize: false },
    { label: 'Canary', color: '#E5F767', value: 'CNRY', prioritize: false },
    { label: 'Punch', color: '#D84F2D', value: 'PNCH', prioritize: false },
    { label: 'Polo Blue', color: '#839CD2', value: 'PLBL', prioritize: false },
    { label: 'Chambray', color: '#3B589E', value: 'CHMBRY', prioritize: false },
    { label: 'Tawny Port', color: '#80273B', value: 'TWNYPRT', prioritize: false },
    { label: 'Burnt Sienna', color: '#E4863E', value: 'BRNTSNN', prioritize: false },
    { label: 'Livid Brown', color: '#512836', value: 'LVDBRWN', prioritize: false },
    { label: 'Cararra', color: '#F2F3EE', value: 'CRRR', prioritize: false },
    { label: 'Sepia Skin', color: '#9C5C38', value: 'SPSKN', prioritize: false },
    { label: 'Silver Chalice', color: '#9D9D9D', value: 'SLVRCHLC', prioritize: false },
    { label: 'Charade', color: '#2F313E', value: 'CHRD', prioritize: false },
    { label: 'Mineral Green', color: '#485A5A', value: 'MNRLGRN', prioritize: false }
  ],
  crewneck: [
    { label: 'Black', color: '#212121', value: 'BLCK', prioritize: true },
    { label: 'Neon Fuchsia', color: '#FD4863', value: 'NNFCHS', prioritize: true },
    { label: 'Byzantium', color: '#763670', value: 'BZNTM', prioritize: true },
    { label: 'Crimson Glory', color: '#B7122F', value: 'CRMSNGLR', prioritize: true },
    { label: 'Mint', color: '#CDE5CB', value: 'MNT', prioritize: true },
    { label: 'Orange', color: '#FD5931', value: 'SFTRNG', prioritize: true },
    { label: 'White', color: '#F9F9F9', value: 'WHT', prioritize: false },
    { label: 'Ash', color: '#CFCDC9', value: 'SH', prioritize: false },
    { label: 'Carmine', color: '#93002C', value: 'CMN', prioritize: false },
    { label: 'Carolina Blue', color: '#A2BCD7', value: 'SKBL', prioritize: false },
    { label: 'Pastel Orange', color: '#FEB43D', value: 'PSTLRNG', prioritize: false },
    { label: 'French Rose', color: '#EE4B8A', value: 'FRNCHRS', prioritize: false },
    { label: 'Shamrock Green', color: '#0AA264', value: 'SHMRCKGRN', prioritize: false },
    { label: 'Asparagus', color: '#8FAC6C', value: 'SPRGS', prioritize: false },
    { label: 'Iceberg', color: '#81A7DF', value: 'CBRG', prioritize: false },
    { label: 'Lavender', color: '#DDCBE7', value: 'LVNDR', prioritize: false },
    { label: 'Regalia', color: '#4E3B75', value: 'RGL', prioritize: false },
    { label: 'Lapis Lazuli', color: '#295FA9', value: 'LPSZL', prioritize: false },
    { label: 'Midori ', color: '#E9F67E', value: 'MDR', prioritize: false },
    { label: 'Carrot', color: '#F06821', value: 'CRRT', prioritize: false },
    { label: 'Flamingo', color: '#FD8CAD', value: 'FLMNG', prioritize: false },
    { label: 'Pale Silver', color: '#CEC1AF', value: 'PLSLVR', prioritize: false },
    { label: 'Blue Yonder', color: '#A9AED6', value: 'BLYNDR', prioritize: false }
  ],
  beanie: [
    { label: 'Black', color: '#212121', value: 'BLCK', prioritize: true }
  ],
  cap: [
    {label: 'Black', color: '#212121', value: 'BLCK', prioritize: true},
    {label: 'White', color: '#F9F9F9', value: 'WHT', prioritize: true}
  ],
  popsocket: [
    {label: 'Black', color: '#212121', value: 'BLCK', prioritize: true},
    {label: 'Monza', color: '#d50332', value: 'MNZ', prioritize: true},
    {label: 'Illusion', color: '#f4a6d6', value: 'ILLSN', prioritize: true},
    {label: 'Astronaut Blue', color: '#013B5B', value: 'STRNTBL', prioritize: true},
    {label: 'White', color: '#fff', value: 'WHT', prioritize: false},
    {label: 'Prelude', color: '#d6c0dd', value: 'PRLD', prioritize: false}
  ],
  pillow: [
    {label: 'White', color: '#fff', value: 'WHT', prioritize: true}
  ],
  mug: [
    {label: 'White', color: '#fff', value: 'WHT', prioritize: true}
  ],
  keychain: [
    {label: 'Silver', color: '#555', value: 'SLVR', prioritize: true}
  ]
};
