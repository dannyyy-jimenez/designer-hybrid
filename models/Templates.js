/*

  All Templates Will Follow the Following Format

    identifier -> 10 digit code that will identify template
    uri -> cloudinary id for template cover
    name -> template name

*/

const Shirts = [];
const ShirtsTSleeve = [];
const Hoodie = [];

export const Templates = {
  get: type => {
    if (type === 'shirtT') {
      return Shirts;
    } else if (type === 'shirtT-sleeve') {
      return ShirtsTSleeve;
    } else if (type === 'hoodie') {
      return Hoodie;
    }

    return [];
  }
};
