export default class Order {

  constructor(identifier, total, date, fulfilled) {
    this.identifier = identifier;
    this.total = total;
    this.date = date;
    this.fulfilled = fulfilled;
  }

  getIdentifier() {
    return this.identifier;
  }

  getTotal() {
    return this.total;
  }

  getDate() {
    return this.date;
  }

  wasFulfilled() {
    return this.fulfilled;
  }
}
